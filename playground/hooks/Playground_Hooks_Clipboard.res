let resi = ""

@module("@root/playground/hooks/Playground_Hooks_Clipboard.res?raw")
external codeExample: string = "default"

@react.component
let make = () => {
  let clipboard = Toolkit.Hooks.useClipboard(
    ~onCopyNotificationMessage="Copied !",
    "copied content",
  )

  <div>
    <Toolkit.Ui.Snackbar.Provider />
    <Toolkit.Ui.Button onClick={_ => clipboard.copy()}>
      {"Copy some content to the clipboard"->React.string}
    </Toolkit.Ui.Button>
  </div>
}
