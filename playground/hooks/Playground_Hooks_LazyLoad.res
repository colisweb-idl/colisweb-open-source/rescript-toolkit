let resi = ""

@module("@root/playground/hooks/Playground_Hooks_LazyLoad.res?raw")
external codeExample: string = "default"

@react.component
let make = () => {
  let ref = React.useRef(Js.Nullable.null)
  let intersection = ReactUse.useIntersection(
    ref,
    {
      root: Js.Nullable.null,
      rootMargin: "0px",
      threshold: 1.,
    },
  )

  <div
    className={cx([
      "h-[300px] w-64 overflow-scroll border",
      switch intersection->Js.Nullable.toOption {
      | Some({intersectionRatio}) if intersectionRatio < 1. => ""
      | _ => "bg-info-100"
      },
    ])}>
    <p className="h-[500px]">
      {"The background will change when the text at the end will be visible in the viewport. Scroll down"->React.string}
    </p>
    <p ref={ReactDOM.Ref.domRef(ref)}> {"not visible"->React.string} </p>
  </div>
}
