module RouterConfig = {
  type componentRoutes =
    | List
    | Details(string)
  type hookRoutes =
    | List
    | Details(string)
  type bindingRoutes =
    | List
    | Details(string)
  type docRoutes =
    | ApiDecoding
    | Primitives
    | Identifiers
    | Requests
    | Form

  type designRoutes =
    | Fonts
    | Colors
    | MediaQueries

  type t =
    | Home
    | Docs(docRoutes)
    | Components(componentRoutes)
    | Hooks(hookRoutes)
    | DesignSystem(designRoutes)
    | Bindings(bindingRoutes)
    | NotFound

  let make = (url: RescriptReactRouter.url) => {
    let path = switch url.path {
    | list{"colisweb-open-source", "rescript-toolkit", ...routes} => routes
    | path => path
    }

    switch path {
    | list{} => Home
    | list{"docs", ...rest} =>
      switch rest {
      | list{"api-decoding"} => Docs(ApiDecoding)
      | list{"identifiers"} => Docs(Identifiers)
      | list{"form"} => Docs(Form)
      | list{"requests"} => Docs(Requests)
      | list{"primitives"} => Docs(Primitives)
      | _ => NotFound
      }
    | list{"design", ...rest} =>
      switch rest {
      | list{"fonts"} => DesignSystem(Fonts)
      | list{"colors"} => DesignSystem(Colors)
      | list{"mediaQueries"} => DesignSystem(MediaQueries)
      | _ => NotFound
      }
    | list{"components", ...rest} =>
      switch rest {
      | list{} => Components(List)
      | list{component} => Components(Details(component))
      | _ => NotFound
      }
    | list{"hooks", ...rest} =>
      switch rest {
      | list{} => Hooks(List)
      | list{component} => Hooks(Details(component))
      | _ => NotFound
      }
    | list{"bindings", ...rest} =>
      switch rest {
      | list{} => Bindings(List)
      | list{component} => Bindings(Details(component))
      | _ => NotFound
      }

    | _ => NotFound
    }
  }

  @val
  external baseUrl: string = "import.meta.env.BASE_URL"

  let toString = (route: t) =>
    switch route {
    | Home => baseUrl
    | Docs(doc) => {
        let base = `${baseUrl}docs`

        switch doc {
        | ApiDecoding => `${base}/api-decoding`
        | Identifiers => `${base}/identifiers`
        | Form => `${base}/form`
        | Requests => `${base}/requests`
        | Primitives => `${base}/primitives`
        }
      }

    | DesignSystem(designRoute) => {
        let base = `${baseUrl}design`

        switch designRoute {
        | Fonts => `${base}/fonts`
        | Colors => `${base}/colors`
        | MediaQueries => `${base}/mediaQueries`
        }
      }

    | Components(route) => {
        let base = `${baseUrl}components`

        switch route {
        | List => `${base}`
        | Details(component) => `${base}/${component}`
        }
      }

    | Hooks(route) => {
        let base = `${baseUrl}hooks`

        switch route {
        | List => `${base}`
        | Details(component) => `${base}/${component}`
        }
      }
    | Bindings(route) => {
        let base = `${baseUrl}bindings`

        switch route {
        | List => `${base}`
        | Details(component) => `${base}/${component}`
        }
      }

    | NotFound => `${baseUrl}404`
    }
}

include Toolkit.Router.Make(RouterConfig)
