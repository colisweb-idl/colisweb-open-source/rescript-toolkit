open Toolkit__Ui

@react.component
let make = () => {
  let body = `
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu pretium leo. Phasellus imperdiet, tellus sit amet egestas varius,
        nisl est viverra lorem, in porttitor est diam id quam. Phasellus dapibus, est in malesuada tristique, eros ipsum interdum sapien,
        vitae auctor sapien magna sit amet justo. Nunc commodo sapien non tincidunt posuere. Nulla maximus porta tellus, vitae pulvinar
        velit cursus a. Nam egestas eget est nec vestibulum. Mauris porta urna eget ultrices consectetur. Phasellus ultricies erat nec
        tellus dapibus pulvinar. Nullam at odio ultrices, feugiat dolor a, vulputate ex. In hac habitasse platea dictumst. Maecenas ut sagittis eros.
        Cras sit amet urna maximus, congue nulla a, rutrum leo. Maecenas semper aliquet sapien convallis eleifend.
      `->React.string

  <div className="flex flex-col gap-4">
    <Card>
      <Card.Header> {"title"->React.string} </Card.Header>
      <Card.Body> body </Card.Body>
    </Card>
    <Card>
      <Card.Header>
        {"title"->React.string}
        <Button> {"Action"->React.string} </Button>
      </Card.Header>
      <Card.FixedBody> body </Card.FixedBody>
    </Card>
    <Card>
      <Card.Header
        action={
          label: "Click me"->React.string,
          event: () => Snackbar.show(~title="Hello", ~variant=Success, ())->ignore,
        }>
        {"title"->React.string}
      </Card.Header>
      <Card.Body> body </Card.Body>
    </Card>
    <Card>
      <Card.Header
        buttonColor=#primary
        action={
          label: "Click me"->React.string,
          event: () => Snackbar.show(~title="Hello", ~variant=Success, ())->ignore,
        }>
        {"title"->React.string}
      </Card.Header>
      <Card.Body> body </Card.Body>
    </Card>
    <Snackbar.Provider />
  </div>
}
