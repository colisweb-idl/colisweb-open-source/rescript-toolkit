open Toolkit__Ui

module Sample = {
  module FormState = {
    type error = unit
    include %lenses(type state = {login: string})
  }

  module Form = Toolkit__Form.Make(FormState)

  @react.component
  let make = () => {
    let form = Form.use(
      ~initialState={login: ""},
      ~schema={
        open Form.Validation

        schema([email(Login)])
      },
      ~onSubmit=_form => {
        Toolkit.Utils.wait(200)
        ->Promise.mapOk(_ => Js.log("submited"))
        ->Promise.mapError(Obj.magic)
      },
      (),
    )

    <ReactIntl.IntlProvider>
      <Form.Provider form className="flex flex-col gap-2 w-60">
        <div>
          <Form.Input id="login" label={"login"->React.string} field=FormState.Login />
        </div>
        <Toolkit__Ui_Button type_="submit"> {"Submit"->React.string} </Toolkit__Ui_Button>
      </Form.Provider>
    </ReactIntl.IntlProvider>
  }
}
module ReformTest = {
  @val
  external alert: string => unit = "alert"

  type userRole = [
    | #user
    | #admin
  ]
  module FormState = {
    type error = unit
    include %lenses(
      type state = {
        login: string,
        password: string,
        phone: string,
        role: option<userRole>,
        date: option<Js.Date.t>,
      }
    )
  }

  module Form = Toolkit__Form.Make(FormState)

  @react.component
  let make = () => {
    let form = Form.use(
      ~initialState={login: "", phone: "", password: "", role: None, date: None},
      ~schema={
        open Form.Validation

        schema([
          nonEmpty(Login),
          nonEmpty(~error="custom", Password),
          phone(Phone),
          optionNonEmpty(Role, ~error="Please select a role"),
          optionNonEmpty(Date, ~error="Please select a date"),
        ])
      },
      ~onSubmit=form => {
        alert(form.state.values->Obj.magic->Js.Json.stringifyWithSpace(2))
        Promise.resolved(Ok())
      },
      (),
    )

    <ReactIntl.IntlProvider>
      <Form.Provider form className="flex flex-col gap-2 w-60">
        <div>
          <Form.Input id="login" label={"login"->React.string} field=FormState.Login />
        </div>
        <div>
          <Form.Input
            id="password" label={"password"->React.string} field=FormState.Password type_="password"
          />
        </div>
        <div>
          <Form.Phone id="phone" label={"Phone"->React.string} field=FormState.Phone />
        </div>
        <div>
          <Form.SelectPolyvariant
            field={FormState.Role}
            id="kind"
            placeholder="Select a role"
            options=[
              {
                label: "User",
                value: #user,
              },
              {
                label: "Admin",
                value: #admin,
              },
            ]
            encodeValueToString={(v: userRole) => {(v :> string)}}
            decodeValueFromString={v =>
              switch v {
              | "admin" => Some(#admin)
              | "user" => Some(#user)
              | _ => None
              }}
          />
        </div>
        <div>
          <Form.DatePicker field={FormState.Date} id="date" label={"Date example"->React.string} />
        </div>
        <Toolkit__Ui_Button type_="submit"> {"Submit"->React.string} </Toolkit__Ui_Button>
      </Form.Provider>
    </ReactIntl.IntlProvider>
  }
}

module ReformNestedField = {
  @val
  external alert: string => unit = "alert"

  module NestedFormConfig = {
    module WormholeFormConfig = {
      module State = %lenses(type state = {value: string, value2: string})
      module Schema = ReSchema.Make(State)
    }

    module State = %lenses(
      type state = {
        name: string,
        date: option<Js.Date.t>,
        wormhole: array<WormholeFormConfig.State.state>,
      }
    )
    module Schema = ReSchema.Make(State)
  }

  module FormState = {
    type error = unit
    type config = {name: string, date: option<Js.Date.t>}

    include %lenses(type state = {test: string, nested: array<NestedFormConfig.State.state>})
  }
  module Form = Toolkit.Form.Make(FormState)

  @new
  external newDate: string => Js.Date.t = "Date"

  @react.component
  let make = () => {
    let form = Form.use(
      ~initialState={test: "default", nested: []},
      ~schema={
        open Form.Validation
        let wormholeSchema = {
          open NestedFormConfig.WormholeFormConfig.Schema.Validation

          schema([nonEmpty(Value)])
        }

        let nestedSchema = {
          open NestedFormConfig.Schema.Validation

          schema([
            nonEmpty(Name),
            optionNonEmpty(Date),
            arrayNonEmpty(Wormhole, ~error="You must add one wormhole"),
            customNestedSchema2(Wormhole, ({wormhole}) => {
              wormhole->Array.map(wormholeState => {
                NestedFormConfig.WormholeFormConfig.Schema.validateAndReturn(
                  wormholeState,
                  wormholeSchema,
                )->Obj.magic
              })
            }),
          ])
        }

        schema([
          arrayNonEmpty(Nested, ~error="You must add one config"),
          customNestedSchema2(Nested, ({nested}) =>
            nested->Array.map(nestedState => {
              NestedFormConfig.Schema.validateAndReturn(nestedState, nestedSchema)->Obj.magic
            })
          ),
        ])
      },
      ~onSubmit=form => {
        Js.log(form.state.values->Obj.magic->Js.Json.stringifyWithSpace(2))
        Toolkit.Utils.wait(2000)->Promise.mapError(Obj.magic)
      },
      (),
    )

    <Form.Provider form className="w-72 flex flex-col gap-4">
      <Button type_="button" onClick={_ => form.submit()}> {"Valider"->React.string} </Button>
      <div>
        <Form.Input label={"Test"->React.string} id="test" field={Test} />
      </div>
      <div>
        <div className="flex flex-row items-center gap-2 mb-2">
          <span> {"Array field :"->React.string} </span>
          <Toolkit__Ui_Button
            type_="button"
            size=#sm
            className="mt-2"
            isLoading={form.isSubmitting}
            color=#primary
            onClick={_ => {
              form.arrayPush(FormState.Nested, {date: None, name: "", wormhole: []})
              form.validateField(Field(Nested))
            }}>
            {"Add config"->React.string}
          </Toolkit__Ui_Button>
        </div>
        {form.state.values.nested
        ->Array.mapWithIndex((i, config) => {
          let nameError = form.getNestedFieldError2(
            Field(Nested),
            [{subfield: Field(NestedFormConfig.State.Name->Obj.magic), index: i}],
            (),
          )

          let dateError = form.getNestedFieldError2(
            Field(Nested),
            [{subfield: Field(NestedFormConfig.State.Date->Obj.magic), index: i}],
            (),
          )
          let wormholeError = form.getNestedFieldError2(
            Field(Nested),
            [{subfield: Field(NestedFormConfig.State.Wormhole->Obj.magic), index: i}],
            (),
          )

          <div key={i->Int.toString} className="ml-2">
            <div className="flex flex-row items-center gap-2">
              <p> {`#${i->Int.toString}`->React.string} </p>
              <div>
                <Toolkit__Ui_Button
                  size=#xs
                  color=#danger
                  type_="button"
                  onClick={_ => {
                    form.arrayRemoveByIndex(Nested, i)
                  }}>
                  {"Remove"->React.string}
                </Toolkit__Ui_Button>
              </div>
            </div>
            <div className="ml-3">
              <div>
                <Toolkit__Ui_TextInput
                  id="config_name"
                  value={config.name}
                  onChangeText={value => {
                    form.arrayUpdateByIndex(~field=Nested, ~index=i, {...config, name: value})
                  }}
                />
                {nameError->Option.mapWithDefault(React.null, error => {
                  <p className="text-danger-600 text-xs"> {error->React.string} </p>
                })}
              </div>
              <div>
                <div className="flex flex-row items-center gap-2 mb-2">
                  <span> {"Wormhole field :"->React.string} </span>
                  <Toolkit__Ui_Button
                    type_="button"
                    className="mt-2"
                    size=#sm
                    isLoading={form.isSubmitting}
                    color=#primary
                    onClick={_ => {
                      form.arrayUpdateByIndex(
                        ~field=Nested,
                        ~index=i,
                        {
                          ...config,
                          wormhole: config.wormhole->Array.concat([{value: "", value2: ""}]),
                        },
                      )
                      form.validateField(Field(Nested))
                    }}>
                    {"Add wormhole"->React.string}
                  </Toolkit__Ui_Button>
                </div>
                {config.wormhole
                ->Array.mapWithIndex((wormholeIndex, wormholeState) => {
                  let valueError = form.getNestedFieldError2(
                    Field(Nested),
                    [
                      {subfield: Field(NestedFormConfig.State.Wormhole->Obj.magic), index: i},
                      {
                        index: wormholeIndex,
                        subfield: Field(NestedFormConfig.WormholeFormConfig.State.Value->Obj.magic),
                      },
                    ],
                    (),
                  )
                  <div className="ml-3" key={`${i->Int.toString}-${wormholeIndex->Int.toString}`}>
                    <Toolkit__Ui_Label htmlFor="value">
                      {`#${wormholeIndex->Int.toString}`->React.string}
                    </Toolkit__Ui_Label>
                    <Toolkit__Ui_TextInput
                      id="value"
                      value={wormholeState.value}
                      onChange={event => {
                        let value = (event->ReactEvent.Form.target)["value"]

                        form.arrayUpdateByIndex(
                          ~field=Nested,
                          ~index=i,
                          {
                            ...config,
                            wormhole: config.wormhole->Array.mapWithIndex(
                              (storedWormholeIndex, storedWormhole) => {
                                storedWormholeIndex === wormholeIndex
                                  ? {...wormholeState, value}
                                  : storedWormhole
                              },
                            ),
                          },
                        )
                      }}
                    />
                    {valueError->Option.mapWithDefault(
                      React.null,
                      error => {
                        <p className="text-danger-600 text-xs"> {error->React.string} </p>
                      },
                    )}
                  </div>
                })
                ->React.array}
                {wormholeError->Option.mapWithDefault(React.null, error => {
                  <p className="text-danger-600 text-xs"> {error->React.string} </p>
                })}
              </div>
              <div>
                <Toolkit__Ui_TextInput
                  id="date"
                  value={config.date->Option.mapWithDefault("", Obj.magic)}
                  type_="date"
                  onChange={event => {
                    let value = (event->ReactEvent.Form.target)["value"]

                    form.arrayUpdateByIndex(
                      ~field=Nested,
                      ~index=i,
                      {
                        ...config,
                        date: switch value {
                        | "" => None
                        | v => Some(v->Obj.magic)
                        },
                      },
                    )
                  }}
                />
                {dateError->Option.mapWithDefault(React.null, error => {
                  <p className="text-danger-600 text-xs"> {error->React.string} </p>
                })}
              </div>
            </div>
          </div>
        })
        ->React.array}
        {form.getFieldError(Field(Nested))->Option.mapWithDefault(React.null, err => {
          <p className="text-danger-600 text-xs"> {err->React.string} </p>
        })}
      </div>
      <Toolkit__Ui_Button type_="submit"> {"Submit"->React.string} </Toolkit__Ui_Button>
    </Form.Provider>
  }
}

module SingleDayPickerExample = {
  open ReactDayPicker
  module Matcher = ReactDayPicker.Matcher
  @react.component
  let make = () => {
    let (date, setDate) = React.useState(() => None)

    <ReactDayPicker.SingleDayPicker
      modifiers={disabled: [Interval({before: Js.Date.make()})->Matcher.make]}
      selected=?{date}
      onSelect={(date, _) => {setDate(_ => {date})}}
      footer={date->Option.mapWithDefault(React.null, date => {
        <p>
          <ReactIntl.FormattedMessage defaultMessage="Date sélectionnée :" />
          {" "->React.string}
          <ReactIntl.FormattedDate
            value={date} day=#"2-digit" month=#"2-digit" year=#numeric hour=#"2-digit"
          />
        </p>
      })}
    />
  }
}
module RangeDayPickerExample = {
  open ReactDayPicker

  @react.component
  let make = () => {
    let (range, setRange) = React.useState(() => None)

    <ReactDayPicker.RangeDayPicker
      selected=?{range}
      onSelect={(range, _) => {setRange(_ => {range})}}
      footer={range->Option.mapWithDefault(React.null, range => {
        <div>
          <ReactIntl.FormattedMessage defaultMessage="Date sélectionnée :" />
          {<div>
            {range.from->Option.mapWithDefault(React.null, from =>
              <ReactIntl.FormattedDate
                value={from} day=#"2-digit" month=#"2-digit" year=#numeric hour=#"2-digit"
              />
            )}
            {" - "->React.string}
            {range.to->Option.mapWithDefault(React.null, to =>
              <ReactIntl.FormattedDate
                value={to} day=#"2-digit" month=#"2-digit" year=#numeric hour=#"2-digit"
              />
            )}
          </div>}
        </div>
      })}
    />
  }
}

@react.component
let make = () => {
  <div className="flex flex-col gap-4">
    <div>
      <h3> {"Sample"->React.string} </h3>
      <Sample />
    </div>
    <div className="flex flex-row gap-12">
      <div>
        <h3> {"Reform"->React.string} </h3>
        <ReformTest />
      </div>
      <div>
        <h3> {"Reform nested"->React.string} </h3>
        <ReformNestedField />
      </div>
    </div>
    <div>
      <h3> {"TextareaInput"->React.string} </h3>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"Label"->React.string}
      </Label>
      <TextareaInput id="test" placeholder="Test" />
      <div>
        <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
          {"Label"->React.string}
        </Label>
        <TextareaInput id="test" placeholder="Test" isInvalid=true />
      </div>
    </div>
    <div className="flex flex-row">
      <div>
        <h3> {"SingleDayPicker"->React.string} </h3>
        <SingleDayPickerExample />
      </div>
      <div>
        <h3> {"RangeDayPicker"->React.string} </h3>
        <RangeDayPickerExample />
      </div>
    </div>
    <div className="flex flex-row gap-4">
      <div>
        <h3> {"SingleDayPickerInput"->React.string} </h3>
        <ReactDayPicker.SingleDayPickerInput onChange={date => {Js.log(date)}} />
      </div>
      <div>
        <h3> {"RangeDayPickerInput"->React.string} </h3>
        <ReactDayPicker.RangeDayPickerInput
          onChange={date => {Js.log(date)}} withHours=true withPresetRanges=true
        />
      </div>
    </div>
    <div>
      <h3> {"NativeDatePicker"->React.string} </h3>
      <div className="grid grid-cols-2 grid-flow-row auto-rows-auto gap-x-3 gap-y-3">
        <p> {"base"->React.string} </p>
        <Toolkit.Ui.NativeDatePicker
          onChange={date => {
            Js.log(date)
          }}
        />
        <p> {"invalid"->React.string} </p>
        <Toolkit.Ui.NativeDatePicker
          onChange={date => {
            Js.log(date)
          }}
          isInvalid=true
        />
        <p> {"disabled"->React.string} </p>
        <Toolkit.Ui.NativeDatePicker
          onChange={date => {
            Js.log(date)
          }}
          disabled=true
        />
      </div>
    </div>
  </div>
}
