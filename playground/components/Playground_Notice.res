open Toolkit__Ui

@react.component
let make = () => {
  let pRef = React.useRef(Js.Nullable.null)
  let noticeRef2 = React.useRef(Js.Nullable.null)
  let (noticeVisible1, setNoticeVisible1) = React.useState(() => false)
  let (noticeVisible2, setNoticeVisible2) = React.useState(() => false)

  <div>
    <div className="flex flex-row items-center">
      <Button onClick={_ => setNoticeVisible1(v => !v)}> {"Toggle notice 1"->React.string} </Button>
      <Button onClick={_ => setNoticeVisible2(v => !v)}> {"Toggle notice 2"->React.string} </Button>
    </div>
    <p className="text-center" ref={ReactDOM.Ref.domRef(pRef)}>
      {"example notice 1"->React.string}
    </p>
    <div ref={ReactDOM.Ref.domRef(noticeRef2)} className="w-40 h-40 bg-danger-300 ml-20 mt-20">
      {"notice 2"->React.string}
    </div>
    <Notice visible=noticeVisible1 baseElementRef=pRef className="bg-info-700 p-2 rounded">
      <p className="text-white"> {"Some text there"->React.string} </p>
    </Notice>
    <Notice visible=noticeVisible2 baseElementRef=noticeRef2 className="bg-info-700 p-2 rounded">
      <p className="text-white"> {"Some text there"->React.string} </p>
    </Notice>
  </div>
}
