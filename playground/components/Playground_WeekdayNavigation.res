open ReactIntl

@react.component
let make = () =>
  <div>
    <h2>
      <FormattedMessage defaultMessage="WeekDateFilter" />
    </h2>
    <Toolkit__Ui_WeekDateFilter>
      {({selectedDay}) => {
        <div>
          {"selected day : "->React.string}
          {selectedDay->Option.mapWithDefault(React.null, value => {
            <FormattedDate value />
          })}
        </div>
      }}
    </Toolkit__Ui_WeekDateFilter>
    <h2>
      <FormattedMessage defaultMessage="WeekdayNavigation" />
    </h2>
    <Toolkit__Ui_WeekdayNavigation disabledBefore={Js.Date.make()->DateFns.startOfDay}>
      {({selectedDay}) => {
        <div>
          {"selected day : "->React.string}
          <FormattedDate value={selectedDay} />
        </div>
      }}
    </Toolkit__Ui_WeekdayNavigation>
  </div>
