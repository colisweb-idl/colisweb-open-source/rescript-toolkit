@react.component
let make = () => {
  <Toolkit__Ui_DropdownMenu
    trigger={"Click me"->React.string}
    content={[
      Item({label: "Content 1"->React.string, onSelect: Js.log}),
      Item({label: "Content 2"->React.string, rightSlot: "right slot"->React.string}),
      Separator,
      Label("Label"->React.string),
      Item({label: "Content 3 disabled"->React.string, disabled: true}),
      Item({label: "Content 4"->React.string}),
      SubItems(
        {
          label: "Sub contents disabled"->React.string,
          rightSlot: <ReactIcons.FaChevronCircleRight />,
          disabled: true,
        },
        [
          {
            label: "Sub content 1"->React.string,
          },
          {
            label: "Sub content 2"->React.string,
          },
        ],
      ),
      SubItems(
        {
          label: "Sub contents 2"->React.string,
          rightSlot: <ReactIcons.FaChevronCircleRight />,
        },
        [
          {
            label: "Sub content 1"->React.string,
          },
          {
            label: "Sub content 2"->React.string,
          },
        ],
      ),
    ]}
  />
}
