@react.component
let make = () => {
  let (login, setLogin) = React.useState(() => "")
  let (password, setPassword) = React.useState(() => "")

  <div className="flex flex-col gap-4">
    <div>
      <Toolkit__Ui_Label htmlFor="login"> {"login"->React.string} </Toolkit__Ui_Label>
      <Toolkit__Ui_TextInput id="login" onChangeText={v => setLogin(_ => v)} />
    </div>
    <div>
      <Toolkit__Ui_Label htmlFor="password"> {"password"->React.string} </Toolkit__Ui_Label>
      <Toolkit__Ui_TextInput
        id="password" type_="password" onChangeText={v => setPassword(_ => v)}
      />
    </div>
    <Toolkit__Ui_PasswordRulesNotice login password />
  </div>
}
