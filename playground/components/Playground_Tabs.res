@react.component
let make = () => {
  <div className="flex flex-col gap-6">
    <div>
      <HeadlessUi.Tab.Group>
        <HeadlessUi.Tab.List className="mb-4">
          <HeadlessUi.Tab> {"Example"->React.string} </HeadlessUi.Tab>
          <HeadlessUi.Tab> {"Code example"->React.string} </HeadlessUi.Tab>
          <HeadlessUi.Tab> {"API"->React.string} </HeadlessUi.Tab>
        </HeadlessUi.Tab.List>
        <HeadlessUi.Tab.Panels>
          <HeadlessUi.Tab.Panel> {"example"->React.string} </HeadlessUi.Tab.Panel>
          <HeadlessUi.Tab.Panel> {"code example"->React.string} </HeadlessUi.Tab.Panel>
          <HeadlessUi.Tab.Panel> {"api"->React.string} </HeadlessUi.Tab.Panel>
        </HeadlessUi.Tab.Panels>
      </HeadlessUi.Tab.Group>
    </div>
    <div>
      <h2> {"Render"->React.string} </h2>
      <HeadlessUi.Tab.Group>
        <HeadlessUi.Tab.List className="mb-4">
          <HeadlessUi.Tab.RenderTab>
            {({selected}) => {
              <span className={selected ? "text-danger-500" : "text-primary-600"}>
                {"Example"->React.string}
              </span>
            }}
          </HeadlessUi.Tab.RenderTab>
          <HeadlessUi.Tab> {"Example"->React.string} </HeadlessUi.Tab>
        </HeadlessUi.Tab.List>
        <HeadlessUi.Tab.Panels>
          <HeadlessUi.Tab.Panel> {"tab1"->React.string} </HeadlessUi.Tab.Panel>
          <HeadlessUi.Tab.Panel> {"tab2"->React.string} </HeadlessUi.Tab.Panel>
        </HeadlessUi.Tab.Panels>
      </HeadlessUi.Tab.Group>
    </div>
  </div>
}
