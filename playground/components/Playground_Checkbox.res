open Toolkit__Ui

@react.component
let make = () => {
  let (checked, setChecked) = React.useState(() => true)

  <div className="flex flex-col gap-4">
    <Checkbox name="test" value="test"> {"children"->React.string} </Checkbox>
    <Checkbox
      name="test2" value="test" inverseLabel={true} checkedClassname="border border-success-500">
      {"children"->React.string}
    </Checkbox>
    <Checkbox
      name="test2"
      value="test"
      inverseLabel={true}
      checkedClassname="border border-success-500"
      hideLabel={true}>
      {"children"->React.string}
    </Checkbox>
    <div className="w-64">
      <h2> {"controlled"->React.string} </h2>
      <Checkbox
        name="test2"
        value="test"
        checked
        onChange={(checked, _) => setChecked(_ => checked)}
        checkedClassname="border border-success-500"
        hideLabel={true}>
        {"children"->React.string}
      </Checkbox>
      <button onClick={_ => setChecked(c => !c)}> {"toggle"->React.string} </button>
    </div>
    <div>
      <h2> {"Checked"->React.string} </h2>
      <Checkbox checked=true name="test" value="test"> {"children"->React.string} </Checkbox>
    </div>
    <div>
      <h2> {"IntermediaryChecked"->React.string} </h2>
      <Checkbox checked=true intermediaryChecked=true name="test" value="test">
        {"children"->React.string}
      </Checkbox>
    </div>
    <div>
      <Checkbox checked=true name="test" value="test" size=#xs className="mb-4">
        {"xs"->React.string}
      </Checkbox>
      <Checkbox checked=true name="test" value="test" size=#sm className="mb-4">
        {"sm"->React.string}
      </Checkbox>
      <Checkbox checked=true name="test" value="test" size=#md className="mb-4">
        {"md"->React.string}
      </Checkbox>
      <Checkbox checked=true name="test" value="test" size=#lg className="mb-4">
        {"lg"->React.string}
      </Checkbox>
    </div>
  </div>
}
