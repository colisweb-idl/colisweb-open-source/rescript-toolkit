open Toolkit__Ui
open ReactSelect

@react.component
let make = () => {
  let options = [
    {
      label: "Group",
      value: "",
      options: [
        {label: "Label", value: "value"},
        {label: "Label2", value: "value2"},
        {label: "Label3", value: "value3"},
      ],
    },
    {
      label: "Alone",
      value: "alone",
    },
    {
      label: "Group2",
      value: "",
      options: [
        {label: "Label", value: "value"},
        {label: "Label2", value: "value2"},
        {label: "Label3", value: "value3"},
      ],
    },
  ]

  <div className="flex flex-col gap-6">
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"Native select"->React.string}
      </Label>
      <Select
        onChange={_ => ()}
        id="test"
        placeholder="This is a placeholder"
        options=[
          {
            label: "Label1",
            value: "1",
          },
          {
            label: "Label2",
            value: "2",
          },
          {
            label: "Label3 disabled",
            value: "3",
            disabled: true,
          },
        ]
      />
    </div>
    <div>
      <h2> {"react-select"->React.string} </h2>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        {"default"->React.string}
      </Label>
      <ReactSelect name="test" placeholder="Placeholder" onChange={v => Js.log(v)} options />
    </div>
    <div>
      <Label htmlFor="test3" optionalMessage={"Optional"->React.string}>
        {"Disabled"->React.string}
      </Label>
      <ReactSelect
        name="test3"
        isDisabled=true
        classNamePrefix="react-select"
        placeholder="Placeholder"
        onChange={v => Js.log(v)}
        options
      />
    </div>
    <div>
      <Label htmlFor="test4" optionalMessage={"Optional"->React.string}>
        {"Errored"->React.string}
      </Label>
      <ReactSelect
        name="test4"
        classNamePrefix="react-select"
        className={cx(["errored"])}
        placeholder="Placeholder"
        onChange={v => Js.log(v)}
        options
      />
      <div className="h-10" />
      <Label htmlFor="test2" optionalMessage={"Optional"->React.string}>
        {"Searchable select with multiple value"->React.string}
      </Label>
      <ReactSelectMultiple
        name="test2"
        defaultValue=[]
        classNamePrefix="react-select"
        placeholder="Placeholder"
        onChange={v => Js.log(v)}
        options
      />
    </div>
  </div>
}
