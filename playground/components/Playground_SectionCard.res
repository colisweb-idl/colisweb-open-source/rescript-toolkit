@react.component
let make = () => {
  <Toolkit__Ui_SectionCard title={"title"->React.string} icon={module(ReactIcons.FaMap)}>
    <p> {"content"->React.string} </p>
  </Toolkit__Ui_SectionCard>
}
