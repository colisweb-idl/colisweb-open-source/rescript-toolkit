open Msw

@spice
type test = {
  name: string,
  age: int,
}

let handlers = [
  Http.getAsync("fake.com/test", _ => {
    delayCustom(1000)->Promise.map(() => {
      HttpResponse.json({name: "Thomas", age: 31}->test_encode)
    })
  }),
]

let worker = setupWorker(handlers)

@val
external isProduction: bool = "import.meta.env.PROD"

module App = {
  @react.component
  let make = () => {
    let (request, fetch, _) = Toolkit__Hooks.useRequest(() => {
      Axios.get(~config=Axios.makeConfig(), "fake.com/test", ())
      ->Promise.Js.toResult
      ->Promise.flatMapOk(({data}) => data->test_decode->Promise.resolved)
    }, [])

    <div>
      <Toolkit.Ui.Button
        isLoading={request === Loading} color=#primary onClick={_ => fetch()->ignore}>
        {"Fetch !"->React.string}
      </Toolkit.Ui.Button>
      {switch request {
      | NotAsked
      | Loading => React.null
      | Done(res) =>
        <div className="bg-white p-4 shadow rounded mt-4">
          <p className="text-lg font-semibold"> {"Response : "->React.string} </p>
          {switch res {
          | Error(_) => <p> {"Error"->React.string} </p>
          | Ok({name, age}) =>
            <React.Fragment>
              <p> {`name : ${name}`->React.string} </p>
              <p> {`age : ${age->Int.toString}`->React.string} </p>
            </React.Fragment>
          }}
        </div>
      }}
    </div>
  }
}

@react.component
let make = () => {
  <div className="relative">
    <MockOverlay
      worker
      workerOptions={isProduction
        ? {
            serviceWorker: {
              url: "/colisweb-open-source/rescript-toolkit/mockServiceWorker.js",
            },
          }
        : {}}
      className="!absolute top-0">
      <App />
    </MockOverlay>
  </div>
}
