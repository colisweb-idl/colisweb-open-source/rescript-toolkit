open Toolkit__Ui

@react.component
let make = () => {
  [Radio.Default, Filled]->Array.map(variant => {
    <div key={(variant :> string)} className="bg-white p-4 rounded border mb-2">
      <h2> {`Variant : ${(variant :> string)}`->React.string} </h2>
      <div className="flex flex-row gap-4">
        <div className="flex flex-col gap-2">
          <Radio name="test" value="test1" variant> {"test1"->React.string} </Radio>
          <Radio name="test" value="test2" icon={module(ReactIcons.FaUserCircle)} variant>
            {"test2"->React.string}
          </Radio>
          <Radio name="test" value="test3" variant> {"test3"->React.string} </Radio>
          <Radio checked=true name="ddd" value="test"> {"default checked"->React.string} </Radio>
        </div>
        <div className="flex flex-col gap-2">
          <h4> {"Sizes"->React.string} </h4>
          <Radio
            checked=true
            name="test2"
            value="test5"
            size=#xs
            variant
            icon={module(ReactIcons.FaArrowAltCircleLeft)}>
            {"xs"->React.string}
          </Radio>
          <Radio
            checked=true
            name="test2"
            value="test55"
            size=#sm
            variant
            icon={module(ReactIcons.FaArrowAltCircleLeft)}>
            {"sm"->React.string}
          </Radio>
          <Radio
            checked=true
            name="test2"
            value="test33"
            size=#md
            variant
            icon={module(ReactIcons.FaArrowAltCircleLeft)}>
            {"md"->React.string}
          </Radio>
          <Radio
            checked=true
            name="test2"
            icon={module(ReactIcons.FaArrowAltCircleLeft)}
            value="test2"
            size=#lg
            variant>
            {"lg"->React.string}
          </Radio>
        </div>
      </div>
    </div>
  })
}
