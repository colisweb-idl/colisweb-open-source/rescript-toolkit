@react.component
let make = () =>
  <div>
    <HeadlessUi.Disclosure>
      <HeadlessUi.Disclosure.Button className="flex flex-row items-center gap-2">
        {"Tab 1"->React.string}
        <ReactIcons.MdKeyboardArrowRight className="ui-open:rotate-90 ui-open:transform" />
      </HeadlessUi.Disclosure.Button>
      <HeadlessUi.Disclosure.Panel> {"tab1"->React.string} </HeadlessUi.Disclosure.Panel>
    </HeadlessUi.Disclosure>
    <div className="my-4" />
    <HeadlessUi.Disclosure>
      <HeadlessUi.Disclosure.Button className="flex flex-row items-center gap-2">
        {"Tab 2"->React.string}
        <ReactIcons.MdKeyboardArrowRight className="ui-open:rotate-90 ui-open:transform" />
      </HeadlessUi.Disclosure.Button>
      <HeadlessUi.Disclosure.Panel> {"tab2"->React.string} </HeadlessUi.Disclosure.Panel>
    </HeadlessUi.Disclosure>
  </div>
