open Radix

let tags = Array.makeBy(50, i => `v1.2.0-beta.${i->Int.toString}`)

@react.component
let make = () => {
  <ScrollArea.Root className="ScrollAreaRoot">
    <ScrollArea.Viewport className="ScrollAreaViewport">
      <div className={"py-4 px-5"}>
        <div className="Text"> {"Tags"->React.string} </div>
        {tags
        ->Array.map(tag => <div className="Tag" key={tag}> {tag->React.string} </div>)
        ->React.array}
      </div>
    </ScrollArea.Viewport>
    <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation=Vertical>
      <ScrollArea.Thumb className="ScrollAreaThumb" />
    </ScrollArea.Scrollbar>
    <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation=Horizontal>
      <ScrollArea.Thumb className="ScrollAreaThumb" />
    </ScrollArea.Scrollbar>
    <ScrollArea.Corner className="ScrollAreaCorner" />
  </ScrollArea.Root>
}
