open Toolkit__Ui

@react.component
let make = () => {
  let variants: array<Toolkit__Ui_Snackbar.variant> = [Success, Warning, Danger]

  <div>
    <Snackbar.Provider />
    <div className="flex flex-col gap-4">
      {variants
      ->Array.map(variant => {
        let variantAsString = (variant :> string)

        <div className="flex flex-row gap-4" key={(variant :> string)}>
          <span>
            <Button onClick={_ => Snackbar.show(~title=variantAsString, ~variant, ())->ignore}>
              {`Trigger ${variantAsString} variant`->React.string}
            </Button>
          </span>
          <span>
            <Button
              onClick={_ =>
                Snackbar.show(
                  ~title=variantAsString,
                  ~content="Description"->React.string,
                  ~variant,
                  (),
                )->ignore}>
              {`Trigger ${variantAsString} variant with description`->React.string}
            </Button>
          </span>
        </div>
      })
      ->React.array}
    </div>
  </div>
}
