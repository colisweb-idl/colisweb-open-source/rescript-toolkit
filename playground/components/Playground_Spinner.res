open Toolkit__Ui

@react.component
let make = () => {
  let colors = Js.Dict.fromArray([
    ("current", #current),
    ("black", #black),
    ("white", #white),
    ("gray", #gray),
    ("success", #success),
    ("primary", #primary),
    ("warning", #warning),
    ("info", #info),
    ("danger", #danger),
  ])

  let sizes = Js.Dict.fromArray([("sm", #sm), ("md", #md), ("lg", #lg), ("xl", #xl)])

  <div>
    {colors
    ->Js.Dict.entries
    ->Array.mapWithIndex((i, (colorText, color)) =>
      <div key={i->Int.toString ++ colorText} className="flex flex-row items-center">
        <strong className="w-40"> {colorText->React.string} </strong>
        <div className="flex flex-row items-center gap-8 mb-4">
          {sizes
          ->Js.Dict.entries
          ->Array.mapWithIndex((k, (sizeText, size)) =>
            <div
              key={(i + k)->Int.toString ++ (colorText ++ sizeText)}
              className="flex flex-col items-center">
              <Spinner color size />
              <span className="text-xxs"> {sizeText->React.string} </span>
            </div>
          )
          ->React.array}
        </div>
      </div>
    )
    ->React.array}
  </div>
}
