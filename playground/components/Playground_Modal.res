open Toolkit__Ui

@react.component
let make = () => {
  let modalSize: array<Modal.size> = [#lg, #md, #sm, #xs]

  let modalColor: array<Modal.color> = [#primary, #success, #danger, #neutral, #dynamic]

  let (isVisible, setVisibility) = React.useState(() => false)
  let (size, setSize) = React.useState(() => #md)
  let (modalType, setModalType) = React.useState(() => #default)

  <div className="flex flex-col gap-4 items-start">
    <div>
      <Toolkit__Ui_Label htmlFor="size"> {"Size :"->React.string} </Toolkit__Ui_Label>
      <Toolkit__Ui.Select
        id="size"
        value={Obj.magic(size)}
        onChange={value => {
          setSize(_ => value->Obj.magic)
        }}
        options={modalSize->Array.map((value): Toolkit__Ui.Select.selectOption => {
          label: value->Obj.magic,
          value: value->Obj.magic,
        })}
      />
    </div>
    <div>
      <Toolkit__Ui_Label htmlFor="color"> {"Color :"->React.string} </Toolkit__Ui_Label>
      <Toolkit__Ui.Select
        id="size"
        onChange={value => {
          setModalType(_ => value === "" ? #default : #colored(Obj.magic(value)))
        }}
        options={
          open! Toolkit__Ui.Select

          [
            {
              label: "default",
              value: "",
            },
          ]->Array.concat(
            modalColor->Array.map(value => {
              label: value->Obj.magic,
              value: value->Obj.magic,
            }),
          )
        }
      />
    </div>
    <Button onClick={_ => setVisibility(_ => true)}> {"Show modal"->React.string} </Button>
    <Modal
      isVisible
      title={"title"->React.string}
      body={"Body"->React.string}
      hide={() => setVisibility(_ => false)}
      size
      type_=modalType
      icon={module(ReactIcons.FaMap)}
    />
  </div>
}
