open Toolkit__Ui

@react.component
let make = () => {
  let (progression, setProgress) = React.useState(() => 50.)
  let colors = Js.Dict.fromArray([
    ("success", #success),
    ("warning", #warning),
    ("info", #info),
    ("danger", #danger),
  ])

  <div>
    <div className="flex flex-row gap-2 mb-2">
      <Toolkit.Ui.Button
        onClick={_ =>
          setProgress(v => {
            switch v {
            | 0. => 0.
            | v => v -. 10.
            }
          })}>
        {"Decrease"->React.string}
      </Toolkit.Ui.Button>
      <Toolkit.Ui.Button
        onClick={_ =>
          setProgress(v => {
            switch v {
            | 100. => 100.
            | v => v +. 10.
            }
          })}>
        {"Increase"->React.string}
      </Toolkit.Ui.Button>
    </div>
    {colors
    ->Js.Dict.entries
    ->Array.mapWithIndex((i, (colorText, color)) =>
      <div
        key={i->Int.toString ++ colorText}
        className="flex flex-row flex-col-gap-4 mb-4 items-center">
        <strong className="w-40"> {colorText->React.string} </strong>
        <div className="w-60">
          <ProgressBar color progression />
        </div>
      </div>
    )
    ->React.array}
  </div>
}
