type rec data = array<(test, toto)>
and test = {
  name: string,
  age: int,
}
and toto = Js.Dict.t<string>

let data: data = Array.make(100, ({name: "toto", age: 2}, Js.Dict.empty()))

@react.component
let make = () => {
  let columns = React.useMemo(
    () => [
      ReactTable.makeColumn(
        ~accessor=((value, _dict)) => value.name,
        ~id="test",
        ~header="Name"->React.string,
        ~filterRender=_ => React.null,
        ~cell=cell => <p> {cell.cell.value->React.string} </p>,
        (),
      ),
    ],
    [],
  )

  let table = {
    open ReactTable
    useTable5(
      make(~columns, ~data, ()),
      useFilters,
      useSortBy,
      usePagination,
      useFlexLayout,
      useResizeColumns,
    )
  }

  <div>
    <Toolkit__Ui_Table.Pagination table />
    <Toolkit__Ui_Table.Core table emptyMessage="No items" />
  </div>
}
