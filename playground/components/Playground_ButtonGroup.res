@react.component
let make = () => {
  let sizes = Js.Dict.fromArray([("md", #md), ("lg", #xl)])

  let (selectedOption, setSelectedOption) = React.useState(() => #option1)

  <div>
    {sizes
    ->Js.Dict.entries
    ->Array.mapWithIndex((i, (sizeText, size)) =>
      <div key={i->Int.toString ++ sizeText} className="flex flex-row mb-4 items-center">
        <strong className="w-40"> {sizeText->React.string} </strong>
        <Toolkit__Ui_ButtonGroup className="my-4 self-center" size>
          <button
            className={selectedOption === #option1 ? "selected" : ""}
            onClick={_ => setSelectedOption(_ => #option1)}>
            {"Option 1"->React.string}
          </button>
          <button
            className={selectedOption === #option2 ? "selected" : ""}
            onClick={_ => setSelectedOption(_ => #option2)}>
            {"Option 2"->React.string}
          </button>
          <button
            className={selectedOption === #option3 ? "selected" : ""}
            onClick={_ => setSelectedOption(_ => #option3)}>
            {"Option 3"->React.string}
          </button>
        </Toolkit__Ui_ButtonGroup>
      </div>
    )
    ->React.array}
  </div>
}
