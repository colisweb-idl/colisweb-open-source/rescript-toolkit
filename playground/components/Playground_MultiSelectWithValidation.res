let options3 = Array.makeBy(2000, (i): Toolkit__Ui_MultiSelectWithValidation.item => {
  itemLabel: <p> {`test${i->Int.toString}`->React.string} </p>,
  label: `test${i->Int.toString}`,
  value: `test${i->Int.toString}`,
})

@react.component
let make = () => {
  let options1: array<Toolkit__Ui_MultiSelectWithValidation.item> = [
    {
      itemLabel: <p className="flex flex-row text-danger-500">
        <ReactIcons.FaAngleDown />
        {"test"->React.string}
      </p>,
      label: "Test",
      value: "test",
    },
    {
      itemLabel: <p> {"test2"->React.string} </p>,
      label: "Test2",
      value: "test2",
    },
    {
      itemLabel: <p> {"test3"->React.string} </p>,
      label: "Test3",
      value: "test3",
    },
    {
      itemLabel: <p> {"test4"->React.string} </p>,
      label: "Test4",
      value: "test4",
    },
  ]
  let options2: array<Toolkit__Ui_MultiSelectWithValidation.item> = [
    {
      itemLabel: <p> {"test4"->React.string} </p>,
      label: "Test4",
      value: "test4",
    },
  ]

  let (selectedOptions1, setSelectedOptions1) = React.useState(() => [])
  let (selectedOptions2, setSelectedOptions2) = React.useState(() => options2)
  let (selectedOptions3, setSelectedOptions3) = React.useState(() => [])

  let tmp = Toolkit__Hooks.useDisclosure()

  let options = tmp.isOpen ? options2 : options1

  <div className="flex flex-col gap-8 items-start">
    <Toolkit__Ui_Button onClick={_ => tmp.toggle()}>
      {"Change options"->React.string}
    </Toolkit__Ui_Button>
    <Toolkit__Ui_MultiSelectWithValidation
      searchPlaceholder="Filter"
      buttonClassName={"w-48"}
      dropdownClassName="w-60"
      placeholder={<span> {"Select me"->React.string} </span>}
      onValidate={selectedOptions => setSelectedOptions1(_ => selectedOptions)}
      defaultValue={selectedOptions1}
      options
    />
    <div>
      <h3 className="mb-2"> {"Preselected options"->React.string} </h3>
      <Toolkit__Ui_MultiSelectWithValidation
        searchPlaceholder="Filter"
        buttonClassName={"w-48"}
        dropdownClassName="w-60"
        placeholder={<span> {"Select me"->React.string} </span>}
        onValidate={selectedOptions => setSelectedOptions2(_ => selectedOptions)}
        defaultValue={selectedOptions2}
        options
      />
    </div>
    <div>
      <h3 className="mb-2"> {"Search"->React.string} </h3>
      <p className="mb-1">
        {"Search input is displayed when there are more than 5 options"->React.string}
      </p>
      <Toolkit__Ui_MultiSelectWithValidation
        searchPlaceholder="Filter"
        buttonClassName={"w-48"}
        dropdownClassName="w-60 max-h-[400px] overflow-auto"
        options={options3}
        placeholder={<span> {"Select me"->React.string} </span>}
        onValidate={selectedOptions => setSelectedOptions3(_ => selectedOptions)}
        defaultValue={selectedOptions3}
      />
    </div>
  </div>
}
