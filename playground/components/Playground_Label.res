open Toolkit__Ui

@react.component
let make = () =>
  <div>
    <Label htmlFor="test"> {"Label"->React.string} </Label>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      {"Label"->React.string}
    </Label>
  </div>
