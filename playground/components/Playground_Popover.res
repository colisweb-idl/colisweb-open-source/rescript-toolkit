@react.component
let make = () => {
  let content =
    <div>
      <p className="Text"> {"Dimensions"->React.string} </p>
      <fieldset className="Fieldset">
        <label className="Label" htmlFor="width"> {"Dimensions"->React.string} </label>
        <input className="Input" id="width" defaultValue="100%" />
      </fieldset>
      <fieldset className="Fieldset">
        <label className="Label" htmlFor="maxWidth"> {"Dimensions"->React.string} </label>
        <input className="Input" id="maxWidth" defaultValue="300px" />
      </fieldset>
      <fieldset className="Fieldset">
        <label className="Label" htmlFor="height"> {"Dimensions"->React.string} </label>
        <input className="Input" id="height" defaultValue="25px" />
      </fieldset>
      <fieldset className="Fieldset">
        <label className="Label" htmlFor="maxHeight"> {"Dimensions"->React.string} </label>
        <input className="Input" id="maxHeight" defaultValue="none" />
      </fieldset>
    </div>

  <div className={"flex gap-4 items-center"}>
    <Toolkit__Ui_Popover
      trigger={<div className={"bg-white p-1 inline-flex items-center rounded border"}>
        {"Top"->React.string}
        <ReactIcons.FaAngleDown />
      </div>}
      content
      contentOptions={{side: Top, sideOffset: 2}}
      rootOptions={{
        onOpenChange: Js.log,
      }}
    />
    <Toolkit__Ui_Popover
      trigger={<div className={"bg-white p-1 inline-flex items-center rounded border"}>
        {"Bottom default"->React.string}
        <ReactIcons.FaAngleDown />
      </div>}
      content
    />
    <Toolkit__Ui_Popover
      trigger={<div className={"bg-white p-1 inline-flex items-center rounded border"}>
        {"Right"->React.string}
        <ReactIcons.FaAngleDown />
      </div>}
      content
      contentOptions={{side: Right}}
    />
    <Toolkit__Ui_Popover
      trigger={<div className={"bg-white p-1 inline-flex items-center rounded border"}>
        {"Left"->React.string}
        <ReactIcons.FaAngleDown />
      </div>}
      content
      contentOptions={{side: Left}}
    />
  </div>
}
