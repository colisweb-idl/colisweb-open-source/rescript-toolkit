@react.component
let make = () => {
  let colors: array<Toolkit__Ui.IconButton.color> = [
    #primary,
    #black,
    #white,
    #gray,
    #success,
    #info,
    #danger,
    #warning,
    #neutral,
    #neutralLight,
  ]
  let variants: array<Toolkit__Ui.IconButton.variant> = [#default, #outline]

  let sizes = Js.Dict.fromArray([("xs", #xs), ("sm", #sm), ("md", #md), ("lg", #lg)])

  <div>
    {colors
    ->Array.mapWithIndex((i, color) => {
      let colorText = (color :> string)
      <div
        key={i->Int.toString ++ colorText}
        className="flex flex-col gap-4 first:border-0 border-t py-4 first:pt-0 border-neutral-500">
        <h2> {colorText->React.string} </h2>
        <div className="flex flex-row flex-wrap gap-8">
          {variants
          ->Array.map(variant => {
            let variantText = (variant :> string)
            <div key={`${colorText}${variantText}`} className="flex flex-col gap-4">
              <h4> {variantText->React.string} </h4>
              {[false, true]
              ->Array.mapWithIndex(
                (j, disabled) =>
                  <div className="flex items-center gap-4">
                    {disabled ? <p> {"Disabled"->React.string} </p> : React.null}
                    {sizes
                    ->Js.Dict.entries
                    ->Array.mapWithIndex(
                      (k, (sizeText, size)) =>
                        <div key={(i + j + k)->Int.toString ++ (colorText ++ sizeText)}>
                          <Toolkit__Ui_IconButton
                            variant color size disabled icon={<ReactIcons.MdHome />}
                          />
                        </div>,
                    )
                    ->React.array}
                  </div>,
              )
              ->React.array}
            </div>
          })
          ->React.array}
        </div>
      </div>
    })
    ->React.array}
  </div>
}
