@react.component
let make = () => {
  let disclosure = Toolkit__Hooks.useDisclosure()

  <div>
    <div>
      <Toolkit__Ui_Tooltip
        contentClassName={"w-48"}
        side={Top}
        color={Black}
        triggerContent={<span className="bg-gray-200 border-2 border-gray-600">
          {"Hover me"->React.string}
        </span>}
        tooltipContent={"Big Content Big Content Big Content Big Content Big Content Big Content "->React.string}
      />
    </div>
    <div>
      <Toolkit__Ui_Tooltip
        contentClassName={"w-48"}
        side={Left}
        color={Black}
        triggerContent={"Should be left but no space"->React.string}
        tooltipContent={"Big Content Big Content Big Content Big Content Big Content Big Content "->React.string}
      />
    </div>
    <div className={"mt-10"}>
      <h2> {"Controlled"->React.string} </h2>
      <Toolkit__Ui_Tooltip.Controlled
        contentClassName={"w-48"}
        side={Bottom}
        color={White}
        open_={disclosure.isOpen}
        onOpenChange={_ => disclosure.toggle()}
        triggerContent={"Hover me"->React.string}
        tooltipContent={"Big Content Big Content Big Content Big Content Big Content Big Content "->React.string}
      />
    </div>
  </div>
}
