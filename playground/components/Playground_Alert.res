open Toolkit__Ui

@react.component
let make = () => {
  let variants: array<Alert.status> = [#success, #error, #info, #warning]
  let sizes: array<Alert.size> = [#xs, #sm, #md]

  <div className="flex flex-col gap-8">
    {variants
    ->Array.map(variant => {
      <div key={(variant :> string)}>
        <h3> {(variant :> string)->React.string} </h3>
        {sizes
        ->Array.map(size => {
          <div key={(size :> string)}>
            <h4> {(size :> string)->React.string} </h4>
            <Alert
              title={"Title"->React.string}
              size
              description={"Description"->React.string}
              status=variant
            />
          </div>
        })
        ->React.array}
        <h4> {"Borderless"->React.string} </h4>
        <Alert
          title={"Title"->React.string}
          borderless=true
          description={"Description"->React.string}
          status=variant
        />
      </div>
    })
    ->React.array}
  </div>
}
