open PlaygroundUtils

module List = {
  @react.component
  let make = () => {
    let (search, setSearch) = React.useState(() => "")
    let inputSearchRef = React.useRef(Js.Nullable.null)

    React.useEffect(() => {
      let fn = Browser.Document.addEventListener("keydown", event => {
        let metaKey: bool = (event->Obj.magic)["metaKey"]
        let key: string = (event->Obj.magic)["key"]

        switch (metaKey, key) {
        | (true, "k") =>
          inputSearchRef.current
          ->Js.Nullable.toOption
          ->Option.forEach(
            domRef => {
              domRef->Browser.DomElement.focus
            },
          )
        | _ => ()
        }
      })

      Some(
        () => {
          Browser.Document.removeEventListener("keydown", fn->Obj.magic)
        },
      )
    }, [])

    <div>
      <h1 className="text-4xl font-bold font-display mb-4 flex flex-row gap-4 items-center">
        {"Components"->React.string}
        <input
          ref={ReactDOM.Ref.domRef(inputSearchRef)}
          type_="search"
          placeholder="Search (cmd + K)"
          value={search}
          onChange={event => {
            let target = event->ReactEvent.Form.target

            setSearch(_ => target["value"])
          }}
          className="bg-white text-sm font-sans px-4 font-normal  rounded-md border border-neutral-300 focus:border-primary-700 h-10 w-60"
        />
      </h1>
      <div className="flex flex-row flex-wrap gap-4">
        {files
        ->Lodash.sortBy(({name}) => name->Js.String2.toLowerCase)
        ->Array.keep(({name}) => name->Js.String2.toLowerCase->Js.String2.includes(search))
        ->Array.map(({name}) => {
          <PlaygroundRouter.Link
            key={name}
            route={Components(Details(name))}
            className="p-4 rounded-lg border bg-white hover:border-primary-700 w-64">
            <strong className="capitalize"> {name->React.string} </strong>
          </PlaygroundRouter.Link>
        })
        ->React.array}
      </div>
    </div>
  }
}

module Details = {
  @react.component
  let make = (~component: string) => {
    PlaygroundRouter.Breadcrumb.use([
      {
        route: Components(List),
        text: "Components"->React.string,
      },
      {
        route: Components(Details(component)),
        text: <span className="capitalize"> {`${component}`->React.string} </span>,
      },
    ])
    <div>
      <h1 className="text-4xl font-bold font-display mb-4 capitalize">
        {component->React.string}
      </h1>
      <div>
        {files
        ->Array.getBy(({name}) => {
          component->Js.String2.toLocaleLowerCase === name->Js.String2.toLocaleLowerCase
        })
        ->Option.mapWithDefault("unknown"->React.string, file => {
          let {jsModule, codeExample} = file
          <div>
            <HeadlessUi.Tab.Group>
              <HeadlessUi.Tab.List className="mb-4 cw-tab-list max-w-[500px]">
                <HeadlessUi.Tab className="cw-tab"> {"Example"->React.string} </HeadlessUi.Tab>
                <HeadlessUi.Tab className="cw-tab"> {"Code example"->React.string} </HeadlessUi.Tab>
                {file.resi->Option.mapWithDefault(React.null, _ => {
                  <HeadlessUi.Tab className="cw-tab"> {".resi"->React.string} </HeadlessUi.Tab>
                })}
              </HeadlessUi.Tab.List>
              <HeadlessUi.Tab.Panels>
                <HeadlessUi.Tab.Panel> {jsModule->Obj.magic} </HeadlessUi.Tab.Panel>
                <HeadlessUi.Tab.Panel>
                  <Playground_CodeBlock code={codeExample} />
                </HeadlessUi.Tab.Panel>
                {file.resi->Option.mapWithDefault(React.null, code => {
                  <HeadlessUi.Tab.Panel>
                    <Playground_CodeBlock code />
                  </HeadlessUi.Tab.Panel>
                })}
              </HeadlessUi.Tab.Panels>
            </HeadlessUi.Tab.Group>
          </div>
        })}
      </div>
    </div>
  }
}
