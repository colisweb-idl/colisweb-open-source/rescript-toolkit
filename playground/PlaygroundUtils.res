type rec globOptions = {\"as"?: globAs, eager?: bool, import?: string}
and globAs = [#raw | #url]
@val
external glob: string => Js.Dict.t<string> = "import.meta.glob"
@val
external globWithOptions: (string, globOptions) => Js.Dict.t<string> = "import.meta.glob"

type binding = {
  name: string,
  content: string,
}

type bindingModule =
  | Single(binding)
  | Multiple(string, array<binding>)

let bindings =
  globWithOptions("@root/src/vendors/**/*.res", {\"as": #raw, eager: true})
  ->Js.Dict.entries
  ->Array.reduce([], (acc, (key, content)) => {
    let folders =
      key
      ->Js.String2.replace("/src/vendors/", "")
      ->Js.String2.replace(".res", "")
      ->Js.String2.split("/")

    switch folders {
    | [name] =>
      acc->Array.concat([
        Single({
          name: name->Js.String2.toLowerCase,
          content,
        }),
      ])
    | [moduleName, fileName] => {
        let found = acc->Array.some(binding => {
          switch binding {
          | Single(_) => false
          | Multiple(m, _) => m === moduleName
          }
        })

        if found {
          acc->Array.map(binding => {
            switch binding {
            | Multiple(moduleName, contents) if moduleName === moduleName =>
              Multiple(moduleName, contents->Array.concat([{name: fileName, content}]))
            | _ => binding
            }
          })
        } else {
          acc->Array.concat([Multiple(moduleName, [{name: fileName, content}])])
        }
      }
    | _ => acc
    }
  })

type jsModule
type rawFileText

type playgroundComponent = {
  name: string,
  jsModule: jsModule,
  codeExample: string,
  resi?: string,
}

let playgroundJsModules: Js.Dict.t<jsModule> =
  globWithOptions(
    `@root/lib/es6_global/playground/components/*.js`,
    {eager: true, import: "make"},
  )->Obj.magic

let filesResi = globWithOptions(`@root/src/ui/*.resi`, {eager: true, \"as": #raw})->Obj.magic

let files: array<playgroundComponent> =
  globWithOptions("@root/playground/components/**/*.res", {\"as": #raw, eager: true})
  ->Js.Dict.entries
  ->Array.reduce([], (acc, (filePathname, content)) => {
    let parsedFilePathname =
      filePathname
      ->Js.String2.replace("/playground/components/", "")
      ->Js.String2.replace(".res", "")
      ->Js.String2.split("/")
      ->Array.getUnsafe(0)

    let jsModule =
      playgroundJsModules->Js.Dict.unsafeGet(
        `/lib/es6_global/playground/components/${parsedFilePathname}.bs.js`,
      )

    let resiFilename = parsedFilePathname->Js.String2.replace("Playground_", "")

    let resi = filesResi->Js.Dict.get(`/src/ui/Toolkit__Ui_${resiFilename}.resi`)

    let component = {
      name: parsedFilePathname->Js.String2.replace("Playground_", ""),
      jsModule,
      codeExample: content,
      ?resi,
    }

    acc->Js.Array2.push(component)->ignore

    acc
  })
