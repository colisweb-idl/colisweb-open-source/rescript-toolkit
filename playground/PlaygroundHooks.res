module type Config = {
  let resi: string
  let codeExample: string
  @react.component
  let make: unit => React.element
}

let components: array<(string, module(Config))> = [
  ("Clipboard", module(Playground_Hooks_Clipboard)),
  ("Disclosure", module(Playground_Hooks_Disclosure)),
  ("LazyLoad", module(Playground_Hooks_LazyLoad)),
  ("MediaQueries", module(Playground_Hooks_MediaQueries)),
]

module List = {
  module ComponentLink = {
    @react.component
    let make = (~name) => {
      <PlaygroundRouter.Link
        route={Hooks(Details(name))}
        className="p-4 rounded-lg border bg-white hover:border-primary-700 w-64">
        <strong className="capitalize"> {name->React.string} </strong>
      </PlaygroundRouter.Link>
    }
  }

  @react.component
  let make = () => {
    let (search, setSearch) = React.useState(() => "")
    let inputSearchRef = React.useRef(Js.Nullable.null)

    React.useEffect(() => {
      let fn = Browser.Document.addEventListener("keydown", event => {
        let metaKey: bool = (event->Obj.magic)["metaKey"]
        let key: string = (event->Obj.magic)["key"]

        switch (metaKey, key) {
        | (true, "k") =>
          inputSearchRef.current
          ->Js.Nullable.toOption
          ->Option.forEach(
            domRef => {
              domRef->Browser.DomElement.focus
            },
          )
        | _ => ()
        }
      })

      Some(
        () => {
          Browser.Document.removeEventListener("keydown", fn->Obj.magic)
        },
      )
    }, [])

    <div>
      <h1 className="text-4xl font-bold font-display mb-4 flex flex-row gap-4 items-center">
        {"Hooks"->React.string}
        <input
          ref={ReactDOM.Ref.domRef(inputSearchRef)}
          type_="search"
          placeholder="Search (cmd + K)"
          value={search}
          onChange={event => {
            let target = event->ReactEvent.Form.target

            setSearch(_ => target["value"])
          }}
          className="bg-white text-sm font-sans px-4 font-normal  rounded-md border border-neutral-300 focus:border-primary-700 h-10 w-60"
        />
      </h1>
      <div className="flex flex-row flex-wrap gap-4">
        {components
        ->Array.keep(((name, _)) => name->Js.String2.toLowerCase->Js.String2.includes(search))
        ->Array.map(((name, _)) => <ComponentLink name key={name} />)
        ->React.array}
      </div>
    </div>
  }
}

module Details = {
  module Component = {
    @react.component
    let make = (~component: module(Config)) => {
      let module(Config) = component
      <div>
        <HeadlessUi.Tab.Group>
          <HeadlessUi.Tab.List className="mb-4 cw-tab-list max-w-[500px] mx-auto">
            <HeadlessUi.Tab className="cw-tab"> {"Example"->React.string} </HeadlessUi.Tab>
            <HeadlessUi.Tab className="cw-tab"> {"Code example"->React.string} </HeadlessUi.Tab>
            <HeadlessUi.Tab className="cw-tab"> {"API"->React.string} </HeadlessUi.Tab>
          </HeadlessUi.Tab.List>
          <HeadlessUi.Tab.Panels>
            <HeadlessUi.Tab.Panel>
              <Config />
            </HeadlessUi.Tab.Panel>
            <HeadlessUi.Tab.Panel>
              <Playground_CodeBlock code={Config.codeExample} />
            </HeadlessUi.Tab.Panel>
            <HeadlessUi.Tab.Panel>
              <Playground_CodeBlock code={Config.resi} />
            </HeadlessUi.Tab.Panel>
          </HeadlessUi.Tab.Panels>
        </HeadlessUi.Tab.Group>
      </div>
    }
  }

  @react.component
  let make = (~component: string) => {
    PlaygroundRouter.Breadcrumb.use([
      {
        route: Hooks(List),
        text: "Hooks"->React.string,
      },
      {
        route: Hooks(Details(component)),
        text: <span className="capitalize"> {`${component}`->React.string} </span>,
      },
    ])
    <div>
      <div>
        {components
        ->Array.getBy(((name, _)) => {
          component->Js.String2.toLowerCase === name->Js.String2.toLowerCase
        })
        ->Option.mapWithDefault("unknown"->React.string, ((_, playgroundModule)) => {
          <Component component={playgroundModule} />
        })}
      </div>
    </div>
  }
}
