import tailwindConfig from "../../src/tailwind/tailwind.config.cjs";

const fontFamily = tailwindConfig.theme.fontFamily;
const fontSize = tailwindConfig.theme.fontSize;

export default () => (
  <div>
    {Object.entries(fontFamily).map(([fontValue, fontName]) => {
      return (
        <div className="mb-6 border-b" key={fontValue}>
          <h3 className="text-xl font-bold mb-4">
            <span className="px-2 py-1 bg-white border rounded">
              font-{fontValue}
            </span>{" "}
            : {fontName.join(", ")}
          </h3>
          <div className="flex flex-row gap-4 items-center flex-wrap">
            {Object.entries(fontSize).map(([key, fontSize]) => {
              return (
                <div key={key + "bug"} className="text-center">
                  <p
                    style={{
                      fontFamily: fontName.join(", "),
                      fontSize,
                    }}
                  >
                    {key}
                  </p>
                  <p>{fontSize}</p>
                </div>
              );
            })}
          </div>
        </div>
      );
    })}
  </div>
);
