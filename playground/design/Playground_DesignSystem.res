module Fonts = {
  @module("../../../../playground/design/DesignSystem_Fonts.mdx") @react.component
  external make: unit => React.element = "default"
}
module Colors = {
  @module("../../../../playground/design/DesignSystem_Colors.mdx") @react.component
  external make: unit => React.element = "default"
}
module MediaQueries = {
  @module("../../../../playground/design/DesignSystem_MediaQueries.mdx") @react.component
  external make: unit => React.element = "default"
}
