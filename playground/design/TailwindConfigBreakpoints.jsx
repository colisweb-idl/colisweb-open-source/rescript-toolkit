import tailwindConfig from "../../src/tailwind/tailwind.config.cjs";

const screens = tailwindConfig.theme.screens;

export default () => (
  <div>
    {Object.entries(screens).map(([key, mediaquery]) => {
      return (
        <div className="flex flex-row items-center mb-4" key={key}>
          <h3 className="text-2xl font-bold mr-2 w-16">{key}</h3>
          <code className="bg-gray-200 p-2">{`@media (min-width: ${mediaquery})`}</code>
        </div>
      );
    })}
  </div>
);
