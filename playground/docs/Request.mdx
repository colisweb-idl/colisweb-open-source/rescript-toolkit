# How to manage an API request

## 1. Create the request config

You have to declare 3 types :

- `argument` is what you give to the function
- `response` is what your API answer
- `error` is the potential custom errors that can occur (⚠️ this has to be handled manually)

And the `exec` method.

We use [spice](https://github.com/green-labs/ppx_spice) to serialize / deserialize the data.

```rescript
let request = Axios.Instance.create(
  Axios.makeConfig(~baseURL=envState.apiUrl,()),
);

module FetchUsers = {
  module Config = {
    type argument = unit;

    @spice
    type response = array<user>
    @spice
    and user = {
      name: string
    };

    type error = string;

    let exec = () => {
      request->Request.get("/", response_decode);
    };
  };
  module Request = Toolkit.Request.Make(Config);
};

FetchUsers.Request.exec();
```

### 1.2 Send data

We create a decodable `body` type that we give to the `argument` type.
This decodable `body` will be used in the exec method.

```rescript
module UpdateUser = {
  module Config = {
    type argument = body
    @spice
    and body = {
      name: string,
    };

    @spice
    type response = unit;

    type error = unit;

    let exec = body => {
      request->Request.postData("/update-user", response_decode, body->body_encode);
    };
  };
  module Request = Toolkit_Request.Make(Config);
};

UpdateUser.Request.exec({ name: "test" })
```

### 1.3 Handle errors

In order to handle special errors comming from the API, you can use the `mapError` parameter.

This parameter requires a callback which contains the **response** as argument and needs to return this type :

```rescript
option<result<'data, error<'a>>>
```

The `result` is useful for some cases when an API error should not break the flow so you can handle the specific error and just return `Ok()`.

#### Request error type

```rescript
type undecodedResponse = {
  data: undecodedData,
  headers,
  config,
  status: int,
};

type noResponse = {
  message: string,
  config,
};

// We get this type in mapError callback argument
type failedResponse = {
  message: string,
  config,
  response,
}
and response = {
  data: failedResponseData,
  headers,
  status: int,
};

type error<'a> = [
  | #noResponse(noResponse)
  | #invalidResponse(failedResponse)
  | #invalidResponseData(undecodedResponse, string)
  | #invalidErrorData
  | #unknown(Js.Promise.error)
  | #custom('a)
]
```

```rescript
let exec = token => {
  request->Request.get(
    "/test",
    ~mapError=
      error => {
        // have to return option(Toolkit.Request.error('a'))
      },
    response_decode,
  );
};
```

`failedResponseData` can be anything depending on the API behavior. So you have to use `Obj.magic` to cast and use the correct field.

```rescript
let handleError = (error: failedResponse) => {
  // the API return an object { code: string }
  let code = error.response.data->Obj.magic##code;

  // If code can be undefined
  code
  ->Option.map(code => {
    switch (code) {
      | "some_code" => Error(#custom("message"))
      | "other_code" => Error(#invalidErrorData)
      | "its_ok" => Ok()
      | _ => Error(#custom("unknown"))
    }
  })
};
```

Often, we use decco in order to handle complex error (error with data). We create a custom Decoder for this kind of case

```rescript
module UpdateUser = {
  module Config = {
    type argument = body
    @spice
    and body = {name: string};

    @spice
    type response = unit;

    module Error =
      Decoders.Enum({
        @deriving(jsConverter)
        type enum = [
          | #"unknown-user"
          | #invalidData
          | #unknownError
        ];
      });

    @spice
    type error = Error.t;

    let exec = body => {
      request->Request.postData(
        "/update-user",
        response_decode,
        body->body_encode,
        ~mapError=
          ({response}) => {
            switch (response.data->Obj.magic->error_decode) {
            | Ok(error) => Some(Error(`custom(error)))
            | Error(err) =>
              Js.log(err);
              Some(Error(`custom(`unknownError)));
            }
          }
      );
    };
  };
  module Request = Toolkit_Request.Make(Config);
};

UpdateUser.Request.exec({ name: "test" })
->Promise.tapError(err => {
  switch (err) {
    | #custom(#"unknown-user") => Js.log("user not found")
    | #custom(#unknownError) => Js.log("alert sentry")
    | _ => Js.log("generic handler")
  }
})
```

## 2. Use request in React

### 2.1 Fetching data with swr

There are several ways to manage a request in React, we chose to use [swr](https://swr.vercel.app/) with [react-suspense](https://reactjs.org/docs/concurrent-mode-suspense.html) enabled.
We have a functor to generate automaticatly a swr fetcher based on the request created before :

```rescript
module GetUsers = {
  module Config = {
    type argument = unit;

    @spice
    type response = array<user>
    @spice
    and user = {
      name: string
    };

    type error = string;

    let exec = () => {
      request->Request.get("/users", response_decode);
    };
  };
  module Request = Toolkit.Request.Make();
};


module GetUsersFetcher =
  Fetcher.Make({
    module Request = GetUsers.Request;
    let key = () => [|
      "GetUsers",
    |];
  });
```

Later in the react app

```rescript
module PageUsers = {
  [@react.component]
  let make = () => {
    let (users, _, _) = GetUsersFetcher.use();

    <div>
      {
        users
        ->Belt.Array.mapWithIndex((i, user) => {
          <div key={i->Belt.Int.toString}>
            {user.name->React.String}
          </div>
        })
      }
    </div>
  };
};

[@react.component]
let make = () => {
  <ErrorBoundary>
    <React.Suspense fallback={<Spinner />}>
      <PageUsers />
    </React.Suspense>
  </ErrorBoundary>
};
```

There are 2 functions generated by the fetcher functor :

- `use` can be used when the API will always return something
- `useOptional` return an `option<'a>` so it's used when the API can return nothing
