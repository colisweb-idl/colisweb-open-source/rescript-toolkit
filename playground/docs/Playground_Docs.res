module ApiDecoding = {
  @module("../../../../playground/docs/ApiDecoding.mdx") @react.component
  external make: unit => React.element = "default"
}
module Form = {
  @module("../../../../playground/docs/Form.mdx") @react.component
  external make: unit => React.element = "default"
}
module Identifiers = {
  @module("../../../../playground/docs/Identifiers.mdx") @react.component
  external make: unit => React.element = "default"
}
module Primitives = {
  @module("../../../../playground/docs/Primitives.mdx") @react.component
  external make: unit => React.element = "default"
}
module Requests = {
  @module("../../../../playground/docs/Request.mdx") @react.component
  external make: unit => React.element = "default"
}
module Setup = {
  @module("../../../../playground/docs/Setup.mdx") @react.component
  external make: unit => React.element = "default"
}
module Router = {
  @module("../../../../playground/docs/Router.mdx") @react.component
  external make: unit => React.element = "default"
}
module Unleash = {
  @module("../../../../playground/docs/Unleash.mdx") @react.component
  external make: unit => React.element = "default"
}
