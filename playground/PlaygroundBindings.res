open PlaygroundUtils

module List = {
  @react.component
  let make = () => {
    let (search, setSearch) = React.useState(() => "")

    <div>
      <h1 className="text-4xl font-bold font-display mb-4 flex flex-row gap-4 items-center">
        {"Bindings"->React.string}
        <input
          type_="search"
          placeholder="Search"
          value={search}
          onChange={event => {
            let target = event->ReactEvent.Form.target

            setSearch(_ => target["value"])
          }}
          className="bg-white text-sm font-sans px-4 font-normal  rounded-md border border-neutral-300 focus:border-primary-700 h-10 w-60"
        />
      </h1>
      <div className="flex flex-row flex-wrap gap-4">
        {bindings
        ->Array.map(binding =>
          switch binding {
          | Single({name})
          | Multiple(name, _) =>
            <PlaygroundRouter.Link
              key={name}
              route={Bindings(Details(name))}
              className="p-4 rounded-lg border bg-white hover:border-primary-700 w-64">
              <strong className="capitalize"> {name->React.string} </strong>
            </PlaygroundRouter.Link>
          }
        )
        ->React.array}
      </div>
    </div>
  }
}

module Details = {
  @react.component
  let make = (~component: string) => {
    PlaygroundRouter.Breadcrumb.use([
      {
        route: Bindings(List),
        text: "Bindings"->React.string,
      },
      {
        route: Bindings(Details(component)),
        text: <span className="capitalize"> {`${component}`->React.string} </span>,
      },
    ])
    <div>
      <h1 className="text-4xl font-bold font-display mb-4 capitalize">
        {component->React.string}
      </h1>
      <div>
        {bindings
        ->Array.getBy(moduleBinding =>
          switch moduleBinding {
          | Single({name})
          | Multiple(name, _) =>
            name === component
          }
        )
        ->Option.mapWithDefault("Not found"->React.string, bindingModule => {
          switch bindingModule {
          | Single({content}) => <Playground_CodeBlock code={content} />

          | Multiple(_, bindings) =>
            bindings
            ->Array.map(({name, content}) => {
              <div key={name}>
                <h2> {name->React.string} </h2>
                <Playground_CodeBlock code={content} />
              </div>
            })
            ->React.array
          }
        })}
      </div>
    </div>
  }
}
