# ReScript toolkit

![ReScript Colisweb toolkit](/media/image.jpg)

- Request
- Unleash
- Identifier
- Hooks
- Decoders
- Fetcher
- Form
- Tailwind classname generator
- vendors (library bindings)
  - `react-icons` (build with a script `build:reacticons`)
  - `react-select`
  - `react-table`
  - `swr`
  - `downshift`
  - `copy-to-clipboard`
  - `react-intl` rescript version of [https://github.com/reasonml-community/bs-react-intl](bs-react-intl)

## Usage

Install the package :

```bash
npm i @colisweb/rescript-toolkit
```

Add the package to the `bsconfig.json` file :

```json
{
  "bs-dependencies": ["@colisweb/rescript-toolkit"]
}
```

The bindings in the _vendors_ are globaly accessible. Everything else is scoped into the `Toolkit` namespace.

```rescript
open Toolkit;

[@react.component]
let make = () => {
  let dialog = Hooks.useDisclosure();

  <div className="bg-white" />
};
```

### Development

#### Build

```bash
npm run build
```

#### Watch

```bash
npm run watch
```
