# Identifier

Convert a string or an int in an opaque type with a spice decoder.
Mostly used for identifiers.

## API

### `string` identifier

```rescript
module DeliveryId = Toolkit.Identifier.MakeString({});

/**
 somewhere
**/
[@spice]
type response = {
  deliveryId: DeliveryId.t
};

deliveryId->DeliveryId.toString
```

### `int` identifier

```rescript
module DeliveryId = Toolkit.Identifier.MakeInt({});

/**
 somewhere
**/
[@spice]
type response = {
  deliveryId: DeliveryId.t
};

deliveryId->DeliveryId.toString;
deliveryId->DeliveryId.toInt;
```
