@spice
type strategy<'parameters> = {
  name: string,
  parameters: 'parameters,
}

@spice
type config<'strategy> = {
  name: string,
  description: string,
  enabled: bool,
  strategies: array<strategy<'strategy>>,
}

module type Config = {
  @spice
  type argument
  type input
  type output
  let envUrl: string
  let featureName: string
  let exec: (input, config<argument>) => output
}

module MakeFeature = (C: Config) => {
  @spice
  type t = config<C.argument>

  let exec = (var: C.input) =>
    Axios.get(C.envUrl ++ C.featureName, ~config={Axios.makeConfig()}, ())
    ->Promise.Js.toResult
    ->Promise.flatMapOk(({data}) => {
      Promise.resolved(t_decode(data))
    })
    ->Promise.mapOk(C.exec(var, ...))
}
