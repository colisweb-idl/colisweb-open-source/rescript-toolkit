// See https://github.com/streamich/react-use/blob/master/src

// ----------------------
// Utils
// ----------------------

module SafeIncrement = {
  let minInt = -999999999
  let maxInt = 1000000000

  let increment = (num: int): int => num !== maxInt ? num + 1 : minInt
}

let useIsFirstMount = ReactUse.useFirstMountState
let useUpdateEffect = (fn, deps) => {
  let isFirstMount = useIsFirstMount()

  React.useEffect(() =>
    if !isFirstMount {
      fn()
    } else {
      None
    }
  , deps)
}

let useTimeoutFn = ReactUse.useTimeoutFn
let useDebounce = ReactUse.useDebounce
let useDebounce2 = ReactUse.useDebounce2
let useDebounce3 = ReactUse.useDebounce3
let useDebounce4 = ReactUse.useDebounce4

let useUpdate = () => {
  let (_, setState) = React.useState(_ => 0)

  React.useCallback(() => setState(SafeIncrement.increment), [])
}

let useDebounceValue = (~debounceDuration=200, value) => {
  let (state, setState) = React.useState(() => value)

  useDebounce(() => {
    setState(_ => value)
  }, debounceDuration, [value])->ignore

  state
}

let useGetSet = initialStateGetter => {
  let state = React.useRef(initialStateGetter())
  let update = useUpdate()

  React.useMemo(() => (
    // get
    () => state.current,
    // set
    newStateGetter => {
      state.current = newStateGetter(state.current)
      update()
    },
  ), [])
}
let usePrevious = ReactUse.usePrevious
let useInitialPrevious = value => {
  let previousRef = React.useRef(value)

  React.useEffectOnEveryRender(() => {
    previousRef.current = value
    None
  })

  previousRef.current
}
let usePreviousDistinct = ReactUse.usePreviousDistinct

let useInitialPreviousDistinct = (~compare=(a, b) => a === b, value) => {
  let previousRef = React.useRef(value)
  let currentRef = React.useRef(value)
  let isFirstMount = useIsFirstMount()

  if !isFirstMount && !compare(currentRef.current, value) {
    previousRef.current = currentRef.current
    currentRef.current = value
  }

  previousRef.current
}

// ----------------------
// useFetcher
// ----------------------

type fetcher<'data> = ('data, bool, unit => Promise.t<result<bool, Js.Promise.error>>)

let useFetcher = (~options: option<Swr.fetcherOptions>=?, key: 'key, fn: 'fn): fetcher<'data> => {
  let {data, isValidating, mutate} = Swr.useSwr(
    key,
    fn,
    Js.Obj.assign(
      switch options {
      | Some(options) => options->Obj.magic
      | None => Js.Obj.empty()
      },
      {"suspense": true},
    ),
  )

  (data->Obj.magic, isValidating, () => Promise.Js.fromBsPromise(mutate())->Promise.Js.toResult)
}

let useOptionalFetcher = (~options: option<Swr.fetcherOptions>=?, key: 'key, fn: 'fn): fetcher<
  'data,
> => {
  let {data, isValidating, mutate} = Swr.useSwrOptional(
    key,
    fn,
    Js.Obj.assign(
      switch options {
      | Some(options) => options->Obj.magic
      | None => Js.Obj.empty()
      },
      {"suspense": true},
    ),
  )

  (data->Obj.magic, isValidating, () => mutate()->Promise.Js.fromBsPromise->Promise.Js.toResult)
}

// ----------------------
// useDisclosure
// ----------------------

type disclosure = {
  isOpen: bool,
  show: unit => unit,
  hide: unit => unit,
  toggle: unit => unit,
}

let useDisclosure = (~defaultIsOpen=?, ()) => {
  let (isOpen, setIsOpen) = React.useState(() => defaultIsOpen->Belt.Option.getWithDefault(false))

  {
    isOpen,
    show: React.useCallback(() => setIsOpen(_ => true), []),
    hide: React.useCallback(() => setIsOpen(_ => false), []),
    toggle: React.useCallback(() => setIsOpen(isOpen => !isOpen), []),
  }
}

// ----------------------
// useClipboard
// This hook require the presence of a Toolkit Ui Snackbar provider for clipboard notifications to work
// ----------------------

type clipboard = {
  value: string,
  copy: unit => unit,
  hasCopied: bool,
}

let useClipboard = (~onCopyNotificationMessage: option<string>=?, value: string) => {
  let (hasCopied, setHasCopied) = React.useState(() => false)
  let onCopy = React.useCallback(() => {
    onCopyNotificationMessage->Option.forEach(message =>
      Toolkit__Ui_Snackbar.show(~title=message, ~variant=Success, ())->ignore
    )

    let didCopy = CopyToClipboard.copy(value)
    setHasCopied(_ => didCopy)
  }, [value])

  React.useEffect(() =>
    hasCopied
      ? {
          let id = Js.Global.setTimeout(() => setHasCopied(_ => false), 1500)
          Some(() => Js.Global.clearTimeout(id))
        }
      : None
  , [hasCopied])

  {value, copy: onCopy, hasCopied}
}

// ----------------------
// useRequest
// ----------------------

external promiseErrorToJsObj: Js.Promise.error => Js.Exn.t = "%identity"

type requestState<'data, 'error> =
  | NotAsked
  | Loading
  | Done(result<'data, 'error>)

let useRequest = (~debounce=true, fn, deps) => {
  let lastCallId = React.useRef(0)
  let canceled = React.useRef(false)
  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState(.)

  let trigger = React.useCallback(args => {
    lastCallId.current = lastCallId.current->SafeIncrement.increment
    let callId = lastCallId.current

    set(_ => Loading)

    canceled.current = false

    fn(args)->Promise.map(result => {
      let isCanceled = (debounce ? callId !== lastCallId.current : false) || canceled.current

      if isMounted() && !isCanceled {
        set(_ => Done(result))
      }

      (result, isCanceled)
    })
  }, deps)

  let cancel = React.useCallback(() => canceled.current = true, [])

  (state, trigger, cancel)
}

// ----------------------
// useMediaQuery
// ----------------------

type matchedMedia = {
  matches: bool,
  media: string,
}

@val external matchMedia: string => matchedMedia = "window.matchMedia"

type mediaQuery = {
  isXs: bool,
  isSm: bool,
  isLg: bool,
}

let useMediaQuery = () => {
  let isXs = React.useMemo(() => matchMedia("(max-width: 639px)").matches, [])
  let isSm = React.useMemo(() => matchMedia("(min-width: 640px)").matches, [])
  let isLg = React.useMemo(() => matchMedia("(min-width: 1024px)").matches, [])
  {isXs, isSm, isLg}
}

let useOnClickOutside = (ref: React.ref<Js.Nullable.t<Dom.element>>, handler) => {
  React.useEffect(() => {
    let listener = event => {
      switch ref.current->Js.Nullable.toOption {
      | None => ()
      | Some(domRef) => {
          let isContentElement = ReactDOM.domElementToObj(domRef)["contains"](.
            event->ReactEvent.Form.target,
          )

          isContentElement ? () : handler()
        }
      }
    }

    Browser.Document.addEventListener("mousedown", listener)
    Browser.Document.addEventListener("touchstart", listener)

    Some(
      () => {
        Browser.Document.removeEventListener("mousedown", listener)
        Browser.Document.removeEventListener("touchstart", listener)
      },
    )
  }, (ref, handler))
}

let useIntersection = ReactUse.useIntersection

let useIsVisibleOnViewport = (~options: option<ReactUse.intersectionOptions<'a>>=?, domRef) => {
  let (isVisible, setIsVisible) = React.useState(() => false)
  let intersection = useIntersection(
    domRef,
    options->Option.getWithDefault({
      root: Js.Nullable.null,
      rootMargin: "0px",
      threshold: 1.,
    }),
  )

  React.useEffect(() => {
    intersection
    ->Js.Nullable.toOption
    ->Option.forEach(intersection => {
      if intersection.intersectionRatio >= 1. {
        setIsVisible(_ => true)
      }
    })

    None
  }, [intersection])

  isVisible
}

let useQueryParams = (~decoder, ~defaultParams) => {
  let queryString = {
    open Browser.Location
    location->search
  }

  React.useMemo(() =>
    queryString
    ->Js.String2.sliceToEnd(~from=1)
    ->Qs.parse
    ->Obj.magic
    ->decoder
    ->Result.getWithDefault(defaultParams)
  , [queryString])
}

type localstorageStateOptions<'state> = {defaultValue?: 'state, storageSync?: bool}

@module("use-local-storage-state")
external useLocalStorageState: (
  string,
  localstorageStateOptions<'state>,
) => ('state, ('state => 'state) => unit) = "default"

external promiseErrorToJsObj: Js.Promise.error => Js.Exn.t = "%identity"

let usePromise = (~debounce=true, fn, deps) => {
  let lastCallId = React.useRef(0)
  let canceled = React.useRef(false)
  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState()

  let trigger = React.useCallback(args => {
    lastCallId.current = lastCallId.current->SafeIncrement.increment

    let callId = lastCallId.current
    set(_ => Loading)

    canceled.current = false

    fn(args)->Promise.map(result => {
      let isCanceled = (debounce ? callId !== lastCallId.current : false) || canceled.current

      if isMounted() && !isCanceled {
        set(_ => Done(result))
      }

      (result, isCanceled)
    })
  }, deps)

  let cancel = React.useCallback(() => canceled.current = true, [])

  (state, trigger, cancel)
}

// V2
// This version is triggered by dependency tracking
// (which is the way to go with Suspense)
// instead of arguments

let usePromiseV2 = (~debounce=true, fn, deps) => {
  let lastCallId = React.useRef(0)
  let canceled = React.useRef(false)
  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState()

  let trigger = React.useCallback(() => {
    lastCallId.current = lastCallId.current->SafeIncrement.increment

    fn()
    ->Option.map(promise => {
      let callId = lastCallId.current
      set(_ => Loading)
      canceled.current = false
      promise->Promise.map(
        result => {
          let isCanceled = (debounce ? callId !== lastCallId.current : false) || canceled.current

          if isMounted() && !isCanceled {
            set(_ => Done(result))
          }

          (result, isCanceled)
        },
      )
    })
    ->ignore
  }, deps)

  let cancel = React.useCallback(() => {
    canceled.current = true
    ()
  }, [])

  React.useEffect(() => {
    trigger()
    None
  }, deps)

  (state, cancel)
}

// V3
// This version is triggered by dependency tracking
// (which is the way to go with Suspense)
// instead of arguments

let usePromiseV3 = (~debounce=true, fn, deps) => {
  let lastCallId = React.useRef(0)
  let canceled = React.useRef(false)
  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState()

  let trigger = React.useCallback(() => {
    lastCallId.current = lastCallId.current->SafeIncrement.increment

    fn()
    ->Option.map(promise => {
      let callId = lastCallId.current
      set(_ => Loading)
      canceled.current = false
      promise->Promise.map(
        result => {
          let isCanceled = (debounce ? callId !== lastCallId.current : false) || canceled.current

          if isMounted() && !isCanceled {
            set(_ => Done(result))
          }

          (result, isCanceled)
        },
      )
    })
    ->ignore
  }, deps)

  let cancel = React.useCallback(() => {
    canceled.current = true
    ()
  }, [])

  React.useEffect(() => {
    trigger()
    None
  }, deps)

  (state, cancel)
}
