open ReactIntl

module Msg = {
  @@intl.messages
  let requiredValue = {defaultMessage: "Champs requis"}
  let requiredPosInt = {defaultMessage: "Doit être un entier positif"}
  let requiredPosIntOrFloat = {
    defaultMessage: "Doit être un entier positif ou un nombre a virgule",
  }
  let requiredRatio = {
    defaultMessage: "Doit être un nombre compris entre 0 et 1 en excluant 0 (exemples: '0.1' '0.85' '1')",
  }
  let wrongFormat = {defaultMessage: "Mauvais format (req: {exemple})"}
  let maxNumberOfPackets = {defaultMessage: "Nbr colis"}
  let totalVolume = {defaultMessage: "Volume total"}
  let binPacking = {defaultMessage: "Bin packing"}
  let vehicleNameAlreadyUsed = {defaultMessage: "Ce nom est deja utilisé dans un autre forfait"}

  let requiredAlphanumericMax4 = {
    defaultMessage: "Format requis : 4 caractères ou nombres (exemples : 'A1b2' 'abcd')",
  }
  let required14Digits = {
    defaultMessage: "Format requis : 14 chiffres",
  }
  let requiredStrictPosInt = {
    defaultMessage: "Doit être un entier strictement positif",
  }

  let stringNonEmpty = {defaultMessage: "Champ requis"}
  let minValue = {
    defaultMessage: "Cette valeur doit être supérieure à {value}",
  }
  let maxValue = {
    defaultMessage: "Cette valeur ne peut être supérieure à {value}",
  }
  let stringMin = {
    defaultMessage: "Le champ doit avoir au minimum {value} caractères.",
  }
  let stringMax = {
    defaultMessage: "Le champ doit avoir au maximum {value} caractères.",
  }
  let email = {defaultMessage: "L'email est invalide."}
  let valueGreaterThan = {
    defaultMessage: "Cette valeur ne peut être supérieure à {value}.",
  }
  let valueLowerThan = {
    defaultMessage: "Cette valeur ne peut être supérieure à {value}.",
  }
  let optionNonEmpty = {defaultMessage: "Champ requis"}
  let invalidValue = {defaultMessage: "Valeur non valide."}
  let passwordTooShort = {
    defaultMessage: "Le mot de passe doit contenir au moins 10 caractères, avoir au moins 1 minuscule, 1 majuscule et un chiffre.",
  }
  let passwordCantBeEqualLogin = {
    defaultMessage: "Veuillez ne pas utiliser votre idenfiant comme mot de passe",
  }
  let passwordCantContainLogin = {
    defaultMessage: "Le mot de passe ne peut pas contenir l'identifiant.",
  }
}

let toReSchemaResult = (opt: option<string>): ReSchema.fieldState<'a> =>
  switch opt {
  | Some(e) => Error(e)
  | None => Valid
  }

let requiredStringNonEmpty = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | _ => None
  }
}

let requiredPosInt = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | value if !Toolkit__Utils_Regex.Test.isPositiveInt(value) =>
    Some(Intl.formatMessage(intl, Msg.requiredPosInt))
  | _ => None
  }
}

let requiredStrictPosInt = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | value if !Toolkit__Utils_Regex.Test.isStrictPositiveInt(value) =>
    Some(Intl.formatMessage(intl, Msg.requiredStrictPosInt))
  | _ => None
  }
}

let optionalPosInt = (intl, value) => {
  switch value {
  | "" => None
  | value if !Toolkit__Utils_Regex.Test.isPositiveInt(value) =>
    Some(Intl.formatMessage(intl, Msg.requiredPosInt))
  | _ => None
  }
}
let optionalStrictPosInt = (intl, value) => {
  switch value {
  | "" => None
  | value if !Toolkit__Utils_Regex.Test.isStrictPositiveInt(value) =>
    Some(Intl.formatMessage(intl, Msg.requiredStrictPosInt))
  | _ => None
  }
}
let requiredPosFloat = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | value if !Toolkit__Utils_Regex.Test.isPositiveIntOrFloat(value) =>
    Some(Intl.formatMessage(intl, Msg.requiredPosIntOrFloat))
  | _ => None
  }
}

let requiredStrictPosFloat = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | value
    if !Toolkit__Utils_Regex.Test.isPositiveIntOrFloat(value) ||
    value->Float.fromString === Some(0.) =>
    Some(Intl.formatMessage(intl, Msg.requiredPosIntOrFloat))
  | _ => None
  }
}

let requiredRatio = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | value
    if !Toolkit__Utils_Regex.Test.isPositiveIntOrFloat(value) ||
    value->Float.fromString === Some(0.) ||
    value->Float.fromString->Option.getWithDefault(2.) > 1. =>
    Some(Intl.formatMessage(intl, Msg.requiredRatio))
  | _ => None
  }
}

let requiredFloat = (intl, value) => {
  switch value->Js.String2.split("-") {
  | _ if value === "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | ["", value]
  | [value] if Toolkit__Utils_Regex.Test.isPositiveIntOrFloat(value) =>
    None
  | _ => Some(Intl.formatMessage(intl, Msg.requiredPosIntOrFloat))
  }
}

let optionalFloat = (intl, value) => {
  switch value->Js.String2.split("-") {
  | _ if value === "" => None
  | ["", value]
  | [value] if Toolkit__Utils_Regex.Test.isPositiveIntOrFloat(value) =>
    None
  | _ => Some(Intl.formatMessage(intl, Msg.requiredPosIntOrFloat))
  }
}

let requiredPosFloatCurrencyEur = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Currency.WithUnit.decodeFromString {
    | Ok(#EUR(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 EUR' '0.5 EUR'"}))
    }
  }
}

let requiredPosFloatTimeHour = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Time.WithUnit.decodeFromString {
    | Ok(#h(_)) => None
    | _ => Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 h' '0.5 h'"}))
    }
  }
}

let requiredPosFloatTimeMin = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Time.WithUnit.decodeFromString {
    | Ok(#min(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 min' '0.5 min'"}))
    }
  }
}

let requiredPosFloatTimeHour_Min = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Time.WithUnit.decodeFromString {
    | Ok(#h(_)) => None
    | Ok(#min(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 min' '0.5 h'"}))
    }
  }
}

let optionalPosFloatTimeHour_Min = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Time.WithUnit.decodeFromString {
    | Ok(#h(_)) => None
    | Ok(#min(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 min' '0.5 h'"}))
    }
  }
}

let optionalPosFloatTimeMin = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Time.WithUnit.decodeFromString {
    | Ok(#min(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 min' '0.5 min'"}))
    }
  }
}

let requiredPosFloatPriceEurByHour = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.CompositeUnits.CurrencyPerTime.WithUnit.decodeFromString {
    | Ok(#EUR_h(_)) => None
    | _ =>
      Some(
        Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 EUR/h' '0.5 EUR/h'"}),
      )
    }
  }
}
let requiredPosFloatPriceEurByKm = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.CompositeUnits.CurrencyPerDistance.WithUnit.decodeFromString {
    | Ok(#EUR_km(_)) => None
    | _ =>
      Some(
        Intl.formatMessageWithValues(
          intl,
          Msg.wrongFormat,
          {"exemple": "'10 EUR/km' '0.5 EUR/km'"},
        ),
      )
    }
  }
}

let requiredPosFloatDistanceKm = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Dimension.WithUnit.decodeFromString {
    | Ok(#km(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 km' '0.5 km'"}))
    }
  }
}
let requiredPosFloatDistanceKm_M = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Dimension.WithUnit.decodeFromString {
    | Ok(#km(_)) => None
    | Ok(#m(_)) => None
    | _ => Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'15 m' '0.5 km'"}))
    }
  }
}

let optionalPosFloatDistanceKm = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Dimension.WithUnit.decodeFromString {
    | Ok(#km(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 km' '0.5 km'"}))
    }
  }
}

let requiredPosFloatVolumeM3 = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Volume.WithUnit.decodeFromString {
    | Ok(#m3(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": `'10 m³' '0.5 m³'`}))
    }
  }
}

let requiredPosFloatDimensionCm = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Dimension.WithUnit.decodeFromString {
    | Ok(#cm(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 cm' '0.5 cm'"}))
    }
  }
}
let requiredPosFloatDimensionCm_M = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Dimension.WithUnit.decodeFromString {
    | Ok(#cm(_)) => None
    | Ok(#m(_)) => None
    | _ => Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 cm' '0.5 m'"}))
    }
  }
}

let requiredPosFloatWeightKg = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Weight.WithUnit.decodeFromString {
    | Ok(#kg(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 kg' '0.5 kg'"}))
    }
  }
}
let optionalPosFloatWeightKg = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Weight.WithUnit.decodeFromString {
    | Ok(#kg(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 kg' '0.5 kg'"}))
    }
  }
}

let optionalPosFloatEnergyCapacityKWh = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.EnergyCapacity.WithUnit.decodeFromString {
    | Ok(#kWh(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 kWh' '0.5 kWh'"}))
    }
  }
}

let optionalPosFloatSpeedKm_h = (intl, value) => {
  switch value {
  | "" => None
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Speed.WithUnit.decodeFromString {
    | Ok(#km_h(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 km/h' '0.5 km/h'"}))
    }
  }
}

let requiredPosFloatWeightG = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch v->Toolkit__Decoders.UnitMeasure.Weight.WithUnit.decodeFromString {
    | Ok(#g(_)) => None
    | _ =>
      Some(Intl.formatMessageWithValues(intl, Msg.wrongFormat, {"exemple": "'10 kg' '0.5 kg'"}))
    }
  }
}

let requiredAlphanumericMax4 = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch Js.Re.test_(%re("/^[a-zA-Z0-9]{4}$/"), v) {
    | true => None
    | false => Some(Intl.formatMessage(intl, Msg.requiredAlphanumericMax4))
    }
  }
}

let required14Digits = (intl, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | v =>
    switch Js.Re.test_(%re("/^[0-9]{14}$/"), v) {
    | true => None
    | false => Some(Intl.formatMessage(intl, Msg.required14Digits))
    }
  }
}
let password = (intl, ~login, value) => {
  switch value {
  | "" => Some(Intl.formatMessage(intl, Msg.requiredValue))
  | p if !Js.Re.test_(%re("/(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{10,}/"), p) =>
    Some(intl->Intl.formatMessage(Msg.passwordTooShort))
  | p if p === login => Some(intl->Intl.formatMessage(Msg.passwordCantBeEqualLogin))
  | p if p->Js.String2.toLowerCase->Js.String2.includes(login->Js.String2.toLowerCase) =>
    Some(intl->Intl.formatMessage(Msg.passwordCantContainLogin))
  | _ => None
  }
}
