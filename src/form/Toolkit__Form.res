open ReactIntl

module ErrorMessage = {
  @react.component
  let make = (~error=?, ~className="") =>
    error->Option.isSome
      ? <p className={cx([className, "text-danger-600 text-sm mt-1 form-error-feedback"])}>
          {error->Option.getExn->React.string}
        </p>
      : React.null
}

module type Config = {
  type error
  type field<'a>
  type state
  let set: (state, field<'a>, 'a) => state
  let get: (state, field<'a>) => 'a
}

module Make = (StateLenses: Config) => {
  include Reform.Make(StateLenses)

  module RadioGroup = {
    @react.component
    let make = (~field, ~elements, ~variant=?, ~inline=?, ~containerClassName=?) =>
      <Field
        field
        render={({handleChange, error, value}) =>
          <React.Fragment>
            <Toolkit__Ui_RadioGroup
              ?containerClassName
              ?variant
              defaultValue=value
              onChange={v => handleChange(v)}
              elements
              ?inline
            />
            <ErrorMessage ?error />
          </React.Fragment>}
      />
  }
  module Input = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name=?,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~type_=?,
      ~containerClassName="",
      ~isInline=false,
      ~inputClassName="",
      ~labelClassName="",
      ~inputContainerClassName="",
      ~step=?,
      ~min=?,
      ~max=?,
      ~inputRightAddon=?,
      ~errorClassName=?,
      ~optionalMessage: option<React.element>=?,
      ~autoComplete=?,
      ~allowWhiteSpace=false,
    ) => {
      let (showPassword, setShowPassword) = React.useState(() => false)
      let isPasswordType = type_->Option.mapWithDefault(false, type_ => type_ === "password")

      <Field
        field
        key={showPassword->string_of_bool}
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onBlur = _ =>
            switch state {
            | Pristine => ()
            | _ => validate()
            }

          <React.Fragment>
            <div
              className={cx([
                "flex",
                isInline ? "flex-row items-center" : "flex-col",
                containerClassName,
              ])}>
              {switch label {
              | None => React.null
              | Some(label) =>
                <Toolkit__Ui.Label
                  htmlFor=id
                  optionalMessage={isOptional->Option.getWithDefault(false)
                    ? optionalMessage->Option.getWithDefault(React.null)
                    : React.null}
                  className=labelClassName>
                  label
                </Toolkit__Ui.Label>
              }}
              <div className={cx(["flex flex-row", inputContainerClassName])}>
                <Toolkit__Ui.TextInput
                  ?autoComplete
                  className={cx([
                    inputClassName,
                    inputRightAddon->Option.isSome || isPasswordType ? "rounded-r-none" : "",
                  ])}
                  id
                  value
                  ?name
                  type_=?{type_->Option.map(type_ =>
                    type_ === "password" && showPassword ? "text" : type_
                  )}
                  ?disabled
                  ?placeholder
                  allowWhiteSpace
                  ?autoFocus
                  ?step
                  ?min
                  ?max
                  isInvalid
                  onChangeText={handleChange}
                  onBlur
                />
                {type_->Option.mapWithDefault(React.null, type_ => {
                  switch type_ {
                  | "password" =>
                    <Toolkit__Ui_Tooltip
                      triggerAsChild=true
                      contentClassName={"min-w-[200px] text-center"}
                      tooltipContent={showPassword
                        ? <ReactIntl.FormattedMessage defaultMessage="Masquer le mot de passe" />
                        : <ReactIntl.FormattedMessage defaultMessage="Afficher le mot de passe" />}
                      triggerContent={<button
                        type_="button"
                        className="p-1 bg-neutral-300 rounded-r border border-gray-300 text-neutral-800"
                        onClick={_ => setShowPassword(v => !v)}>
                        {showPassword
                          ? <ReactIcons.AiFillEyeInvisible size={26} />
                          : <ReactIcons.AiFillEye size={26} />}
                      </button>}
                    />

                  | _ => React.null
                  }
                })}
                {inputRightAddon->Option.getWithDefault(React.null)}
              </div>
            </div>
            <ErrorMessage ?error className=?errorClassName />
          </React.Fragment>
        }}
      />
    }
  }

  module Int = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name=?,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~step=?,
      ~min=?,
      ~max=?,
      ~className=?,
    ) => {
      let parseInt: string => option<int> = %raw(`
          x => {
            var result = parseInt(x, 10);
            return isNaN(result) ? undefined : result;
          }
        `)

      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onChange = event =>
            ReactEvent.Form.target(event)["value"]->Option.flatMap(parseInt)->handleChange

          let onBlur = _ =>
            switch state {
            | Pristine => ()
            | _ => validate()
            }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_TextInput
              ?className
              id
              value={value->Option.map(Js.Int.toString)->Option.getWithDefault("")}
              type_="number"
              ?name
              ?disabled
              ?placeholder
              ?autoFocus
              ?step
              ?min
              ?max
              isInvalid
              onChange
              onBlur
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module DateTime = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name=?,
      ~startDate=?,
      ~endDate=?,
      ~selectsStart=?,
      ~selectsEnd=?,
      ~minDate=?,
      ~showTimeSelect=?,
      ~showYearDropdown=?,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~timeCaption=?,
      ~timeFormat=?,
      ~minTime=?,
      ~maxTime=?,
      ~timeIntervals=?,
      ~inline=?,
    ) =>
      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onChange = value => handleChange(value->Js.Nullable.toOption)

          let onBlur = _ =>
            switch state {
            | Pristine => ()
            | _ => validate()
            }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_DatetimeInput
              ?value
              ?showTimeSelect
              ?showYearDropdown
              ?disabled
              ?startDate
              ?endDate
              ?inline
              ?selectsStart
              ?timeFormat
              ?timeCaption
              ?timeIntervals
              ?selectsEnd
              ?minTime
              ?maxTime
              ?minDate
              id
              ?name
              ?placeholder
              ?autoFocus
              onBlur
              onChange
              isInvalid
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
  }

  module Textarea = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~variant=#default,
      ~id,
      ~name=?,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~className=?,
    ) =>
      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onChange = event => handleChange(ReactEvent.Form.target(event)["value"])

          let onBlur = _ =>
            switch state {
            | Pristine => ()
            | _ => validate()
            }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_TextareaInput
              ?className
              id
              variant
              value
              ?name
              ?disabled
              ?placeholder
              ?autoFocus
              isInvalid
              onChange
              onBlur
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
  }

  module Checkbox = {
    @react.component
    let make = (~field, ~label, ~name=?, ~disabled=?, ~className=?) =>
      <Field
        field
        render={({handleChange, error, value}) =>
          <React.Fragment>
            <Toolkit__Ui_Checkbox
              ?className
              checked={value->Obj.magic}
              value={value->Obj.magic}
              ?name
              ?disabled
              onChange={(checked, _value) => handleChange(checked)}>
              label
            </Toolkit__Ui_Checkbox>
            <ErrorMessage ?error />
          </React.Fragment>}
      />
  }

  module Select = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name=?,
      ~options: Toolkit__Ui_Select.options,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~className=?,
    ) => {
      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onBlur = _ => {
            switch state {
            | Pristine => ()
            | _ => validate()
            }
          }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_Select
              options
              ?name
              ?placeholder
              ?autoFocus
              ?className
              isDisabled=?{disabled}
              onBlur
              id
              isInvalid
              value
              onChange={selectedValue => {
                handleChange(selectedValue)
              }}
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module SelectPolyvariant = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name=?,
      ~options: Toolkit__Ui_SelectPolyvariant.options<'value>,
      ~encodeValueToString: 'value => string,
      ~decodeValueFromString,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~isOptional=?,
      ~className=?,
    ) => {
      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let isInvalid = error->Option.isSome

          let onBlur = _ => {
            switch state {
            | Pristine => ()
            | _ => validate()
            }
          }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_SelectPolyvariant
              options
              ?name
              ?placeholder
              ?autoFocus
              ?className
              isDisabled=?{disabled}
              onBlur
              id
              isInvalid
              ?value
              encodeValueToString
              decodeValueFromString
              onChange={selectedValue => {
                handleChange(selectedValue)
              }}
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module SearchSelect = {
    open! ReactSelect

    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~name,
      ~options,
      ~placeholder=?,
      ~isDisabled=?,
      ~className="",
      ~isOptional=?,
      ~valueFormat,
      ~autoFocus=?,
    ) => {
      <Field
        field
        render={({handleChange, error, value}) => {
          let isInvalid = error->Option.isSome

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <ReactSelect
              defaultMenuIsOpen=?autoFocus
              ?autoFocus
              name
              ?isDisabled
              classNamePrefix="react-select"
              className={cx([className, isInvalid ? "errored" : ""])}
              ?placeholder
              onChange={o => handleChange(Some(o.value))}
              value=?{value->Option.map(valueFormat)}
              options
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module MultiSelect = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~options: Toolkit__Ui_MultiSelect.options,
      ~placeholder,
      ~disabled=?,
      ~isOptional=?,
      ~buttonClassName=?,
      ~dropdownClassName=?,
      ~itemClassName=?,
    ) => {
      <Field
        field
        render={({handleChange, error, value, validate, state}) => {
          let onClose = _ => {
            switch state {
            | Pristine => ()
            | _ => validate()
            }
          }

          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <Toolkit__Ui_MultiSelect
              ?buttonClassName
              ?dropdownClassName
              ?itemClassName
              options
              defaultValue={value}
              ?disabled
              onChange={items => handleChange(items)}
              onClose
              placeholder
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module Switch = {
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~size=?,
      ~displayTitle=?,
      ~switchClassName=?,
      ~labelClassName=?,
      ~color=?,
    ) =>
      <Field
        field
        render={({handleChange, value}) =>
          <React.Fragment>
            <Toolkit__Ui_Switch
              ?label
              ?color
              onChange={value => handleChange(value)}
              name=id
              checked=value
              ?size
              ?displayTitle
              ?switchClassName
              className=?{labelClassName}
            />
          </React.Fragment>}
      />
  }

  module DatePicker = {
    open ReactDayPicker
    @react.component
    let make = (
      ~field,
      ~label=?,
      ~id,
      ~isOptional=?,
      ~containerClassName="",
      ~placeholder=?,
      ~disabledBefore=?,
    ) => {
      <Field
        field
        render={({handleChange, value, error}) => {
          <React.Fragment>
            {switch label {
            | None => React.null
            | Some(label) =>
              <Toolkit__Ui_Label
                htmlFor=id
                optionalMessage={isOptional->Option.getWithDefault(false)
                  ? <FormattedMessage defaultMessage="(Optionnel)" />
                  : React.null}>
                label
              </Toolkit__Ui_Label>
            }}
            <ReactDayPicker.SingleDayPickerInput
              ?value
              onChange={v => handleChange(v)}
              allowEmpty=?isOptional
              buttonClassName="w-full"
              labelClassName={cx([
                "flex flex-row items-center gap-4 font-normal text-neutral-700 w-full",
                containerClassName,
              ])}
              modifiers=?{disabledBefore->Option.map((disabledBefore): ReactDayPicker.modifiers => {
                disabled: [
                  ReactDayPicker.Matcher.interval({
                    before: disabledBefore,
                  }),
                ],
              })}
              placeholder={placeholder->Option.getWithDefault(
                <FormattedMessage defaultMessage="Choisissez une date" />,
              )}
              labelFormatter={date => {
                <React.Fragment>
                  <span>
                    <FormattedDate
                      value={date} weekday=#long day=#numeric month=#long year=#numeric
                    />
                  </span>
                  <div
                    className="border inline-flex items-center justify-center w-8 h-8 rounded border-primary-700 text-primary-700">
                    <ReactIcons.FaPencilAlt size={18} />
                  </div>
                </React.Fragment>
              }}
            />
            <ErrorMessage ?error />
          </React.Fragment>
        }}
      />
    }
  }

  module Phone = {
    @react.component
    let make = (
      ~field,
      ~international=true,
      ~defaultCountry="FR",
      ~label,
      ~isOptional=false,
      ~id,
    ) =>
      <Field
        field
        render={({value, handleChange, validate, error}) => {
          <>
            <Toolkit__Ui_Label
              htmlFor=id
              optionalMessage={isOptional
                ? <FormattedMessage defaultMessage="(Optionnel)" />
                : React.null}>
              {label}
            </Toolkit__Ui_Label>
            <ReactPhoneNumberInput
              international
              className={cx([
                "bg-white border rounded",
                error->Option.isSome ? "border-danger-500" : "",
              ])}
              id
              value
              countryCallingCodeEditable={false}
              defaultCountry
              onChange={handleChange}
              onBlur={_ => validate()}
            />
            {error->Option.mapWithDefault(React.null, error => {
              <ErrorMessage error />
            })}
          </>
        }}
      />
  }
}
