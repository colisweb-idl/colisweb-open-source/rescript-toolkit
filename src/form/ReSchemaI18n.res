type t = {
  false_: unit => string,
  true_: unit => string,
  intMin: (~value: int, ~min: int) => string,
  intMax: (~value: int, ~max: int) => string,
  floatMin: (~value: float, ~min: float) => string,
  floatMax: (~value: float, ~max: float) => string,
  email: (~value: string) => string,
  stringNonEmpty: (~value: string) => string,
  stringRegExp: (~value: string, ~pattern: string) => string,
  stringMin: (~value: string, ~min: int) => string,
  stringMax: (~value: string, ~max: int) => string,
  phone: string,
  passwordTooShort: string,
  passwordCantContainLogin: string,
}

let default = {
  false_: () => "Cette valeur doit être fausse",
  true_: () => "Cette valeur doit être vraie",
  intMin: (~value as _value, ~min) =>
    `Cette valeur doit être supérieur ou égale à ${min->Int.toString}`,
  intMax: (~value as _value, ~max) =>
    `Cette valeur doit être inférieur ou égale à ${max->Int.toString}`,
  floatMin: (~value as _value, ~min) =>
    `Cette valeur doit être supérieur ou égale à ${min->Js.Float.toString}`,
  floatMax: (~value as _value, ~max) =>
    `Cette valeur doit être supérieur ou égale à ${max->Js.Float.toString}`,
  email: (~value) => `${value} n'est pas un e-mail valide`,
  stringNonEmpty: (~value as _) => "Champs requis",
  stringRegExp: (~value as _value, ~pattern) =>
    `Cette valeur doit correspondre à ce pattern /${pattern}/`,
  stringMin: (~value as _value, ~min) =>
    `Le champ doit avoir au minimum ${min->Int.toString} caractères.`,
  stringMax: (~value as _value, ~max) =>
    `Le champ doit avoir au maximum ${max->Int.toString} caractères.`,
  phone: "Format du numéro invalide. Ex : +33612345678",
  passwordTooShort: "Le mot de passe doit contenir au moins 10 caractères, avoir au moins 1 minuscule, 1 majuscule et un chiffre.",
  passwordCantContainLogin: "Le mot de passe ne peut pas contenir l'identifiant.",
}
