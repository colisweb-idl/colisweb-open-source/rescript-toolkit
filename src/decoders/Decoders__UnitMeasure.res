module type MakeConfig = {
  type t
  let toString: t => string
  let toValue: t => float
  let display: (t, ~digits: int=?, unit) => string

  let wrapValue: float => t
}

module Make = (C: MakeConfig) => {
  type dimension = C.t

  let encoder: Spice.encoder<C.t> = value => value->C.toString->Spice.stringToJson

  let decoder: Spice.decoder<C.t> = json =>
    switch Spice.floatFromJson(json) {
    | Ok(v) => v->C.wrapValue->Ok
    | Error(_) as err => err
    }

  let codec: Spice.codec<dimension> = (encoder, decoder)

  let toValue = C.toValue
  let display = C.display
  let toString = C.toString

  @spice
  type t = @spice.codec(codec) C.t
}

module type MakeWithUnitConfig = {
  type t
  let toString: t => string
  let toValue: t => float
  let display: (t, ~digits: int=?, unit) => string

  module Unit: {
    type t
    let t_decode: Js.Json.t => result<t, Spice.decodeError>
    let t_encode: t => Js.Json.t
  }

  let wrapValue: (Unit.t, float) => t

  let errorPrefix: string
}

module MakeWithUnit = (Config: MakeWithUnitConfig) => {
  module Unit = Config.Unit
  let decodeFromString = (string: string) => {
    // match a positive int or float followed by it's unit (capturing int/float in group1 and unit in group2)
    let regexp = %re("/^(-?\d+(?:\.\d*)?)\s*(\D*)$/")

    // regexpCaptures: [match , captureGroup1, captureGroup2]
    let regexpCaptures =
      Js.Re.exec_(regexp, string)->Belt.Option.map(r =>
        r->Js.Re.captures->Array.map(Js.Nullable.toOption)
      )

    let jsonString = string->Js.Json.string

    let errorPrefix = Config.errorPrefix

    switch regexpCaptures {
    | None => Spice.error(`${errorPrefix} unhandled format`, jsonString)
    | Some([None, _value, _unit]) => Spice.error(`${errorPrefix} unhandled format`, jsonString)
    | Some([_match, _value, None]) => Spice.error(`${errorPrefix} unhandled no unit`, jsonString)
    | Some([_match, None, _unit]) => Spice.error(`${errorPrefix} unhandled no value`, jsonString)
    | Some([Some(_match), Some(value), Some(unit)]) => {
        let value = value->Js.Float.fromString
        switch unit
        // regex captures adjacent to spaces to slashes and deletes them (for composite units)
        ->Js.String2.replaceByRe(%re("/ *\/ */"), "/")
        ->Js.Json.string
        ->Config.Unit.t_decode {
        | Ok(unit) => Config.wrapValue(unit, value)->Ok
        | Error(_) => Spice.error(`${errorPrefix} unhandled unit '${unit}'`, jsonString)
        }
      }

    | Some(_) => Spice.error(`${errorPrefix} regex capture group error`, jsonString)
    }
  }

  %%private(
    let encoder: Spice.encoder<Config.t> = value => value->Config.toString->Spice.stringToJson
    let decoder: Spice.decoder<Config.t> = json =>
      switch Spice.stringFromJson(json) {
      | Ok(v) => v->decodeFromString
      | Error(_) as err => err
      }
    let codec: Spice.codec<Config.t> = (encoder, decoder)
  )

  let toValue = Config.toValue
  let toString = Config.toString
  let display = Config.display

  @spice
  type t = @spice.codec(codec) Config.t
}

module Dimension = {
  module Config = Toolkit__Utils_UnitMeasure.Dimension

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Dimension with unit"
  })

  module Mm = Make({
    include Config
    let wrapValue = Config.wrapValue(Mm, ...)
  })
  module Cm = Make({
    include Config
    let wrapValue = Config.wrapValue(Cm, ...)
  })
  module Dm = Make({
    include Config
    let wrapValue = Config.wrapValue(Dm, ...)
  })
  module M = Make({
    include Config
    let wrapValue = Config.wrapValue(M, ...)
  })
  module Km = Make({
    include Config
    let wrapValue = Config.wrapValue(Km, ...)
  })
}

module Speed = {
  module Config = Toolkit__Utils_UnitMeasure.Speed

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Speed with unit"
  })

  module Km_h = Make({
    include Config
    let wrapValue = Config.wrapValue(Km_h, ...)
  })
}

module Volume = {
  module Config = Toolkit__Utils_UnitMeasure.Volume

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Volume with unit"
  })

  module M3 = Make({
    include Config
    let wrapValue = Config.wrapValue(M3, ...)
  })
}

module EnergyCapacity = {
  module Config = Toolkit__Utils_UnitMeasure.EnergyCapacity

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "EnergyCapacity with unit"
  })

  module KWh = Make({
    include Config
    let wrapValue = Config.wrapValue(Kwh, ...)
  })
}

module Weight = {
  module Config = Toolkit__Utils_UnitMeasure.Weight

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Weight with unit"
  })

  module G = Make({
    include Config
    let wrapValue = Config.wrapValue(Gram, ...)
  })
  module Kg = Make({
    include Config
    let wrapValue = Config.wrapValue(Kg, ...)
  })
}

module Time = {
  module Config = Toolkit__Utils_UnitMeasure.Time

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Time with unit"
  })

  module Min = Make({
    include Config
    let wrapValue = Config.wrapValue(Min, ...)
  })
  module H = Make({
    include Config
    let wrapValue = Config.wrapValue(Hour, ...)
  })
}

module Currency = {
  module Config = Toolkit__Utils_UnitMeasure.Currency

  module WithUnit = MakeWithUnit({
    include Config
    let errorPrefix = "Curency with unit"
  })

  module EUR = Make({
    include Config
    let wrapValue = Config.wrapValue(EUR, ...)
  })
}

module CompositeUnits = {
  module CurrencyPerDistance = {
    module Config = Toolkit__Utils_UnitMeasure.CompositeUnits.CurrencyPerDistance

    module WithUnit = MakeWithUnit({
      include Config
      let errorPrefix = "Curency per distance with unit"
    })

    module EUR_km = Make({
      include Config
      let wrapValue = Config.wrapValue(EUR_km, ...)
    })
  }
  module CurrencyPerTime = {
    module Config = Toolkit__Utils_UnitMeasure.CompositeUnits.CurrencyPerTime

    module WithUnit = MakeWithUnit({
      include Config
      let errorPrefix = "Curency per time with unit"
    })

    module EUR_h = Make({
      include Config
      let wrapValue = Config.wrapValue(EUR_h, ...)
    })
  }
}
