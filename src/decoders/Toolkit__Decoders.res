module Option = {
  let encoder = (encoder, value) =>
    switch value {
    | None => %raw("undefined")
    | Some(value) => encoder(value)
    }

  let decoder = (decoder, json) =>
    json !== %raw("undefined") ? Spice.optionFromJson(decoder, json) : Ok(None)

  let codec = (encoder, decoder)

  @spice
  type t<'a> = @spice.codec((encoder, decoder)) option<'a>
}

module Coordinates = {
  @spice
  type t = {
    latitude: float,
    longitude: float,
  }
  let make = (~lat: float, ~lng: float): t => {latitude: lat, longitude: lng}
  let areEquals = (a: t, b: t): bool => a.latitude === b.latitude && a.longitude === b.longitude
  let fromJs = value => {
    latitude: (value->Obj.magic)["lat"](),
    longitude: (value->Obj.magic)["lng"](),
  }
  type gmaps = {
    lat: float,
    lng: float,
  }
  let toGmaps = (t: t): gmaps => {lat: t.latitude, lng: t.longitude}
  let toHere = (t: t): Here.coordinates => {lat: t.latitude, lng: t.longitude}
  let fromGmaps = (gmaps: gmaps) => {
    latitude: gmaps.lat,
    longitude: gmaps.lng,
  }
}

module Datetime = {
  type date = Js.Date.t

  let encoder: Spice.encoder<date> = value => value->Js.Date.toISOString->Spice.stringToJson

  let decoder: Spice.decoder<date> = json =>
    switch Spice.stringFromJson(json) {
    | Ok(value) => Ok(value->Js.Date.fromString)
    | Error(_) as error => error
    }

  let codec: Spice.codec<date> = (encoder, decoder)

  @spice
  type t = @spice.codec(codec) date
}

module Date = {
  type date = Js.Date.t

  let encoder: Spice.encoder<date> = date =>
    date->DateFns.formatWithPattern("yyyy-MM-dd")->Spice.stringToJson

  let decoder: Spice.decoder<date> = json =>
    switch Spice.stringFromJson(json) {
    | Ok(v) => Js.Date.fromString(v)->Ok
    | Error(_) as err => err
    }

  let codec: Spice.codec<date> = (encoder, decoder)

  @spice
  type t = @spice.codec(codec) date
}

module DatetimeTimeSlot = {
  @spice
  type t = {
    start: Datetime.t,
    @as("end") @spice.key("end")
    end_: Datetime.t,
  }

  let isEqual = (a: t, b: t): bool => {
    a.start->Js.Date.toString === b.start->Js.Date.toString &&
      a.end_->Js.Date.toString === b.end_->Js.Date.toString
  }
}

module DateTimeSlot = {
  @spice
  type t = {
    start: Date.t,
    @as("end") @spice.key("end")
    end_: Date.t,
  }

  let isEqual = (a: t, b: t): bool => {
    a.start->Js.Date.toString === b.start->Js.Date.toString &&
      a.end_->Js.Date.toString === b.end_->Js.Date.toString
  }
}

module Float = {
  let encoder = value => value->Spice.floatToJson

  let decoder = json =>
    switch Js.Json.classify(json) {
    | JSONNumber(_) =>
      switch Spice.floatFromJson(json) {
      | Ok(_) as value => value
      | Error(_) as error => error
      }
    | JSONString(_) =>
      switch Spice.stringFromJson(json) {
      | Ok(value) =>
        switch Spice.floatFromJson(value->Js.Float.fromString->Js.Json.number) {
        | Ok(_) as value => value
        | Error(_) as error => error
        }
      | Error(_) => Spice.error(~path="", "Not a number", json)
      }
    | _ => Spice.error(~path="", "Not a number", json)
    }

  let codec = (encoder, decoder)

  @spice
  type t = @spice.codec(codec) float
}

module Int = {
  let encoder = value => value->Spice.intToJson

  let decoder = json =>
    switch Js.Json.classify(json) {
    | JSONNumber(_) =>
      switch Spice.intFromJson(json) {
      | Ok(_) as value => value
      | Error(_) as error => error
      }
    | JSONString(_) =>
      switch Spice.stringFromJson(json) {
      | Ok(value) =>
        switch Spice.intFromJson(value->Js.Float.fromString->Js.Json.number) {
        | Ok(_) as value => value
        | Error(_) as error => error
        }
      | Error(_) => Spice.error(~path="", "Not an integer", json)
      }
    | _ => Spice.error(~path="", "Not a number", json)
    }

  let codec = (encoder, decoder)

  @spice
  type t = @spice.codec(codec) int
}

module Boolean = {
  let encoder = value => value->Spice.boolToJson

  let decoder = json =>
    switch Js.Json.classify(json) {
    | JSONFalse => Ok(false)
    | JSONTrue => Ok(true)
    | JSONString(_) =>
      switch Spice.stringFromJson(json) {
      | Ok(value) =>
        switch Spice.boolFromJson(value->bool_of_string->Js.Json.boolean) {
        | Ok(_) as value => value
        | Error(_) as error => error
        }
      | Error(_) => Spice.error(~path="", "Not a boolean", json)
      }
    | _ => Spice.error(~path="", "Not a boolean", json)
    }

  let codec = (encoder, decoder)

  @spice
  type t = @spice.codec(codec) bool
}

module UnitMeasure = Decoders__UnitMeasure

module StringArray = {
  let encoder = (encoder, value) =>
    value->Array.map(encoder)->Js.Array2.joinWith(",")->Spice.stringToJson

  let decoder = (decoder, json) =>
    switch json->Spice.stringFromJson {
    | Ok(string) =>
      let resultArray = string->Js.String2.split(",")->Array.map(e => e->Js.Json.string->decoder)
      let error = resultArray->Array.getBy(Result.isError)
      switch error {
      | Some(_) => Spice.error(~path="", "Invalid content", json)
      | None => Ok(resultArray->Array.map(Result.getExn))
      }
    | Error(_) as error => error
    }

  let codec = (encoder, decoder)

  @spice
  type t<'a> = @spice.codec((encoder, decoder)) array<'a>
}
