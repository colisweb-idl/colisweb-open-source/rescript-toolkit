type size = [#xs | #sm | #md | #lg | #xl]

type color = [
  | #current
  | #black
  | #white
  | #gray
  | #success
  | #primary
  | #info
  | #danger
  | #warning
]

@react.component
let make = (~size=#md, ~color=#black, ~label="Loading...", ~className="", ~style=?) => {
  let size = switch size {
  | #xs => "w-3 h-3"
  | #sm => "w-4 h-4"
  | #md => "w-6 h-6"
  | #lg => "w-8 h-8"
  | #xl => "w-12 h-12"
  }

  let color = switch color {
  | #black => "border-black"
  | #white => "border-white"
  | #gray => "border-gray-800"
  | #success => "border-success-500"
  | #primary => "border-primary-700"
  | #info => "border-info-500"
  | #danger => "border-danger-500"
  | #warning => "border-warning-500"
  | #current => "border-current-color"
  }

  <div
    ?style
    className={cx([
      className,
      "spin",
      "inline-block border-t-2 border-r-2 rounded-full",
      color,
      size,
    ])}>
    <Toolkit__Ui_VisuallyHidden> {label->React.string} </Toolkit__Ui_VisuallyHidden>
  </div>
}
