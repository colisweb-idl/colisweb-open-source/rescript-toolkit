open ReactIntl

module PasswordHint = {
  @react.component
  let make = (~isValid, ~children) => {
    <div
      className={cx([
        "flex flex-row gap-1 items-center transition-colors duration-150 ease-in",
        isValid ? "text-success-700 font-semibold" : "",
      ])}>
      <ReactIcons.MdCheckCircle
        className={cx([
          "transition-colors duration-150 ease-in",
          isValid ? "text-success-700" : "text-info-400",
        ])}
      />
      children
    </div>
  }
}
@react.component
let make = (~login=?, ~password) => {
  let passwordHasEnoughCharacters = password->Js.String2.length >= 10
  let passwordHasMinusCharacter = React.useMemo(
    () => {Js.Re.test_(%re("/(?=.*[a-z])./"), password)},
    [password],
  )
  let passwordHasUpperCharacter = React.useMemo(
    () => Js.Re.test_(%re("/(?=.*[A-Z])./"), password),
    [password],
  )
  let passwordHasDigit = React.useMemo(() => Js.Re.test_(%re("/(?=.*\d)./"), password), [password])

  <div
    className="bg-info-50 rounded-lg p-2 flex flex-row items-center gap-2 border border-info-600">
    <ReactIcons.FaInfoCircle size={30} className="text-info-600 mr-2" />
    <div className="text-info-600">
      <p className="mb-2 font-semibold">
        <FormattedMessage defaultMessage={"Le mot de passe doit contenir :"} />
      </p>
      <div className="grid  grid-cols-1 lg:grid-cols-2 text-sm gap-2">
        <PasswordHint isValid={passwordHasEnoughCharacters}>
          <FormattedMessage defaultMessage={"Au moins 10 caractères"} />
        </PasswordHint>
        <PasswordHint isValid={passwordHasMinusCharacter}>
          <FormattedMessage defaultMessage={"Au moins 1 minuscule"} />
        </PasswordHint>
        <PasswordHint isValid={passwordHasDigit}>
          <FormattedMessage defaultMessage={"Au moins 1 chiffre"} />
        </PasswordHint>
        <PasswordHint isValid={passwordHasUpperCharacter}>
          <FormattedMessage defaultMessage={"Au moins 1 majuscule"} />
        </PasswordHint>
      </div>
      {login->Option.mapWithDefault(React.null, login => {
        let passwordDontContainLogin = !(
          password
          ->Js.String2.toLowerCase
          ->Js.String2.includes(login->Js.String2.toLowerCase)
        )

        <>
          <p className="mt-4 mb-2 font-semibold">
            <FormattedMessage defaultMessage={"Votre mot de passe ne doit pas contenir :"} />
          </p>
          <div className="grid grid-cols-1 lg:grid-cols-2 text-sm gap-2">
            <PasswordHint isValid={passwordDontContainLogin && password !== ""}>
              <FormattedMessage defaultMessage={"Votre identifiant"} />
            </PasswordHint>
          </div>
        </>
      })}
    </div>
  </div>
}
