type size = [#sm | #md | #lg]
type color =
  | Primary
  | Black

@react.component
let make = (
  ~onChange=?,
  ~name=?,
  ~checked=?,
  ~disabled=?,
  ~size=#md,
  ~checkedClassName="",
  ~label=?,
  ~className="",
  ~switchClassName="",
  ~disabledClassName="",
  ~checkedSwitchClassName="",
  ~disabledSwitchClassName="",
  ~switchRef: option<ReactDOM.domRef>=?,
  ~displayTitle=false,
  ~color=Primary,
) => {
  let (isChecked, setIsChecked) = React.useState(() => checked->Option.getWithDefault(false))
  let previousChecked = Toolkit__Hooks.usePrevious(checked)

  React.useEffect(() => {
    checked->Option.forEach(checked => {
      switch (previousChecked, checked) {
      | (Some(Some(c1)), c2) if c1 != c2 => setIsChecked(_ => c2)
      | _ => ()
      }
    })

    None
  }, (checked, previousChecked))

  <label
    ref=?switchRef
    className={cx([
      "flex flex-row items-center gap-2 cursor-pointer flex-shrink-0",
      className,
      isChecked ? `${checkedClassName} cw-switch--checked` : "",
      disabled->Option.getWithDefault(false) ? disabledClassName : "",
    ])}>
    <span
      className={cx([
        "shrink-0 flex items-center relative rounded-full py-[2px] cursor-pointer border transition-all",
        switch size {
        | #sm => "w-7"
        | #md if displayTitle => "w-[56px] h-[22px]"
        | #md => "w-[44px] h-[22px]"
        | #lg if displayTitle => "w-[87px] [h-32px]"
        | #lg => "w-[66px] [h-32px]"
        },
        switch color {
        | Primary => isChecked ? "bg-primary-700 border-primary-700" : "bg-neutral-500"
        | Black => isChecked ? "bg-black border-black" : "bg-neutral-500"
        },
        switchClassName,
        isChecked ? checkedSwitchClassName : "",
        disabled->Option.getWithDefault(false) ? disabledSwitchClassName : "",
      ])}>
      <Toolkit__Ui_VisuallyHidden>
        <input
          ?disabled
          type_="checkbox"
          className="mr-3 h-6"
          onChange={_ => {
            setIsChecked(v => !v)
            onChange->Option.map(fn => fn(!isChecked))->ignore
          }}
          ?name
          ?checked
        />
      </Toolkit__Ui_VisuallyHidden>
      {displayTitle
        ? <span
            className={cx([
              "uppercase absolute text-white font-semibold",
              switch (size, isChecked) {
              | (#sm, _) => "hidden"
              | (#md, true) => "text-xs ml-2"
              | (#md, false) => "text-xs ml-[21px]"
              | (#lg, true) => "text-lg ml-2"
              | (#lg, false) => "text-lg ml-8"
              },
            ])}>
            {isChecked
              ? <ReactIntl.FormattedMessage defaultMessage={"Oui"} />
              : <ReactIntl.FormattedMessage defaultMessage={"Non"} />}
          </span>
        : React.null}
      <span
        className={cx([
          "rounded-full shadow transition duration-200 ease-linear transform bg-white",
          switch size {
          | #sm => "w-3 h-3"
          | #md => "w-[16px] h-[16px]"
          | #lg => "w-6 h-6"
          },
          switch size {
          | #sm if isChecked => "translate-x-[12px]"
          | #sm => "translate-x-[2px]"
          | #md =>
            switch (isChecked, displayTitle) {
            | (true, true) => "translate-x-[36px]"
            | (true, false) => "translate-x-[24px]"
            | (false, _) => "translate-x-[2px]"
            }
          | #lg =>
            switch (isChecked, displayTitle) {
            | (true, true) => "translate-x-[58px]"
            | (true, false) => "translate-x-[24px]"
            | (false, _) => "translate-x-[3px]"
            }
          },
        ])}
      />
    </span>
    {label->Option.mapWithDefault(React.null, label => {
      label
    })}
  </label>
}
