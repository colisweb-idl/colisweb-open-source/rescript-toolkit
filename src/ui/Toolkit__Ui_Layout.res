module Container = {
  @react.component
  let make = (~children) =>
    <div className="max-w-[1140px] mx-auto px-2 md:px-0 pb-6"> children </div>
}

module Footer = {
  @react.component
  let make = (~hasSidebar=false, ~className="", ~children=?) =>
    <footer
      className={cx([
        className,
        "flex-none text-gray-600 text-center text-lg pt-4 print:hidden mt-auto",
        hasSidebar ? "lg:ml-20" : "",
      ])}>
      {switch children {
      | None =>
        (`© Colisweb ` ++ Js.Date.make()->Js.Date.getFullYear->Js.Float.toString)->React.string
      | Some(children) => children
      }}
    </footer>
}

module NavOpenContext = {
  let context = React.createContext(false)

  module Provider = {
    let make = React.Context.provider(context)
  }
}

module App = {
  let localStorageNavKey = "@colisweb/navOpen"

  module TopNavigationBar = {
    @react.component
    let make = (~toggleMenu, ~username=?, ~logoSrc=?, ~onLogoClick, ~logout=?, ~environment=?) => {
      <div
        className="bg-white h-16 border-b flex items-center px-1 justify-between fixed top-0 left-0 w-full z-40 print:hidden">
        <div className="flex items-center px-1 gap-1">
          <button
            onClick=toggleMenu className="text-neutral-800 hover:bg-neutral-300 rounded-full p-2">
            <ReactIcons.MdMenu size=32 />
          </button>
          <a
            href="#"
            onClick={event => {
              event->ReactEvent.Mouse.preventDefault

              onLogoClick()
            }}>
            <img
              src={logoSrc->Option.getWithDefault("/assets/logo.svg")}
              alt="Colisweb"
              className="w-16 ml-4"
            />
          </a>
          {environment->Option.mapWithDefault(React.null, environment => {
            <p className="text-neutral-600 font-semibold text-lg font-display">
              {environment->React.string}
            </p>
          })}
        </div>
        {switch (username, logout) {
        | (Some(username), Some(logout)) =>
          <Toolkit__Ui_Dropdown
            buttonClassName="border-none focus:!shadow-none active:!shadow-none"
            dropdownClassName={"w-40 -mt-1 !shadow-[0_0_8px_rgba(44,44,44,0.20)]"}
            label={<div className="flex items-center gap-2 text-neutral-800">
              <span className="font-semibold truncate max-w-[150px]">
                {username->React.string}
              </span>
              <div>
                <ReactIcons.MdAccountCircle size=28 />
              </div>
              <div>
                <ReactIcons.MdKeyboardArrowDown size=18 />
              </div>
            </div>}>
            <button
              className="py-1 text-base text-neutral-700 font-semibold block w-full hover:bg-neutral-200"
              onClick={_ => logout()}>
              <ReactIntl.FormattedMessage defaultMessage="Déconnexion" />
            </button>
          </Toolkit__Ui_Dropdown>

        | _ => React.null
        }}
      </div>
    }
  }

  module LeftNavigationBar = {
    type sideNavRender = {
      onLinkClick: unit => unit,
      isNavOpen: bool,
      openMenu: unit => unit,
    }

    @react.component
    let make = (
      ~isNavOpen,
      ~bottom=?,
      ~hideMenu,
      ~children: sideNavRender => React.element,
      ~openMenu,
    ) => {
      let {isLg} = Toolkit__Hooks.useMediaQuery()

      let onLinkClick = () => isLg ? () : hideMenu()

      <React.Fragment>
        <nav
          className={cx([
            "sidenav",
            "lg:flex border-r px-2 py-3 fixed flex-col bg-white justify-between transition-all duration-300 ease-in-out z-40 print:hidden",
            isNavOpen ? "w-64 overflow-y-auto" : "w-16 hidden sidenav--closed",
          ])}>
          {children({onLinkClick, isNavOpen, openMenu})}
          {bottom->Option.mapWithDefault(React.null, content =>
            <div className={!isNavOpen ? "overflow-hidden" : ""}>
              <div
                className={cx([
                  "flex flex-col justify-end transition-all duration-300 ease-in-out w-[14.75rem]",
                  isNavOpen ? "opacity-100" : "opacity-0",
                ])}>
                content
              </div>
            </div>
          )}
        </nav>
        {switch (isNavOpen, isLg) {
        | (true, false) =>
          <div
            onClick={_ => hideMenu()}
            className="bg-neutral-700 opacity-75 fixed top-0 left-0 w-full h-full z-30 cursor-pointer"
          />
        | _ => React.null
        }}
      </React.Fragment>
    }
  }

  @react.component
  let make = (
    ~className="",
    ~children,
    ~username=?,
    ~sideNavRender,
    ~logout=?,
    ~onLogoClick,
    ~bottom=?,
    ~logoSrc=?,
    ~environment=?,
  ) => {
    let {isLg} = Toolkit__Hooks.useMediaQuery()
    let (isNavOpen, setOpen) = React.useState(() =>
      Browser.LocalStorage.getItem(localStorageNavKey)
      ->Js.Nullable.toOption
      ->Option.flatMap(value => value->bool_of_string_opt)
      ->Option.flatMap(isOpen => isLg ? Some(isOpen) : None)
      ->Option.getWithDefault(false)
    )

    let toggleMenu = React.useCallback(_ =>
      setOpen(value => {
        let newValue = !value

        Browser.LocalStorage.setItem(
          localStorageNavKey,
          newValue->string_of_bool->Js.Nullable.return,
        )
        newValue
      })
    , [])

    let hideMenu = React.useCallback(_ => setOpen(_ => false), [])
    let openMenu = React.useCallback(_ => setOpen(_ => true), [])

    <NavOpenContext.Provider value={isNavOpen}>
      <div>
        <TopNavigationBar ?environment ?logoSrc ?username ?logout toggleMenu onLogoClick />
        <div className="flex">
          <LeftNavigationBar isNavOpen ?bottom hideMenu openMenu>
            {sideNavRender}
          </LeftNavigationBar>
          <main
            className={cx([
              className,
              "flex-initial w-full transition-all duration-300 ease-in-out mt-16 z-20 print:mt-0 print:!pl-0",
              isNavOpen ? "navOpen lg:pl-64" : "lg:pl-16",
            ])}>
            children
          </main>
        </div>
      </div>
    </NavOpenContext.Provider>
  }
}
