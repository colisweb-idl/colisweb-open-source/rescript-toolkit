let ignoredTerms = ["timeout", "network"]

@react.component
let make = (
  ~fallbackRender: ReactErrorBoundary.fallbackProps => React.element,
  ~children: React.element,
  ~resetKeys: option<array<'a>>=?,
  ~customShouldRaiseAnError=false,
) => {
  <ReactErrorBoundary
    fallbackRender
    ?resetKeys
    onError={err => {
      switch Obj.magic(err) {
      | #decodeError(requestConfig, deccoError) =>
        Toolkit__BrowserLogger.error2(
          `Decode error${requestConfig["url"]->Option.mapWithDefault("", url => " " ++ url)}`,
          {
            "deccoError": deccoError,
            "requestConfig": requestConfig,
          },
        )
      | #default(axiosError) =>
        Js.Exn.message(axiosError)->Option.forEach(message => {
          let message = message->Js.String2.toLowerCase

          if ignoredTerms->Array.some(Js.String2.includes(message)) {
            ()
          } else {
            Toolkit__BrowserLogger.error2(axiosError, axiosError)
          }
        })
      | #custom(err) =>
        if customShouldRaiseAnError {
          Toolkit__BrowserLogger.error2("Custom error", err)
        }
      | err =>
        switch Js.Exn.message(Obj.magic(err)) {
        | None => Toolkit__BrowserLogger.error2("Unhandled error", err)
        | Some(message) =>
          if ignoredTerms->Array.some(Js.String2.includes(message->Js.String2.toLowerCase)) {
            ()
          } else {
            Toolkit__BrowserLogger.error2(Obj.magic(message), err)
          }
        }
      }
    }}>
    {children}
  </ReactErrorBoundary>
}
