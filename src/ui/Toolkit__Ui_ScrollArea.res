open Radix
@react.component
let make = (~children, ~className="") => {
  <ScrollArea.Root className={cx(["ScrollAreaRoot", className])}>
    <ScrollArea.Viewport className="ScrollAreaViewport"> children </ScrollArea.Viewport>
    <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation=Vertical>
      <ScrollArea.Thumb className="ScrollAreaThumb" />
    </ScrollArea.Scrollbar>
    <ScrollArea.Scrollbar className="ScrollAreaScrollbar" orientation=Horizontal>
      <ScrollArea.Thumb className="ScrollAreaThumb" />
    </ScrollArea.Scrollbar>
    <ScrollArea.Corner className="ScrollAreaCorner" />
  </ScrollArea.Root>
}
