type selectOption<'value> = {
  label: string,
  value: 'value,
  disabled?: bool,
}

type options<'value> = array<selectOption<'value>>

@react.component
let make = (
  ~options: options<'value>,
  ~onChange: option<'value> => unit,
  ~encodeValueToString: 'value => string,
  ~decodeValueFromString,
  ~onBlur=?,
  ~placeholder=?,
  ~defaultValue=?,
  ~isDisabled=?,
  ~isInvalid=?,
  ~className="",
  ~containerClassName="",
  ~id=?,
  ~value=?,
  ~name=?,
  ~autoFocus=?,
) =>
  <Toolkit__Ui_Select
    ?id
    ?name
    containerClassName
    className
    ?autoFocus
    ?onBlur
    ?isDisabled
    ?isInvalid
    ?placeholder
    value=?{value->Option.map(encodeValueToString)}
    defaultValue=?{defaultValue->Option.map(encodeValueToString)}
    options={options->Array.map((option): Toolkit__Ui_Select.selectOption => {
      label: option.label,
      value: option.value->encodeValueToString,
      disabled: ?option.disabled,
    })}
    onChange={value => value->decodeValueFromString->onChange}
  />
