open Radix

type color =
  | White
  | Black
  | Neutral
  | Primary

@react.component
let make = (
  ~trigger,
  ~triggerAsChild=false,
  ~content,
  ~color=White,
  ~rootOptions: option<Popover.Root.options>=?,
  ~contentOptions: option<Popover.Content.options>=?,
) => {
  let defaultContent: Popover.Content.props = {
    children: <>
      content
      <Popover.Close
        className="absolute w-6 h-6 right-2 top-2 inline-flex items-center justify-center rounded-full hover:bg-primary-500/25">
        <ReactIcons.MdClose />
      </Popover.Close>
      <Popover.Arrow
        className={switch color {
        | White => "fill-white"
        | Neutral => "fill-neutral-800"
        | Black => "fill-neutral-900"
        | Primary => "fill-primary-700"
        }}
      />
    </>,
  }
  <Popover.Root {...(rootOptions->Option.getWithDefault({})->Obj.magic)}>
    <Popover.Trigger asChild=triggerAsChild> trigger </Popover.Trigger>
    <Popover.Portal>
      <Popover.Content
        {...contentOptions->Option.mapWithDefault(defaultContent, options => {
          ...options->Obj.magic,
          children: defaultContent.children,
        })}
        className={cx([
          "rounded-lg w-72 p-4 shadow-lg z-50 cw-PopoverContent",
          switch color {
          | White => "bg-white text-neutral-800"
          | Neutral => "bg-neutral-800 text-white"
          | Black => "bg-neutral-900 text-white"
          | Primary => "bg-primary-700 text-white"
          },
          contentOptions->Option.flatMap(v => v.className)->Option.getWithDefault(""),
        ])}
        sideOffset={contentOptions->Option.flatMap(v => v.sideOffset)->Option.getWithDefault(5)}
      />
    </Popover.Portal>
  </Popover.Root>
}
