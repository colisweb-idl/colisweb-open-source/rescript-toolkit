@react.component
let make = (
  ~baseElementRef: React.ref<Js.Nullable.t<Dom.element>>,
  ~className="",
  ~visible: bool,
  ~children,
) => {
  let (visibility, setVisibility) = React.useState(() => false)
  let (customClassName, setCustomClassName) = React.useState(() => "")
  let (elementReady, setElementReady) = React.useState(() => false)
  let (style, setStyle) = React.useState(() => None)
  let noticeRef = React.useRef(Js.Nullable.null)

  React.useEffect(() => {
    let timeoutId = ref(None)

    baseElementRef.current
    ->Js.Nullable.toOption
    ->Option.forEach(dom => {
      let {x, y, width, height} = dom->Browser.DomElement.getBoundingClientRect

      if visible {
        setVisibility(_ => true)

        setStyle(
          _ => {
            setElementReady(_ => true)
            Some(
              ReactDOM.Style.make(
                ~top=`${Js.Float.toString(y +. height)}px`,
                ~left=`${(x +. width /. 2.)->Js.Float.toString}px`,
                (),
              ),
            )
          },
        )
      } else {
        timeoutId :=
          Some(
            Js.Global.setTimeout(
              () => {
                setElementReady(_ => false)
                setVisibility(_ => visible)
              },
              500,
            ),
          )
        setCustomClassName(_ => "opacity-0")
      }
    })

    Some(() => timeoutId.contents->Option.forEach(Js.Global.clearTimeout))
  }, (baseElementRef, visible))

  React.useEffect(() => {
    let timeoutId = ref(None)

    if elementReady {
      timeoutId := Some(Js.Global.setTimeout(() => {
            setCustomClassName(_ => "!opacity-100")
          }, 150))
    }
    Some(() => timeoutId.contents->Option.forEach(Js.Global.clearTimeout))
  }, [elementReady])

  React.useEffect(() => {
    if visibility {
      noticeRef.current
      ->Js.Nullable.toOption
      ->Option.forEach(dom => {
        let {left, right} = dom->Browser.DomElement.getBoundingClientRect

        setStyle(
          style =>
            style->Option.map(
              style => {
                ReactDOM.Style.combine(
                  style,
                  ReactDOM.Style.make(
                    ~marginLeft=left < 0. ? `${-.left->Js.Float.toString}px` : "",
                    ~marginRight=right < 0. ? `${-.right->Js.Float.toString}px` : "",
                    (),
                  ),
                )
              },
            ),
        )
      })
    }
    None
  }, (visibility, noticeRef))

  {
    visibility
      ? <div
          ref={ReactDOM.Ref.domRef(noticeRef)}
          className={cx([
            "absolute rounded mt-2 p-2 transform -translate-x-1/2 z-30 opacity-0 transition-all ease-in-out duration-300",
            className,
            customClassName,
          ])}
          ?style>
          children
        </div>
      : React.null
  }
}
