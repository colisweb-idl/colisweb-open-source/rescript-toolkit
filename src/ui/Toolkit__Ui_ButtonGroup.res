@react.component
let make = (~children, ~className="", ~size: [#md | #xl]=#md) => {
  let isMd = size === #md
  <div
    className={cx([
      className,
      "flex justify-center items-center rounded-full font-display bg-gray-200 cw-ButtonGroup",
      isMd ? "h-6" : "h-10",
      isMd ? "text-sm" : "text-xl",
    ])}>
    children
  </div>
}
