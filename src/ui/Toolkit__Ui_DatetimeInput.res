module DatePicker = {
  @module("react-datepicker") @react.component
  external make: (
    ~selected: Js.Date.t=?,
    ~selectsStart: bool=?,
    ~selectsEnd: bool=?,
    ~showTimeInput: bool=?,
    ~showPopperArrow: bool=?,
    ~showYearDropdown: bool=?,
    ~autoFocus: bool=?,
    ~autoComplete: string=?,
    ~showTimeSelect: bool=?,
    ~disabled: bool=?,
    ~inline: bool=?,
    ~required: bool=?,
    ~isClearable: bool=?,
    ~timeIntervals: int=?,
    ~name: string=?,
    ~id: string=?,
    ~className: string=?,
    ~calendarClassName: string=?,
    ~dateFormat: string=?,
    ~timeFormat: string=?,
    ~placeholderText: string=?,
    ~locale: string=?,
    ~timeCaption: string=?,
    ~startDate: Js.Date.t=?,
    ~endDate: Js.Date.t=?,
    ~minDate: Js.Date.t=?,
    ~minTime: Js.Date.t=?,
    ~maxTime: Js.Date.t=?,
    ~onChange: Js.Nullable.t<Js.Date.t> => unit=?,
    ~onBlur: ReactEvent.Focus.t => unit=?,
    ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
    ~ref: ReactDOM.Ref.t=?,
    unit,
  ) => React.element = "default"

  @module("react-datepicker")
  external registerLocale: (string, 'messages) => unit = "registerLocale"

  @module("date-fns/locale/fr") external fr: 'messages = "default"
  registerLocale("fr", fr)
}

@react.component
let make = (
  ~id,
  ~name=?,
  ~value=?,
  ~placeholder=?,
  ~autoFocus=?,
  ~disabled=?,
  ~required=?,
  ~showTimeSelect=?,
  ~showYearDropdown=?,
  ~startDate=?,
  ~inline=?,
  ~endDate=?,
  ~selectsStart=?,
  ~selectsEnd=?,
  ~minDate=?,
  ~minTime=?,
  ~maxTime=?,
  ~onBlur: option<ReactEvent.Focus.t => unit>=?,
  ~onKeyDown: option<ReactEvent.Keyboard.t => unit>=?,
  ~onChange: option<Js.Nullable.t<Js.Date.t> => unit>=?,
  ~isInvalid=?,
  ~className="",
  ~timeFormat="p",
  ~timeIntervals=15,
  ~timeCaption="Heure",
  ~inputRef=?,
) =>
  <DatePicker
    ref=?{inputRef->Option.map(ReactDOM.Ref.domRef)}
    selected=?value
    ?showTimeSelect
    showPopperArrow=false
    ?showYearDropdown
    isClearable=true
    timeCaption
    ?disabled
    ?required
    ?inline
    ?startDate
    ?endDate
    ?selectsStart
    ?selectsEnd
    ?minDate
    ?minTime
    ?maxTime
    id
    ?name
    placeholderText=?placeholder
    timeFormat
    dateFormat="Pp"
    timeIntervals
    locale="fr"
    ?autoFocus
    autoComplete="off"
    ?onBlur
    ?onChange
    ?onKeyDown
    className={cx([
      className,
      "appearance-none outline-none transition duration-150 ease-in-out block w-full bg-white text-gray-800 border rounded py-2 px-4 leading-tight focus:z30 relative disabled:bg-gray-200 disabled:text-gray-700",
      isInvalid->Option.getWithDefault(false) ? "border-danger-500 shadow-danger-500" : "",
    ])}
  />
