type size = [#xs | #sm | #md | #lg]
type variant =
  | Default
  | Filled

@react.component
let make = (
  ~value,
  ~children: option<React.element>=?,
  ~disabled: option<bool>=?,
  ~onChange=?,
  ~name=?,
  ~checked=?,
  ~className="",
  ~contentClassName="",
  ~size: size=#sm,
  ~variant: variant=Default,
  ~icon: option<module(ReactIcons.Icon)>=?,
) => {
  <label
    className={cx([
      "flex items-center cw-radio relative",
      switch variant {
      | Default => "cw-radio--default"
      | Filled => "self-start bg-neutral-100 font-medium px-2 py-1 rounded-full cw-radio--filled"
      },
      disabled->Option.getWithDefault(false)
        ? "cursor-not-allowed opacity-75 text-gray-600"
        : "cursor-pointer",
      className,
    ])}>
    <input
      type_="radio"
      value
      className="opacity-0 w-0 h-0 peer cw-radio-input"
      onChange={event => {
        let target = ReactEvent.Form.target(event)
        let value = target["value"]

        onChange->Option.forEach(fn => fn(value))
      }}
      ?disabled
      ?name
      ?checked
    />
    {switch variant {
    | Default => React.null
    | Filled =>
      <span
        className="cw-radio-filled-bg peer-checked:bg-primary-700 w-full h-full absolute rounded-full left-0 top-0"
      />
    }}
    <span
      className={cx([
        "bg-white  flex-shrink-0 cw-radio-circle",
        {
          switch variant {
          | Default => "border peer-checked:border-4 peer-checked:border-primary-700 peer-checked:text-primary-700"
          | Filled => "border-2 peer-checked:border-primary-700 peer-checked:text-white"
          }
        },
        "checkmark rounded-full   mr-2 border-neutral-700 transform transition-all ease-in-out flex items-center justify-center",
        switch size {
        | #xs => "w-4 h-4"
        | #sm => "w-6 h-6"
        | #md => "w-8 h-8"
        | #lg => "w-10 h-10"
        },
      ])}>
      {switch variant {
      | Default => React.null
      | Filled =>
        <span
          className={cx([
            "transform transition-all ease-in-out cw-radio-circle-content rounded-full",
            switch size {
            | #xs => "w-2 h-2"
            | #sm => "w-3 h-3"
            | #md => "w-4 h-4"
            | #lg => "w-6 h-6"
            },
          ])}
        />
      }}
    </span>
    <span
      className={cx([
        "flex flex-row items-center gap-2",
        {
          switch variant {
          | Default => ""
          | Filled => "peer-checked:text-white relative pr-1"
          }
        },
        contentClassName,
      ])}>
      {icon->Option.mapWithDefault(React.null, icon => {
        let module(Icon) = icon

        <Icon
          size={switch size {
          | #lg => 26
          | #md => 24
          | #sm => 20
          | #xs => 16
          }}
        />
      })}
      {children->Option.getWithDefault(React.null)}
    </span>
  </label>
}
