@react.component
let make = (~title: React.element, ~icon: module(ReactIcons.Icon), ~children) => {
  let module(Icon) = icon
  <section className={"bg-white shadow-md rounded-lg"}>
    <header className="bg-neutral-100 text-primary-700 px-6 py-4 flex flex-row items-center gap-2">
      <Icon size={32} />
      <p className={"font-medium text-xl font-display text-primary-800"}> {title} </p>
    </header>
    <div className="p-6 min-h-64"> {children} </div>
  </section>
}
