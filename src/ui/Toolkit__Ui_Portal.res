@val
external createElement: string => Dom.element = "document.createElement"

@val
external appendToBody: Dom.element => unit = "document.body.appendChild"

@val
external removeChild: Dom.element => unit = "document.body.removeChild"

@react.component
let make = (~children) => {
  let node = React.useRef(createElement("div")).current
  let (defaultNode, setDefaultNode) = React.useState(() => None)

  React.useEffect(() => {
    switch defaultNode {
    | None => {
        appendToBody(node)
        setDefaultNode(_ => Some(node))
      }

    | Some(_) => ()
    }

    Some(
      () => {
        defaultNode->Option.forEach(defaultNode => {
          removeChild(defaultNode)
        })
      },
    )
  }, [defaultNode])

  ReactDOM.createPortal(children, node)
}
