open ReactIntl

type item = {
  itemLabel?: React.element,
  label: string,
  value: string,
}

type options = array<item>

@react.component
let make = (
  ~options: options,
  ~placeholder: React.element,
  ~buttonClassName="",
  ~dropdownClassName="",
  ~itemClassName="",
  ~searchPlaceholder: option<string>=?,
  ~allowFilter=true,
  ~defaultValue: array<item>=[],
  ~onChange: array<item> => unit,
  ~disabled: option<bool>=?,
  ~onClose: option<unit => unit>=?,
  ~displaySelectOnlyOption=false,
) => {
  let (selectedOptions, setSelectedOptions) = React.useState(() => defaultValue)
  let previousDefaultValue = Toolkit__Hooks.usePrevious(defaultValue)
  let (search, setSearch) = React.useState(() => "")
  let allowFilter = options->Array.length > 5 && allowFilter

  React.useEffect(() => {
    let prev =
      previousDefaultValue
      ->Option.getWithDefault([])
      ->Array.map(({value}) => value)
      ->Js.Array2.joinWith("")
    let new = defaultValue->Array.map(({value}) => value)->Js.Array2.joinWith("")

    if prev != new {
      setSelectedOptions(_ => defaultValue)
    }

    None
  }, (previousDefaultValue, defaultValue))

  let filterOptionsBySearch = (~options, ~search) => {
    options->Array.keep(({label}) =>
      // normalize nfd -> replace by re remove split by commponents the accent letters and deletes the accent component only, leaving the un-accented letter
      search == "" || Toolkit__Primitives.String.normalizedIncludes(label, search)
    )
  }

  <Toolkit__Ui_Dropdown
    ?disabled
    buttonClassName
    ?onClose
    dropdownClassName
    position=#bottom
    label={switch selectedOptions {
    | [] =>
      <p className="flex flex-row gap-2 w-full items-center relative">
        <span className="ml-1"> {placeholder} </span>
        <span className="absolute inset-y-0 right-0 flex items-center">
          <ReactIcons.FaAngleDown />
        </span>
      </p>
    | options =>
      <>
        <div
          className="table table-fixed w-full mr-8"
          title={options->Array.map(({label}) => label)->Js.Array2.joinWith(", ")}>
          <span className="table-cell truncate text-left">
            {options->Array.map(({label}) => label)->Js.Array2.joinWith(", ")->React.string}
          </span>
        </div>
        <button
          className={"absolute right-1 bg-info-100 text-info-600 text-sm rounded px-1.5 py-0.5 inline-flex items-center gap-2"}
          onClick={event => {
            event->JsxEventC.Mouse.stopPropagation
            onChange([])
            setSelectedOptions(_ => [])
          }}>
          {options->Array.length->React.int}
          <span className={"rounded-full bg-info-600 text-info-100 p-0.5"}>
            <ReactIcons.MdClose size={14} />
          </span>
        </button>
      </>
    }}>
    <div className="py-2 pl-2 pr-1">
      {allowFilter
        ? <div className="mb-3">
            <Toolkit__Ui_TextInput
              id="search"
              autoFocus={true}
              placeholder=?{searchPlaceholder}
              allowWhiteSpace={true}
              onKeyDown={event => {
                if event->ReactEvent.Keyboard.key === "Enter" && search !== "" {
                  let selectedOptions = filterOptionsBySearch(~options, ~search)
                  onChange(selectedOptions)
                  setSelectedOptions(_ => {
                    selectedOptions
                  })
                }
              }}
              value={search}
              onChange={event => {
                let target = event->ReactEvent.Form.currentTarget
                // normalize nfd -> replace by re remove split by commponents the accent letters and deletes the accent component only, leaving the un-accented letter
                setSearch(_ => target["value"])
              }}
            />
          </div>
        : React.null}
      {options
      ->Array.keep(({label}) =>
        // normalize nfd -> replace by re remove split by commponents the accent letters and deletes the accent component only, leaving the un-accented letter
        search == "" || Toolkit__Primitives.String.normalizedIncludes(label, search)
      )
      ->Array.mapWithIndex((i, item) => {
        let {label, value} = item

        <div
          key={`multiselectoption-${label}-${value}-${i->Int.toString}`}
          className={cx([
            "group flex flex-row items-center gap-2 pt-3 text-left relative",
            i > 0 ? "mt-3" : "",
            itemClassName,
          ])}>
          <Toolkit__Ui_Checkbox
            value
            className="w-full flex-shrink-0 relative"
            checked={selectedOptions->Array.some(item =>
              item.label == label && item.value == value
            )}
            onChange={(checked, _value) => {
              if checked {
                setSelectedOptions(selectedOptions => {
                  let value = selectedOptions->Array.concat([item])

                  onChange(value)
                  value
                })
              } else {
                setSelectedOptions(selectedOptions => {
                  let value =
                    selectedOptions->Array.keep(
                      selectedItem => selectedItem.value != value && selectedItem.label != label,
                    )

                  onChange(value)
                  value
                })
              }
            }}>
            {item.itemLabel->Option.getWithDefault(label->React.string)}
          </Toolkit__Ui_Checkbox>
          {displaySelectOnlyOption
            ? <div className="group-hover:block hidden absolute right-0">
                <Toolkit__Ui_Button
                  onClick={_ => {
                    setSelectedOptions(_ => {
                      let value = [item]

                      onChange(value)
                      value
                    })
                  }}
                  color=#neutralLight
                  size=#sm
                  className="hover:underline hover:!bg-neutral-200 hover:!border-neutral-200 hover:!text-primary-700">
                  <FormattedMessage defaultMessage="Seulement" />
                </Toolkit__Ui_Button>
              </div>
            : React.null}
        </div>
      })
      ->React.array}
    </div>
  </Toolkit__Ui_Dropdown>
}
