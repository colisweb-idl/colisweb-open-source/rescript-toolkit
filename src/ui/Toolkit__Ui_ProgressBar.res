type color = [#success | #danger | #warning | #info | #primary]

@react.component
let make = (~progression: float, ~color: color=#success) =>
  <div className="flex flex-row w-full h-2 bg-neutral-300 rounded-lg">
    <div
      style={ReactDOMStyle.make(~width=`${progression->Float.toString}%`, ())}
      className={cx([
        "h-full rounded-lg transition-all duration-250 ease-in",
        switch color {
        | #success => "bg-success-500"
        | #danger => "bg-danger-500"
        | #warning => "bg-warning-500"
        | #info => "bg-info-500"
        | #primary => "bg-primary-600"
        },
      ])}
    />
  </div>
