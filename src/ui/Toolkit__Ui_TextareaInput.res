type variant = [#default | #code]

let autoExpand: Dom.element => unit = %raw(`
  function (field) {
    field.style.height = 'inherit';

    var computed = window.getComputedStyle(field);
    var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
                + parseInt(computed.getPropertyValue('padding-top'), 10)
                + field.scrollHeight
                + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                + parseInt(computed.getPropertyValue('border-bottom-width'), 10);

    field.style.height = height + 'px';
  }
  `)

@react.component
let make = React.forwardRef((
  ~id,
  ~name=?,
  ~value=?,
  ~variant: variant=#default,
  ~defaultValue=?,
  ~placeholder=?,
  ~autoFocus=?,
  ~disabled=?,
  ~required=?,
  ~onBlur: option<ReactEvent.Focus.t => unit>=?,
  ~onKeyDown: option<ReactEvent.Keyboard.t => unit>=?,
  ~onChange: option<ReactEvent.Form.t => unit>=?,
  ~isInvalid=?,
  ~className="",
  ref: Js.Nullable.t<React.ref<Js.Nullable.t<Dom.element>>>,
) => {
  let textareaRef = React.useRef(Js.Nullable.null)

  let onChange = event => {
    onChange->Option.map(onChange => onChange(event))->ignore
    textareaRef.current->Js.Nullable.toOption->Option.forEach(autoExpand)
  }

  React.useEffect(() => {
    textareaRef.current
    ->Js.Nullable.toOption
    ->Option.forEach(autoExpand)

    ref
    ->Js.Nullable.toOption
    ->Option.forEach(ref => ref.current = textareaRef.current)

    None
  }, [textareaRef.current])

  <textarea
    ref={ReactDOM.Ref.domRef(textareaRef)}
    style={ReactDOM.Style.make(
      ~height="auto",
      ~minHeight="38px",
      ~maxHeight="40vh",
      ~resize="none",
      (),
    )}
    className={cx([
      className,
      "appearance-none outline-none transition-all duration-150 ease-in-out block w-full bg-white text-gray-800 border rounded py-2 px-4 leading-normal focus:z30 relative disabled:bg-gray-200 disabled:text-gray-700",
      variant == #code ? "font-code" : "font-sans",
      isInvalid->Option.getWithDefault(false) ? "border-danger-500 shadow-danger-500" : "",
    ])}
    spellCheck={variant == #code ? false : true}
    autoComplete={variant == #code ? "off" : "on"}
    id
    rows=1
    onChange
    ?name
    ?value
    ?defaultValue
    ?disabled
    ?required
    ?placeholder
    ?autoFocus
    ?onBlur
    ?onKeyDown
  />
})
