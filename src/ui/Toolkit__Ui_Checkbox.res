type size = [#xs | #sm | #md | #lg]

@react.component
let make = (
  ~value,
  ~children: option<React.element>=?,
  ~disabled: option<bool>=?,
  ~onChange=?,
  ~name=?,
  ~checked=?,
  ~defaultChecked=?,
  ~intermediaryChecked=false,
  ~className="",
  ~size: size=#sm,
  ~checkboxClassName="",
  ~checkedClassname="",
  ~checkedCheckboxClassName="",
  ~inverseLabel=false,
  ~hideLabel=false,
) => {
  let (isChecked, setChecked) = React.useState(() => checked->Option.getWithDefault(false))
  let previousChecked = Toolkit__Hooks.usePrevious(isChecked)

  React.useEffect(() => {
    switch (previousChecked, checked) {
    | (Some(previous), Some(checked)) if previous != checked => setChecked(_ => checked)
    | _ => ()
    }

    None
  }, (previousChecked, checked))

  <label
    className={cx([
      "items-center cw-Checkbox",
      inverseLabel ? "inline-flex flex-row-reverse justify-end" : "flex flex-row",
      disabled->Option.getWithDefault(false)
        ? "cursor-not-allowed opacity-75 text-gray-600"
        : "cursor-pointer",
      className,
      isChecked ? checkedClassname : "",
    ])}>
    <input
      type_="checkbox"
      value
      ?defaultChecked
      className="hidden peer"
      onChange={event => {
        let target = ReactEvent.Form.target(event)
        let checked = target["checked"]
        let value = target["value"]

        setChecked(_ => checked)
        onChange->Option.forEach(fn => fn(checked, value))
      }}
      ?disabled
      ?name
      ?checked
    />
    <span
      className={cx([
        "peer-checked:bg-primary-500 peer-checked:border-primary-500",
        "bg-white rounded border text-white border-neutral-300 transform transition-all ease-in-out flex items-center justify-center flex-shrink-0",
        switch size {
        | #xs => "w-4 h-4"
        | #sm => "w-6 h-6"
        | #md => "w-8 h-8"
        | #lg => "w-10 h-10"
        },
        hideLabel ? "" : inverseLabel ? "ml-3" : "mr-3",
        checkboxClassName,
        isChecked ? checkedCheckboxClassName : "",
      ])}>
      {intermediaryChecked
        ? <ReactIcons.FaMinus
            className="transform transition-all ease-in-out"
            size={switch size {
            | #xs => 14
            | #sm => 16
            | #md => 18
            | #lg => 24
            }}
          />
        : <ReactIcons.FaCheck
            className="transform transition-all ease-in-out"
            size={switch size {
            | #xs => 14
            | #sm => 16
            | #md => 18
            | #lg => 24
            }}
          />}
    </span>
    {children->Option.mapWithDefault(React.null, children => hideLabel ? React.null : children)}
  </label>
}
