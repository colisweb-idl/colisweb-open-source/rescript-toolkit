open ReactIntl
open Browser

type state = {
  timeslot: Toolkit__Decoders.DatetimeTimeSlot.t,
  selectedDay: option<Js.Date.t>,
}

type action =
  | PreviousTimeSlot
  | NextTimeSlot
  | UpdateSelectedDay(option<Js.Date.t>)

@spice
type queryParams = {
  start: Toolkit__Decoders.Option.t<Toolkit__Decoders.Date.t>,
  selectedDay: Toolkit__Decoders.Option.t<Toolkit__Decoders.Date.t>,
}

let updateSearchParams = (state: state) => {
  let search = Location.location->Location.search->Js.String2.sliceToEnd(~from=1)
  let searchParams = URLSearchParams.make(search)

  switch state.selectedDay {
  | None => {
      searchParams->URLSearchParams.delete("start")
      searchParams->URLSearchParams.delete("selectedDay")
    }
  | Some(selectedDay) => {
      searchParams->URLSearchParams.set(
        "start",
        state.timeslot.start->DateFns.formatWithPattern("yyyy-MM-dd"),
      )

      searchParams->URLSearchParams.set(
        "selectedDay",
        selectedDay->DateFns.formatWithPattern("yyyy-MM-dd"),
      )
    }
  }

  RescriptReactRouter.replace("?" ++ searchParams->URLSearchParams.toString())
}

@react.component
let make = (~initialStart=?, ~selectedDay=?, ~children: state => React.element) => {
  let {isSm} = Toolkit__Hooks.useMediaQuery()
  let intl = useIntl()
  let timeSlotLength = isSm ? 7 : 3

  let queryParams = Toolkit__Hooks.useQueryParams(
    ~decoder=queryParams_decode,
    ~defaultParams={
      start: None,
      selectedDay: None,
    },
  )

  // Note: not sure about priority there...
  let initialStart = switch (initialStart, queryParams.start) {
  | (Some(_) as date, _) => date
  | (_, Some(_) as date) => date
  | _ => None
  }
  let selectedDay = switch (selectedDay, queryParams.selectedDay) {
  | (Some(_) as date, _) => date
  | (_, Some(_) as date) => date
  | _ => None
  }

  let ({timeslot, selectedDay}, dispatch) = ReactUpdate.useReducerWithMapState(
    (state, action) =>
      switch action {
      | PreviousTimeSlot =>
        UpdateWithSideEffects(
          {
            timeslot: {
              start: state.timeslot.start->DateFns.subDays(timeSlotLength)->DateFns.startOfDay,
              end_: state.timeslot.start->DateFns.subDays(1),
            },
            selectedDay: state.selectedDay->Option.map(_ =>
              state.timeslot.start->DateFns.subDays(timeSlotLength)->DateFns.startOfDay
            ),
          },
          ({state}) => {
            updateSearchParams(state)
            None
          },
        )
      | NextTimeSlot =>
        UpdateWithSideEffects(
          {
            timeslot: {
              start: state.timeslot.end_->DateFns.addDays(1),
              end_: state.timeslot.end_->DateFns.addDays(timeSlotLength),
            },
            selectedDay: state.selectedDay->Option.map(_ =>
              state.timeslot.end_->DateFns.addDays(1)
            ),
          },
          ({state}) => {
            updateSearchParams(state)
            None
          },
        )
      | UpdateSelectedDay(date) =>
        UpdateWithSideEffects(
          {...state, selectedDay: date},
          ({state}) => {
            updateSearchParams(state)
            None
          },
        )
      },
    () => {
      let defaultTimeslot: Toolkit__Decoders.DatetimeTimeSlot.t = {
        let today = Js.Date.make()
        let timeSlotStart = isSm ? DateFns.startOfWeek(today) : today

        let timeSlotEnd = isSm ? DateFns.endOfWeek(today) : today->DateFns.addDays(2)
        {
          start: timeSlotStart,
          end_: timeSlotEnd,
        }
      }

      {
        timeslot: initialStart->Option.mapWithDefault(defaultTimeslot, start => {
          start,
          end_: start->DateFns.addDays(timeSlotLength - 1),
        }),
        selectedDay: selectedDay->Option.isSome
          ? selectedDay
          : isSm
          ? None
          : Some(initialStart->Option.getWithDefault(defaultTimeslot.start)),
      }
    },
  )

  <React.Fragment>
    <div className="bg-white rounded flex justify-center items-center p-6">
      <Toolkit__Ui_IconButton
        size=#xs
        variant=#outline
        color=#neutral
        onClick={_ => dispatch(PreviousTimeSlot)}
        ariaLabel="previous week"
        icon={<ReactIcons.MdKeyboardArrowLeft size=30 />}
      />
      <div className="flex items-center mx-2">
        {DateFns.eachDayOfInterval({
          start: timeslot.start,
          end_: timeslot.end_,
        })
        ->Array.mapWithIndex((i, day) => {
          let isSelected =
            selectedDay->Option.mapWithDefault(false, sDay => DateFns.isSameDay(sDay, day))
          <React.Fragment key={`${i->Int.toString}-day`}>
            {i == 0 || day->DateFns.isFirstDayOfMonth
              ? <p className="text-xs text-neutral-600 uppercase">
                  <FormattedDate value=day month=#short />
                </p>
              : React.null}
            <div
              onClick={_ => dispatch(UpdateSelectedDay(isSm && isSelected ? None : Some(day)))}
              className="flex flex-col items-stretch w-16 mx-1 font-display cursor-pointer">
              <p
                className={cx([
                  "flex flex-col items-center justify-center uppercase text-xs w-full rounded-sm leading-tight py-1",
                  isSelected ? "text-white bg-primary-700" : "text-primary-700 bg-primary-50",
                ])}>
                <span>
                  {
                    let day =
                      intl->Intl.formatDateWithOptions(
                        day,
                        dateTimeFormatOptions(~weekday=#long, ()),
                      )
                    day
                    ->Js.String2.slice(~from=0, ~to_=4)
                    ->Js.String2.concat(day->Js.String2.length > 4 ? "." : "")
                    ->React.string
                  }
                </span>
                <span className="text-sm font-semibold">
                  <FormattedDate value=day day=#"2-digit" />
                </span>
              </p>
            </div>
          </React.Fragment>
        })
        ->React.array}
      </div>
      <Toolkit__Ui_IconButton
        size=#xs
        variant=#outline
        color=#neutral
        onClick={_ => dispatch(NextTimeSlot)}
        ariaLabel="next week"
        icon={<ReactIcons.MdKeyboardArrowRight size=30 />}
      />
    </div>
    {children({timeslot, selectedDay})}
  </React.Fragment>
}
