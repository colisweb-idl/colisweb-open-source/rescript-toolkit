let transformUrl = text =>
  text->Js.String2.unsafeReplaceBy0(%re("/(https?:\/\/[^\s]+)/g"), (url, _, _) =>
    `<a target='_blank' class='text-info-500 underline' href="${url}">${url}</a>`
  )

@module("sanitize-html")
external sanitizeHtml: (string, Js.t<'a>) => string = "default"

@react.component
let make = (~text) =>
  <span
    dangerouslySetInnerHTML={
      "__html": text
      ->sanitizeHtml({
        "allowedTags": ["a"],
        "allowedAttributes": {
          "a": ["href"],
        },
      })
      ->transformUrl,
    }
  />
