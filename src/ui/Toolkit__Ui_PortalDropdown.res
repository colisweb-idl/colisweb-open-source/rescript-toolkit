type position = [
  | #bottom
  | #right
  | #top
  | #left
]

@react.component
let make = (
  ~label: React.element,
  ~children,
  ~dropdownClassName="",
  ~buttonClassName="",
  ~containerClassName="",
  ~defaultIsOpen=false,
  ~onButtonClick=?,
  ~onClickOutside=?,
  ~buttonColor: Toolkit__Ui_Button.color=#white,
  ~buttonSize: Toolkit__Ui_Button.size=#md,
  ~buttonVariant: Toolkit__Ui_Button.variant=#default,
) => {
  let dropDownRef = React.useRef(Js.Nullable.null)
  let buttonRef = React.useRef(Js.Nullable.null)
  let disclosure = Toolkit__Hooks.useDisclosure(~defaultIsOpen, ())
  let {isOpen, hide, toggle} = disclosure

  Toolkit__Hooks.useOnClickOutside(dropDownRef, _ => {
    onClickOutside->Option.forEach(fn => fn())

    Js.Global.setTimeout(() => {
      if isOpen {
        hide()
      }
    }, 100)->ignore
  })

  let (adjustmentStyle, setAdjustmentStyle) = React.useState(() => None)

  React.useEffect(() => {
    if isOpen {
      ()
    } else {
      setAdjustmentStyle(_ => None)
    }
    None
  }, [isOpen])

  <div className={cx(["relative", containerClassName])}>
    <Toolkit__Ui_Button
      variant=buttonVariant
      size=buttonSize
      type_="button"
      color=buttonColor
      buttonRef={ReactDOM.Ref.domRef(buttonRef)}
      onClick={_ => {
        onButtonClick->Option.forEach(fn => fn())
        isOpen ? hide() : toggle()
      }}
      className={cx([buttonClassName, "dropdown-button"])}>
      label
    </Toolkit__Ui_Button>
    {isOpen
      ? <Toolkit__Ui_Portal>
          <div
            ref={ReactDOM.Ref.callbackDomRef(ref => {
              dropDownRef.current = ref

              if adjustmentStyle->Option.isNone {
                let ref = ref->Js.Nullable.toOption
                let buttonRef = buttonRef.current->Js.Nullable.toOption

                switch (ref, buttonRef) {
                | (Some(ref), Some(buttonRef)) => Js.Global.setTimeout(() => {
                    let {left, top, width, height} =
                      buttonRef->Browser.DomElement.getBoundingClientRect
                    let dropdown = ref->Browser.DomElement.getBoundingClientRect
                    let left = left +. width /. 2. -. dropdown.width /. 2.

                    let adjustmentStyle =
                      ReactDOM.Style.make(
                        ~top=`${(top +. height)->Js.Float.toString}px`,
                        ~opacity="1",
                        ...
                      )
                    let adjustmentStyle = switch left {
                    | left if left < 0. => adjustmentStyle(~left="8px", ())
                    | left if left +. dropdown.width > Browser.innerWidth->Int.toFloat =>
                      adjustmentStyle(~right="8px", ())
                    | left => adjustmentStyle(~left=`${left->Js.Float.toString}px`, ())
                    }

                    setAdjustmentStyle(_ => Some(adjustmentStyle))
                  }, 16)->ignore

                | _ => ()
                }
              }
            })}
            className={cx([
              "dropdown",
              "absolute z-40 bg-white p-2 transform shadow rounded text-base font-normal text-neutral-700 opacity-0",
              dropdownClassName,
            ])}
            style={adjustmentStyle->Option.getWithDefault(ReactDOM.Style.make())}>
            {children(disclosure)}
          </div>
        </Toolkit__Ui_Portal>
      : React.null}
  </div>
}
