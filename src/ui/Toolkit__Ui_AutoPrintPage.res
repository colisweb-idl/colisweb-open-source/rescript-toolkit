@val
external print: unit => unit = "window.print"

@react.component
let make = () => {
  React.useEffect(() => {
    print()
    None
  }, [])

  React.null
}
