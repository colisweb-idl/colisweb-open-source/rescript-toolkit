type position = [
  | #bottom
  | #top
]

type item = {
  icon: React.element,
  label: React.element,
  onClick: unit => unit,
  className: string,
}

@react.component
let make = (
  ~label: React.element,
  ~dropdownClassName="",
  ~buttonClassName="",
  ~containerClassName="",
  ~defaultIsOpen=false,
  ~buttonColor: Toolkit__Ui_Button.color=#white,
  ~buttonSize: Toolkit__Ui_Button.size=#md,
  ~buttonVariant: Toolkit__Ui_Button.variant=#default,
  ~position=#bottom,
  ~items: array<item>,
) => {
  let dropdownRef = React.useRef(Js.Nullable.null)
  let buttonRef = React.useRef(Js.Nullable.null)
  let (position, _setPosition) = React.useState(() => position)
  let {isOpen, hide, toggle} = Toolkit__Hooks.useDisclosure(~defaultIsOpen, ())
  let {isXs} = Toolkit__Hooks.useMediaQuery()

  Toolkit__Hooks.useOnClickOutside(dropdownRef, _ => {
    hide()
  })
  let (adjustmentStyle, setAdjustmentStyle) = React.useState(() => None)

  React.useEffect(() => {
    if isOpen && !isXs {
      Js.Global.setTimeout(() => {
        buttonRef.current
        ->Js.Nullable.toOption
        ->Option.forEach(
          dom => {
            let buttonRect = dom->Browser.DomElement.getBoundingClientRect
            let dropdownRect =
              dropdownRef.current
              ->Js.Nullable.toOption
              ->Option.map(Browser.DomElement.getBoundingClientRect)

            let calculatedTop = switch position {
            | #bottom => buttonRect.top +. buttonRect.height +. 10.
            | #top => {
                let base = buttonRect.top -. 10.
                switch dropdownRect {
                | None => base
                | Some({height}) => {
                    let newBase = base -. height /. 2.
                    newBase < 0. ? buttonRect.top +. buttonRect.height +. 10. : newBase
                  }
                }
              }
            }

            let calculatedLeft = {
              let baseContainerCenter = buttonRect.left +. buttonRect.width /. 2.

              switch dropdownRect {
              | None => baseContainerCenter
              | Some({width}) => {
                  let baseCalculated = baseContainerCenter -. width /. 2.
                  let hasOverflowLeft = baseCalculated < 0.
                  let hasOverflowRight =
                    baseContainerCenter +. width /. 2. > Browser.innerWidth->Int.toFloat

                  if hasOverflowLeft {
                    0.
                  } else if hasOverflowRight {
                    let tmp = baseContainerCenter +. width /. 2.

                    baseCalculated -. (tmp -. Browser.innerWidth->Int.toFloat)
                  } else {
                    baseCalculated
                  }
                }
              }
            }

            setAdjustmentStyle(
              _ => Some(
                ReactDOM.Style.make(
                  ~top=`${calculatedTop->Js.Float.toString}px`,
                  ~left=`${calculatedLeft->Js.Float.toString}px`,
                  ~opacity="1",
                  (),
                ),
              ),
            )
          },
        )
      }, 16)->ignore
    } else {
      setAdjustmentStyle(_ => None)
    }
    None
  }, [isOpen, isXs])

  <div className={cx(["relative", containerClassName])}>
    <Toolkit__Ui_Button
      variant=buttonVariant
      buttonRef={ReactDOM.Ref.domRef(buttonRef)}
      size=buttonSize
      type_="button"
      color=buttonColor
      onClick={event => {
        switch dropdownRef.current->Js.Nullable.toOption {
        | None => toggle()
        | Some(domRef) => {
            let isInDropdown = ReactEvent.Mouse.currentTarget(event)["contains"](. domRef)
            if !isInDropdown {
              toggle()
            }
          }
        }
      }}
      className={cx([buttonClassName, "dropdown-button"])}>
      label
      {isOpen
        ? <Toolkit__Ui_Portal>
            <div
              ref={ReactDOM.Ref.domRef(dropdownRef)}
              className={cx([
                "dropdown",
                "top-0 left-0 p-2 transform transition-opacity duration-150 ease-in-out shadow rounded text-base font-normal text-neutral-700",
                isXs ? "fixed !w-full !h-full z-40" : "absolute z-20 bg-white w-60 opacity-0",
                dropdownClassName,
              ])}
              style={adjustmentStyle->Option.getWithDefault(ReactDOM.Style.make())}>
              {isXs
                ? <div
                    className="bg-neutral-500 absolute left-0 top-0 w-full h-full opacity-50 z-30"
                  />
                : React.null}
              <div className={cx([isXs ? "z-40 absolute left-0 bottom-0  w-full" : ""])}>
                <div
                  className={cx([
                    "flex flex-col",
                    isXs ? "bg-white p-2 mx-2 shadow rounded-t-lg" : "",
                  ])}>
                  {items
                  ->Array.mapWithIndex((i, {icon, label, onClick, className}) => {
                    <div
                      onClick={_ => onClick()}
                      className={cx([
                        "flex flex-row gap-4 items-center py-2 hover:bg-neutral-100 focus:bg-neutral-100 cursor-pointer",
                        i == 0 ? "" : "border-t",
                        className,
                      ])}
                      key={i->Int.toString}>
                      {icon}
                      {label}
                    </div>
                  })
                  ->React.array}
                </div>
              </div>
            </div>
          </Toolkit__Ui_Portal>
        : React.null}
    </Toolkit__Ui_Button>
  </div>
}
