open Radix

type rec state = {list: array<options>}
and options = {
  id: id,
  isVisible: bool,
  title: React.element,
  content: option<React.element>,
  closable: bool,
  variant: variant,
  timeout: int,
}
and id
and variant = Success | Warning | Danger

type action =
  | Show(options)
  | Hide(id)
  | Remove(id)

let store = Restorative.createStore({list: []}, (state, action) =>
  switch action {
  | Show(options) => {list: state.list->Array.concat([options])}
  | Hide(id) => {
      list: state.list->Array.map(e =>
        if e.id === id {
          {...e, isVisible: false}
        } else {
          e
        }
      ),
    }
  | Remove(id) => {list: state.list->Array.keep(e => e.id !== id)}
  }
)

let show = (~title, ~content=?, ~closable=true, ~variant, ~timeout=5000, ()) => {
  let id: id = Js.Math.random()->Js.Float.toString->Obj.magic
  store.dispatch(
    Show({
      id,
      isVisible: true,
      title: title->React.string,
      content,
      closable,
      variant,
      timeout,
    }),
  )
}

let success = title => {
  let id: id = Js.Math.random()->Js.Float.toString->Obj.magic
  store.dispatch(
    Show({
      id,
      isVisible: true,
      title,
      content: None,
      closable: true,
      variant: Success,
      timeout: 5000,
    }),
  )
}
let warning = title => {
  let id: id = Js.Math.random()->Js.Float.toString->Obj.magic
  store.dispatch(
    Show({
      id,
      isVisible: true,
      title,
      content: None,
      closable: true,
      variant: Warning,
      timeout: 5000,
    }),
  )
}
let error = title => {
  let id: id = Js.Math.random()->Js.Float.toString->Obj.magic
  store.dispatch(
    Show({
      id,
      isVisible: true,
      title,
      content: None,
      closable: true,
      variant: Danger,
      timeout: 5000,
    }),
  )
}

let remove = id => store.dispatch(Remove(id))
let hide = id => {
  store.dispatch(Hide(id))
  Js.Global.setTimeout(() => {
    remove(id)
  }, 160)->ignore
}

module Item = {
  @react.component
  let make = (~options) => {
    let {id, isVisible, variant, title, content, closable, timeout} = options

    React.useEffect(() => {
      let timeoutId = Js.Global.setTimeout(() => {
        hide(id)
      }, timeout)
      Some(() => Js.Global.clearTimeout(timeoutId))
    }, [])

    <Toast.Root
      open_={isVisible}
      onOpenChange={v => v ? () : hide(id)}
      className={cx([
        "transition duration-500 ease-in-out mt-3 p-3 pl-3 pr-10 transform cw-snackbar ToastRoot",
        {
          switch variant {
          | Success => "bg-success-100 cw-snackbar--success"
          | Warning => "bg-warning-100 cw-snackbar--warning"
          | Danger => "bg-danger-100 cw-snackbar--danger"
          }
        },
      ])}>
      {closable
        ? <button className="absolute top-2 right-1 " onClick={_ => hide(id)}>
            <ReactIcons.MdClose
              size={24}
              className={cx([
                "rounded-full",
                {
                  switch variant {
                  | Success => "text-neutral-700 hover:bg-neutral-300"
                  | Warning => "text-warning-700 hover:bg-warning-300"
                  | Danger => "text-danger-700 hover:bg-danger-300"
                  }
                },
              ])}
            />
          </button>
        : React.null}
      <div className="flex flex-row gap-3 relative">
        <div>
          {switch variant {
          | Success => <ReactIcons.MdCheckCircle size=28 className="text-success-600" />
          | Warning => <ReactIcons.MdWarning size=28 className="text-warning-600" />
          | Danger => <ReactIcons.MdError size=28 className="text-danger-600" />
          }}
        </div>
        <Toast.Title className={"ToastTitle"}>
          <p
            className={cx([
              "text-lg cw-snackbar-title mb-1",
              switch variant {
              | Success => "text-neutral-700"
              | Warning => "text-warning-700"
              | Danger => "text-danger-700"
              },
            ])}>
            {title}
          </p>
        </Toast.Title>
      </div>
      {content->Option.mapWithDefault(React.null, content => {
        <Toast.Description className="ToastDescription"> content </Toast.Description>
      })}
    </Toast.Root>
  }
}

module Provider = {
  @react.component
  let make = () => {
    let {list} = store.useStore()

    <Radix.Toast.Provider swipeDirection={Right}>
      {list
      ->Array.map(e => {
        <Item key={e.id->Obj.magic} options=e />
      })
      ->React.array}
      <Toast.Viewport className="ToastViewport" />
    </Radix.Toast.Provider>
  }
}
