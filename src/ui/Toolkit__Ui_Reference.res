@react.component
let make = (
  ~reference,
  ~onCopyNotificationMessage: option<string>=?,
  ~className="",
  ~textClassName="",
  ~copyButtonSize=9,
  ~onReferenceClick=?,
) => {
  let refClipboard = Toolkit__Hooks.useClipboard(reference, ~onCopyNotificationMessage?)

  <div
    className={cx([
      "flex flex-row items-center bg-info-50 text-info-700 min-w-0 w-full",
      className,
    ])}>
    <p
      onClick={_ => onReferenceClick->Option.forEach(fn => fn())}
      className={cx(["font-mono w-full text-sm pl-2 truncate", textClassName])}>
      {reference->React.string}
    </p>
    <Toolkit__Ui_IconButton
      size=#xs
      color=#info
      onClick={_ => refClipboard.copy()}
      ariaLabel="copy"
      icon={<ReactIcons.MdContentCopy size={copyButtonSize} />}
      className="flex-shrink-0"
    />
  </div>
}
