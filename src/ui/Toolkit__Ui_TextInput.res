@react.component
let make = (
  ~id: string,
  ~accept: option<string>=?,
  ~name: option<string>=?,
  ~value: option<string>=?,
  ~defaultValue: option<string>=?,
  ~placeholder: option<string>=?,
  ~autoFocus: option<bool>=?,
  ~autoComplete: option<string>=?,
  ~ariaControls: option<string>=?,
  ~ariaActivedescendant: option<string>=?,
  ~ariaLabelledby: option<string>=?,
  ~disabled: option<bool>=?,
  ~required: option<bool>=?,
  ~type_: option<string>=?,
  ~step: option<float>=?,
  ~min: option<string>=?,
  ~max: option<string>=?,
  ~onBlur: option<ReactEvent.Focus.t => unit>=?,
  ~onKeyDown: option<ReactEvent.Keyboard.t => unit>=?,
  ~onChange: option<ReactEvent.Form.t => unit>=?,
  ~onChangeText: option<string => unit>=?,
  ~allowWhiteSpace=false,
  ~isInvalid: option<bool>=?,
  ~className: string="",
  ~style: option<ReactDOM.style>=?,
  ~inputRef=?,
) =>
  <input
    ref=?{inputRef->Option.map(ReactDOM.Ref.domRef)}
    className={cx([
      className,
      "appearance-none outline-none transition duration-150 ease-in-out block w-full bg-white text-gray-800 border rounded py-2 px-4 leading-tight focus:z30 relative disabled:bg-gray-200 disabled:text-gray-700",
      isInvalid->Option.getWithDefault(false) ? "border-danger-500 shadow-danger-500" : "",
    ])}
    id
    ?accept
    ?style
    ?name
    ?value
    ?defaultValue
    ?type_
    ?disabled
    ?required
    ?placeholder
    ?ariaControls
    ?ariaActivedescendant
    ?ariaLabelledby
    ?autoFocus
    ?autoComplete
    ?step
    ?min
    ?max
    onChange={event => {
      let value = (event->ReactEvent.Form.target)["value"]

      onChangeText->Option.forEach(fn => {
        if allowWhiteSpace {
          fn(value)
        } else {
          fn(value->Js.String2.trim)
        }
      })
      onChange->Option.forEach(fn => fn(event))
    }}
    ?onBlur
    ?onKeyDown
  />
