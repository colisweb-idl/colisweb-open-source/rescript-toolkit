type size = [
  | #md
  | #xl
]

type button<'value> = {
  onClick?: ReactEvent.Mouse.t => unit,
  label: React.element,
  value?: 'value,
  count?: int,
}

type buttons<'value> = array<button<'value>>

@react.component
let make = (
  ~buttons: buttons<'value>,
  ~onIndexChange: option<(int, option<'value>) => unit>=?,
  ~defaultActiveIndex=0,
  ~className="",
  ~size: size=#md,
  ~buttonClassName="",
) => {
  let isMd = size === #md
  let (activeIndex, setActiveIndex) = React.useState(() => defaultActiveIndex)
  let previousIndex = Toolkit__Hooks.usePrevious(activeIndex)

  React.useEffect(() => {
    onIndexChange->Option.forEach(fn => {
      switch previousIndex {
      | Some(previousIndex) if previousIndex !== activeIndex =>
        fn(activeIndex, (buttons[activeIndex]->Option.getUnsafe).value)
      | _ => ()
      }
    })
    None
  }, (activeIndex, previousIndex))

  <div
    className={cx([
      className,
      "flex justify-center items-center rounded-full font-display bg-gray-200 cw-ButtonGroup",
      isMd ? "h-6" : "h-10",
      isMd ? "text-sm" : "text-xl",
    ])}>
    {buttons
    ->Array.mapWithIndex((i, button) => {
      <button
        key={`btn-group-${i->Int.toString}`}
        onClick={event => {
          setActiveIndex(_ => i)
          button.onClick->Option.forEach(fn => {
            fn(event)
          })
        }}
        className={cx([activeIndex === i ? "selected" : "", buttonClassName])}>
        {button.label}
        {button.count->Option.mapWithDefault(React.null, count => {
          <span
            className={cx([
              "ml-2 bg-gray-300/40 rounded px-1.5 ",
              switch size {
              | #md => "text-xs"
              | #xl => "text-sm"
              },
              activeIndex === i ? "text-white fn" : "text-black",
            ])}>
            {count->React.int}
          </span>
        })}
      </button>
    })
    ->React.array}
  </div>
}
