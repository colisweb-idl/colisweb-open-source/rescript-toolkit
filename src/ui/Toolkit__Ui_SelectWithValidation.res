open ReactIntl

type item = {
  itemLabel?: React.element,
  label: string,
  value: string,
}

type options = array<item>

module Footer = {
  @react.component
  let make = (
    ~onCancel,
    ~onValidateClick,
    ~validateMessage=?,
    ~cancelButtonClassName="",
    ~validateButtonClassName="",
  ) => {
    let dropdownContext = React.useContext(Toolkit__Ui_Dropdown.dropdownContext)

    <footer className="bg-white p-1 flex flex-row justify-between">
      <Toolkit__Ui_Button
        type_="button"
        color=#neutralLight
        className={cancelButtonClassName}
        variant=#outline
        onClick={_ => {
          dropdownContext.hide()
          onCancel()
        }}>
        <FormattedMessage defaultMessage={"Annuler"} />
      </Toolkit__Ui_Button>
      <Toolkit__Ui_Button
        color=#primary
        type_="button"
        className={validateButtonClassName}
        onClick={_ => {
          onValidateClick()
          dropdownContext.hide()
        }}>
        {validateMessage->Option.getWithDefault(<FormattedMessage defaultMessage={"Valider"} />)}
      </Toolkit__Ui_Button>
    </footer>
  }
}

module Options = {
  @react.component
  let make = (~options, ~deferredSearch, ~itemClassName, ~setSelectedOption, ~selectedOption) => {
    options
    ->Array.keep(({label}) =>
      deferredSearch == "" || Toolkit__Primitives.String.normalizedIncludes(label, deferredSearch)
    )
    ->Array.mapWithIndex((i, item) => {
      let {label, value} = item

      <div
        key={`multiselectoption-${label}-${value}-${i->Int.toString}`}
        className={cx([
          "group flex flex-row items-center gap-2 pt-3 text-left relative",
          i > 0 ? "mt-3" : "",
          itemClassName,
        ])}>
        <Toolkit__Ui_Radio
          value
          className="w-full flex-shrink-0 relative"
          checked={selectedOption->Option.mapWithDefault(false, selectedOption => {
            item.label == selectedOption.label && item.value == selectedOption.value
          })}
          onChange={_ => {
            setSelectedOption(_ => {
              Some(item)
            })
          }}>
          {item.itemLabel->Option.getWithDefault(label->React.string)}
        </Toolkit__Ui_Radio>
      </div>
    })
    ->React.array
  }
}

@react.component
let make = (
  ~options: options,
  ~placeholder: React.element,
  ~buttonClassName="",
  ~dropdownClassName="",
  ~validateButtonClassName="",
  ~cancelButtonClassName="",
  ~itemClassName="",
  ~optionsClassName="",
  ~buttonColor=?,
  ~buttonSize=?,
  ~buttonVariant=?,
  ~buttonLeftIcon=?,
  ~searchPlaceholder: option<string>=?,
  ~allowFilter=true,
  ~defaultValue: option<item>=?,
  ~onValidate: item => unit,
  ~disabled: option<bool>=?,
  ~onCancel: option<unit => unit>=?,
  ~validateMessage: option<React.element>=?,
  ~showCarret=true,
) => {
  let (selectedOption, setSelectedOption) = React.useState(() => defaultValue)
  let previousDefaultValue = Toolkit__Hooks.usePrevious(defaultValue)
  let (search, setSearch) = React.useState(() => "")
  let deferredSearch = React.useDeferredValue(search)
  let allowFilter = options->Array.length > 5 && allowFilter

  React.useEffect(() => {
    switch (previousDefaultValue, defaultValue) {
    | (Some(Some(v)), Some(v2)) if v !== v2 => setSelectedOption(_ => defaultValue)
    | _ => ()
    }

    None
  }, (previousDefaultValue, defaultValue))

  <Toolkit__Ui_Dropdown
    ?disabled
    ?buttonColor
    ?buttonSize
    ?buttonVariant
    ?buttonLeftIcon
    buttonClassName
    onClose={_ => setSelectedOption(_ => defaultValue)}
    dropdownClassName
    position=#bottom
    label={switch defaultValue {
    | None =>
      <p className="flex flex-row gap-2 w-full items-center relative">
        <span> {placeholder} </span>
        {showCarret
          ? <span className="absolute inset-y-0 right-0 flex items-center">
              <ReactIcons.FaAngleDown />
            </span>
          : React.null}
      </p>
    | Some(selectedOption) =>
      <div className="table table-fixed w-full" title={selectedOption.label}>
        <span className="table-cell truncate text-left w-full">
          {selectedOption.itemLabel->Option.getWithDefault(selectedOption.label->React.string)}
        </span>
        {showCarret
          ? <span className="absolute inset-y-0 right-2 flex items-center">
              <ReactIcons.FaAngleDown />
            </span>
          : React.null}
      </div>
    }}>
    <div className={cx(["py-2 pl-2 pr-1 max-h-[300px] overflow-y-scroll", optionsClassName])}>
      {allowFilter
        ? <div className="mb-3">
            <Toolkit__Ui_TextInput
              id="search"
              autoFocus={true}
              placeholder=?{searchPlaceholder}
              allowWhiteSpace={true}
              value={search}
              onChange={event => {
                let target = event->ReactEvent.Form.currentTarget

                setSearch(_ => target["value"]->Toolkit__Primitives.String.normalizeForSearch)
              }}
            />
          </div>
        : React.null}
      <Options deferredSearch options setSelectedOption selectedOption itemClassName />
    </div>
    <Footer
      onCancel={() => {
        setSelectedOption(_ => defaultValue)
        onCancel->Option.forEach(fn => fn())
      }}
      ?validateMessage
      onValidateClick={() => selectedOption->Option.forEach(onValidate)}
      validateButtonClassName
      cancelButtonClassName
    />
  </Toolkit__Ui_Dropdown>
}
