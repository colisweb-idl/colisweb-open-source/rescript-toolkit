module Radio = Toolkit__Ui.Radio

type element = {
  label: React.element,
  value: string,
  disabled?: bool,
}

@react.component
let make = (
  ~onChange,
  ~elements: array<element>,
  ~variant=?,
  ~defaultValue=?,
  ~containerClassName="",
  ~inline=false,
) => {
  let name = "radio-" ++ ReachUi_AutoId.use("")
  let (value, setValue) = React.useState(() => defaultValue->Option.getWithDefault(""))

  React.useEffect(() => {
    onChange(value)
    None
  }, [value])

  <div className={cx([inline ? "flex gap-3" : "", containerClassName])}>
    {elements
    ->Array.mapWithIndex((i, element) =>
      <Radio
        ?variant
        key={"radio-" ++ string_of_int(i)}
        value=element.value
        name
        disabled=?element.disabled
        checked={element.value === value}
        onChange={value => {
          setValue(_ => value)
        }}>
        element.label
      </Radio>
    )
    ->React.array}
  </div>
}
