let positiveIntRegex = %re("/^[0-9]+$/")
let strictPositiveIntReex = %re("/^[0-9]*[1-9][0-9]*$/")
let positiveIntOrFloatRegex = %re("/^[0-9]+(?:\.?[0-9]+)?$/")

@new
external makeRegex: string => Js.Re.t = "RegExp"

module Test = {
  let isPositiveInt = Js.Re.test_(positiveIntRegex, _)
  let isStrictPositiveInt = Js.Re.test_(strictPositiveIntReex, _)
  let isPositiveIntOrFloat = Js.Re.test_(positiveIntOrFloatRegex, _)
}

let isValid = regex => {
  try {
    let _test = makeRegex(regex)
    true
  } catch {
  | _ => false
  }
}
