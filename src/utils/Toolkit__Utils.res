module UnitMeasure = Toolkit__Utils_UnitMeasure
module Regex = Toolkit__Utils_Regex

let wait = ms =>
  Js.Promise.make((~resolve, ~reject as _) => {
    Js.Global.setTimeout(() => {
      let data = ()
      resolve(. data)
    }, ms)->ignore
  })
  ->Promise.Js.fromBsPromise
  ->Promise.Js.toResult

let durationMinutesToHourMinutesString = (durationMinutes: float, ~separator=":", ()) => {
  let hours = (durationMinutes /. 60.)->Js.Math.trunc
  let minutes = (durationMinutes -. hours *. 60.)->Js.Math.round

  let hours = minutes === 60. ? hours +. 1. : hours
  let minutes = minutes === 60. ? 0. : minutes

  switch (hours, minutes) {
  | (0., min) => min->Js.Float.toFixed ++ " min"
  | (h, 0.) => h->Js.Float.toFixed ++ " h"
  | (h, min) =>
    `${h < 10. ? "0" : ""}${h->Js.Float.toFixed}${separator}${min < 10.
        ? "0"
        : ""}${min->Js.Float.toFixed}`
  }
}

module type Enum = {
  type t
  let t_decode: Js.Json.t => result<t, Spice.decodeError>
  let t_encode: t => Js.Json.t
}

let encodeEnumToString = (type enum, enumValue: enum, enum: module(Enum with type t = enum)) => {
  let module(Enum) = enum

  (enumValue->Enum.t_encode->Obj.magic: string)
}

let decodeEnumFromString = (type enum, str: string, enum: module(Enum with type t = enum)) => {
  let module(Enum) = enum
  switch str->Obj.magic->Enum.t_decode {
  | Ok(v) => Some(v)
  | Error(_) => None
  }
}
