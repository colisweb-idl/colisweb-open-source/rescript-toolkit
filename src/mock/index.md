# Mock

We use [msw](https://mswjs.io) for mocking API content.

## Usage

Install the package

```
yarn add msw --dev
```

### Wrap the App for dev only

```js
import React from "react";
import ReactDOM from "react-dom";
import "@colisweb/rescript-toolkit/src/ui/styles.css";
import { make as App } from "../lib/es6_global/src/App.bs";
import { make as Spinner } from "@colisweb/rescript-toolkit/lib/es6_global/src/ui/Toolkit__Ui_SpinnerFullScreen.bs.js";
import { make as AppWithMock } from "@colisweb/rescript-toolkit/lib/es6_global/src/mock/AppWithMock.bs";

const root = ReactDOM.createRoot(document.getElementById("root"));

if (import.meta.env.DEV) {
  import("../lib/es6_global/mocks/Mock.bs").then(({ worker }) => {
    root.render(
      <AppWithMock worker={worker}>
        <App />
      </AppWithMock>
    );
  });
} else {
  root.render(
    <React.Suspense fallback={<Spinner />}>
      <App />
    </React.Suspense>
  );
}
```

### Define the mock

```rescript
/* Mock_Page_X.res */

open Identifiers

let mocks = [
  Msw.Rest.get(
    "https://api.url",
    ({params}) => {
      open ColiswebApi.V5.Transporter.GetContactsWithParameters.Config
      let _emptyResponse = []
      let defaultResponse = [
        {
          id: ContactIdString.make("1"),
          firstName: "Thomas",
          lastName: "Deconinck",
          email: "dck@colisweb.com",
          primaryPhone: "0123456789",
          parameters: {
            id: ParameterId.make("1"),
            email: true,
            sms: true,
            call: true,
          },
          carrier: None,
        }
      ]

      let transporterId = params->Js.Dict.get("transporterId")

      switch transporterId {
      | Some("9") => Msw.HttpResponse.json(defaultResponse->response_encode)
      | _ => Msw.HttpResponse.json(_emptyResponse->response_encode)
      }
    },
  ),
  Msw.Rest.put(
    "https://api.url",
    (_) => {
      Msw.HttpResponse.jsonWithOptions(Js.Json.null, {status: 200})
    },
  ),
]
```

Define the worker with the mock at the top of your app.

```rescript
/* Mock.res */
let worker = Msw.setupWorker([]->Array.concat(Mock_Page_X.mocks))
```
