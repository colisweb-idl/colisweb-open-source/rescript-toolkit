let localStorageKey = "@colisweb/mock"

@react.component
let make = (~worker: Msw.worker, ~workerOptions: Msw.startOptions={}, ~className="", ~children) => {
  let (mockEnabled, setMock) = Toolkit__Hooks.useLocalStorageState(
    localStorageKey,
    {
      defaultValue: false,
    },
  )
  let (workerStarted, setWorkerStarted) = React.useState(() => false)

  React.useLayoutEffect(() => {
    if mockEnabled {
      worker
      ->Msw.start(workerOptions)
      ->Promise.Js.toResult
      ->Promise.tapOk(_ => {
        setWorkerStarted(_ => true)
        Browser.LocalStorage.setItem(localStorageKey, Js.Nullable.return("true"))
      })
      ->Promise.tapError(err => {
        Toolkit.BrowserLogger.debug(err)
      })
      ->ignore
    } else {
      worker->Msw.stop
      setWorkerStarted(_ => false)
      Browser.LocalStorage.removeItem(localStorageKey)
    }

    None
  }, [mockEnabled])

  <React.Suspense fallback={<Toolkit__Ui_SpinnerFullScreen />}>
    <label
      className={cx([
        "fixed top-2 left-1/2 flex flex-row gap-2 items-center justify-center z-50 print:hidden",
        mockEnabled
          ? "bg-success-100 border-success-400 text-success-700"
          : "bg-neutral-100 p-1 border rounded-lg cursor-pointer",
        className,
      ])}>
      <span> {"Mock"->React.string} </span>
      <input
        type_={"checkbox"}
        checked={mockEnabled}
        onChange={event => {
          let checked = (event->ReactEvent.Form.target)["checked"]

          setMock(_ => checked)
        }}
      />
    </label>
    {switch (mockEnabled, workerStarted) {
    | (false, _) => children
    | (true, false) => React.null
    | (true, true) => children
    }}
  </React.Suspense>
}

let default = make
