# Primitives

Utililties functions for primitives types :

- `option`
- `string`
- `array`

## API

### String

#### includes

Check if the string is included by setting both strings to lowercase and using `normalize("NFD")`.

```rescript
let search = "aurelie"
let data = "Aurélie

data->Primitives.String.includes(search) // true
```

### Option

```rescript
let anEmptyString = "";

Primitives.Option.fromString(anEmptyString)
->Option.map(v => v ++ "test");
```

### Array

```rescript
let variable = [|"abc", "dce", ""|];

Js.log(Primitives.Array.joinNonEmpty(anEmptyString)); // "abc, dce"
Js.log(Primitives.Array.joinNonEmpty(~separator="+", anEmptyString)); // "abc+dce"
Js.log(variable->Primitives.Array.tail); // ""
```

### Result

```rescript
let result = Ok("test");
let test = Error("error");

Js.log(Primitives.Result.get(result)); // Some("test")
Js.log(Primitives.Result.get(test)); // None
```
