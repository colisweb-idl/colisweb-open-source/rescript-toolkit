module Result = {
  include Belt.Result

  let get = result =>
    switch result {
    | Ok(v) => Some(v)
    | Error(_) => None
    }

  let mapError = (result: t<'a, 'b>, fn: 'b => 'c) => {
    switch result {
    | Ok(_) as ok => ok
    | Error(err) => Error(fn(err))
    }
  }

  let mapOk = map
}
