type t
@val external windowInstance: option<t> = "window"
@val external window: t = "window"
@val external process: t = "import.meta.env"
let get = () => Js.typeof(windowInstance) !== "undefined" ? window : process
let instance = get()

@new
external makeError: string => Js.Exn.t = "Error"
@get external _nodeEnv: t => string = "NODE_ENV"

let nodeEnv = instance->_nodeEnv

let debugStyle = "background-color: #208E9C; color: white; padding: 5px; font-size: 10px;"
let warningStyle = "background-color: #FFAD2D; color: white; padding: 5px; font-size: 10px;"
let errorStyle = "background-color: #FF4714; color: white; padding: 5px; font-size: 10px;"

let debug = str => {
  if nodeEnv !== "production" {
    Js.Console.log3("%cDEBUG", debugStyle, str)
  }
}

let debug2 = (str, str2) => {
  if nodeEnv !== "production" {
    Js.Console.log4("%cDEBUG", debugStyle, str, str2)
  }
}
let debug3 = (str, str2, str3) => {
  if nodeEnv !== "production" {
    Js.Console.logMany(["%cDEBUG", debugStyle, Obj.magic(str), Obj.magic(str2), Obj.magic(str3)])
  }
}

let warning = str => {
  if nodeEnv !== "production" {
    Js.Console.log3("%cWARN", warningStyle, str)
  }
}

let warning2 = (str, str2) => {
  if nodeEnv !== "production" {
    Js.Console.log4("%cWARN", warningStyle, str, str2)
  }
}

let warning3 = (str, str2, str3) => {
  if nodeEnv !== "production" {
    Js.Console.logMany(["%cWARN", warningStyle, str, str2, str3])
  }
}

let error = (str: string) => {
  DatadogRum.Browser.addError(makeError(str))
  // if nodeEnv !== "production" {
  //   Js.Console.log3("%cERROR", errorStyle, str)
  // } else {
  // }
}

let error2 = (str, str2) => {
  DatadogRum.Browser.addErrorWithContext(
    makeError(str),
    {
      "cause": str2,
    },
  )
  // if nodeEnv !== "production" {
  //   Js.Console.log4("%cERROR", errorStyle, str, str2)
  // } else {
  // }
}
