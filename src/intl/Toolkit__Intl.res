open ReactIntl

type rec messages = {fr: array<translation>}
and translation = {
  id: string,
  defaultMessage: string,
  message: Js.nullable<string>,
}

let toDict = (translations: array<translation>) =>
  translations->Array.reduce(Js.Dict.empty(), (dict, entry) => {
    dict->Js.Dict.set(
      entry.id,
      entry.message->Js.Nullable.toOption->Option.getWithDefault(entry.defaultMessage),
    )
    dict
  })

type availableLanguages = [
  | #fr
]

let availableLanguagesToString = (availableLanguages: availableLanguages) =>
  switch availableLanguages {
  | #fr => "fr"
  }

let availableLanguagesFromString = v =>
  switch v {
  | v if v->Js.String2.includes("fr") => #fr
  | _ => #fr
  }

let getDateFnsLocale = (locale: availableLanguages) =>
  switch locale {
  | #fr => DateFns.frLocale
  }

let createIntl = (~onError=?, locale: availableLanguages, messages) => {
  let cache = createIntlCache()

  createIntl(
    intlConfig(~locale=locale->availableLanguagesToString, ~messages, ~onError?, ()),
    cache,
  )
}

module type IntlConfig = {
  let messages: messages
  let defaultLocale: option<availableLanguages>
  let onError: string => unit
}

module Make = (Config: IntlConfig) => {
  let browserLocale = Browser.Navigator.getBrowserLanguage()
  let locale =
    Config.defaultLocale->Option.getWithDefault(browserLocale->availableLanguagesFromString)

  DateFns.setDefaultOptions({locale: locale->getDateFnsLocale})

  let messages = switch locale {
  | #fr => Config.messages.fr->toDict
  }
  let intl = createIntl(locale, messages)

  type state = {
    locale: availableLanguages,
    intl: Intl.t,
  }

  type action = SetLocale(availableLanguages)

  let store = Restorative.createStore({locale, intl}, (_state, action) =>
    switch action {
    | SetLocale(locale) => {
        locale,
        intl: {
          let messages = switch locale {
          | #fr => Config.messages.fr->toDict
          }
          createIntl(locale, messages)
        },
      }
    }
  )

  let setCurrentLocale = (locale: availableLanguages) => store.dispatch(SetLocale(locale))

  let useCurrentLocale = () => store.useStore().locale
  let getCurrentLocale = () => store.getState().locale
  let getIntl = () => store.getState().intl
  let useIntl = () => store.useStore().intl

  module Provider = {
    @react.component
    let make = (~children) => {
      let intl = useIntl()
      let locale = useCurrentLocale()

      React.useLayoutEffect(() => {
        DateFns.setDefaultOptions({locale: locale->getDateFnsLocale})
        None
      }, [locale])

      <RawIntlProvider value=intl> children </RawIntlProvider>
    }
  }
}
