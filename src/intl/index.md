# Intl

## extract.js

Run the `bs-react-intl-extractor` command.

The structure must be like this :

```
<root>
  | src
  | locale
    | translations
      | en.json
      | fr.json
```

## check

Script that check if `*.json` file has empty values
