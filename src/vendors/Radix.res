module Tooltip = {
  type state =
    | @as("closed") Closed
    | @as("delayed-open") DelayedOpen
    | @as("instant-open") InstantOpen
  type side =
    | @as("top") Top
    | @as("right") Right
    | @as("bottom") Bottom
    | @as("left") Left
  type align =
    | @as("start") Start
    | @as("center") Center
    | @as("end") End

  module Provider = {
    type props = {
      delayDuration?: int,
      skipDelayDuration?: int,
      disableHoverableContent?: bool,
      children: React.element,
    }
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Provider"
  }
  module Root = {
    type props = {
      delayDuration?: int,
      defaultOption?: bool,
      defaultOpen?: bool,
      @as("open") open_?: bool,
      disableHoverableContent?: bool,
      onOpenChange?: bool => unit,
      children: React.element,
    }
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Root"
  }
  module Trigger = {
    type props = {
      asChild?: bool,
      @as("data-state") dataState?: state,
      children: React.element,
      @as("type")
      type_: string,
    }
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Trigger"
  }
  module Portal = {
    type props = {forceMount?: bool, children: React.element}
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Portal"
  }
  module Arrow = {
    type props = {className?: string, asChild?: bool, width?: int, height?: int}
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Arrow"
  }
  module Content = {
    type props = {
      className?: string,
      asChild?: bool,
      forceMount?: bool,
      arialLabel?: string,
      onEscapeKeyDown?: ReactEvent.Keyboard.t => unit,
      onPointerDownOutside?: ReactEvent.Keyboard.t => unit,
      side?: side,
      sideOffset?: int,
      align?: align,
      alignOffset?: int,
      avoidCollisions?: bool,
      sticky?: bool,
      hideWhenDetached?: bool,
      children?: React.element,
    }
    @module("@radix-ui/react-tooltip")
    external make: React.component<props> = "Content"
  }
}

module Popover = {
  module Root = {
    type options = {
      defaultOpen?: bool,
      open_?: bool,
      onOpenChange?: bool => unit,
      modal?: bool,
    }
    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-popover")
    external make: React.component<props> = "Root"
  }
  module Trigger = {
    type props = {children: React.element, asChild?: bool}
    @module("@radix-ui/react-popover")
    external make: React.component<props> = "Trigger"
  }
  module Anchor = {
    @module("@radix-ui/react-popover") @react.component
    external make: (~children: React.element) => React.element = "Anchor"
  }
  module Portal = {
    @module("@radix-ui/react-popover") @react.component
    external make: (~children: React.element) => React.element = "Portal"
  }
  module Content = {
    type align =
      | @as("center") Center
      | @as("start") Start
      | @as("end") End

    type side =
      | @as("top") Top
      | @as("right") Right
      | @as("bottom") Bottom
      | @as("left") Left

    type options = {
      className?: string,
      align?: align,
      side?: side,
      arrowPadding?: int,
      sideOffset?: int,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-popover")
    external make: React.component<props> = "Content"
  }
  module Close = {
    type props = {className?: string, children?: React.element}
    @module("@radix-ui/react-popover")
    external make: React.component<props> = "Close"
  }
  module Arrow = {
    type props = {className?: string}
    @module("@radix-ui/react-popover")
    external make: React.component<props> = "Arrow"
  }
}

module DropdownMenu = {
  module Root = {
    type options = {
      defaultOpen?: bool,
      open_?: bool,
      onOpenChange?: bool => unit,
      modal?: bool,
    }
    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Root"
  }
  module Trigger = {
    type options = {asChild?: bool}
    type props = {...options, children: React.element}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Trigger"
  }
  module ItemIndicator = {
    type props = {children: React.element, className?: string, asChild?: bool}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "ItemIndicator"
  }
  module Group = {
    type props = {children: React.element, asChild?: bool}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Group"
  }
  module RadioGroup = {
    type props = {
      children: React.element,
      asChild?: bool,
      value?: string,
      onValueChange?: string => unit,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "RadioGroup"
  }
  module RadioItem = {
    type options = {
      className?: string,
      value?: string,
      checked?: bool,
      disabled?: bool,
      textValue?: bool,
      asChild?: bool,
      onSelect?: ReactEvent.Mouse.t => unit,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "RadioItem"
  }
  module Label = {
    type props = {children: React.element, className?: string, asChild?: bool}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Label"
  }
  module Portal = {
    @module("@radix-ui/react-dropdown-menu") @react.component
    external make: (~children: React.element) => React.element = "Portal"
  }
  module Content = {
    type align =
      | @as("center") Center
      | @as("start") Start
      | @as("end") End

    type side =
      | @as("top") Top
      | @as("right") Right
      | @as("bottom") Bottom
      | @as("left") Left

    type options = {
      className?: string,
      align?: align,
      side?: side,
      arrowPadding?: int,
      sideOffset?: int,
      asChild?: bool,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Content"
  }
  module Sub = {
    type options = {
      className?: string,
      defaultOpen?: bool,
      @as("open") open_?: bool,
      onOpenChange?: bool => unit,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Sub"
  }
  module CheckboxItem = {
    type options = {
      className?: string,
      checked?: bool,
      disabled?: bool,
      textValue?: bool,
      onCheckedChange?: bool => unit,
      onSelect?: ReactEvent.Mouse.t => unit,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "CheckboxItem"
  }
  module Item = {
    type options = {
      className?: string,
      textValue?: string,
      asChild?: bool,
      disabled?: bool,
      onSelect?: ReactEvent.Mouse.t => unit,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Item"
  }
  module SubTrigger = {
    type options = {
      className?: string,
      textValue?: string,
      asChild?: bool,
      disabled?: bool,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "SubTrigger"
  }
  module SubContent = {
    type options = {
      className?: string,
      arrowPadding?: int,
      sideOffset?: int,
      alignOffset?: int,
      asChild?: bool,
    }

    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "SubContent"
  }
  module Arrow = {
    type props = {className?: string}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Arrow"
  }
  module Separator = {
    type props = {className?: string}
    @module("@radix-ui/react-dropdown-menu")
    external make: React.component<props> = "Separator"
  }
}
module ScrollArea = {
  type type_ =
    | @as("auto") Auto
    | @as("always") Always
    | @as("hover") Hover
    | @as("scroll") Scroll

  module Root = {
    type options = {
      asChild?: bool,
      @as("type") type_?: type_,
      scrollHideDelay?: int,
      className?: string,
    }
    type props = {
      ...options,
      children: React.element,
    }
    @module("@radix-ui/react-scroll-area")
    external make: React.component<props> = "Root"
  }
  module Viewport = {
    type props = {asChild?: string, className?: string, children: React.element}
    @module("@radix-ui/react-scroll-area")
    external make: React.component<props> = "Viewport"
  }
  module Thumb = {
    type props = {asChild?: string, className?: string}
    @module("@radix-ui/react-scroll-area")
    external make: React.component<props> = "Thumb"
  }
  module Corner = {
    type props = {asChild?: string, className?: string}
    @module("@radix-ui/react-scroll-area")
    external make: React.component<props> = "Corner"
  }
  module Scrollbar = {
    type orientation =
      | @as("vertical") Vertical
      | @as("horizontal") Horizontal
    type props = {
      asChild?: string,
      orientation?: orientation,
      className?: string,
      children: React.element,
    }
    @module("@radix-ui/react-scroll-area")
    external make: React.component<props> = "Scrollbar"
  }
}

module Toast = {
  module Provider = {
    type swipeDirection =
      | @as("right") Right
      | @as("left") Left
      | @as("up") Up
      | @as("Down") Down

    type props = {
      duration?: int,
      swipeThreshold?: int,
      label?: string,
      swipeDirection?: swipeDirection,
      className?: string,
      children: React.element,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Provider"
  }
  module Viewport = {
    type props = {
      asChild?: string,
      hotkey?: array<string>,
      className?: string,
      label?: string,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Viewport"
  }
  module Root = {
    type props = {
      asChild?: string,
      defaultOpen?: bool,
      @as("open") open_?: bool,
      onOpenChange?: bool => unit,
      onPause?: unit => unit,
      onResume?: unit => unit,
      onSwipeStart?: ReactEvent.Mouse.t => unit,
      onSwipeMove?: ReactEvent.Mouse.t => unit,
      onSwipeEnd?: ReactEvent.Mouse.t => unit,
      onSwipeCancel?: ReactEvent.Mouse.t => unit,
      className?: string,
      label?: string,
      children: React.element,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Root"
  }
  module Title = {
    type props = {
      asChild?: string,
      className?: string,
      children: React.element,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Title"
  }
  module Description = {
    type props = {
      asChild?: string,
      className?: string,
      children: React.element,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Description"
  }
  module Action = {
    type props = {
      asChild?: string,
      altText?: string,
      className?: string,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Action"
  }
  module Close = {
    type props = {
      asChild?: string,
      altText?: string,
      className?: string,
      children: React.element,
    }
    @module("@radix-ui/react-toast")
    external make: React.component<props> = "Close"
  }
}
