type rec configurationOptions = {
  clientToken: string,
  site: string,
  service?: string,
  env?: string,
  forwardErrorsToLogs: bool,
  sampleRate: int,
}

@module("@datadog/browser-logs") @scope("datadogLogs")
external init: configurationOptions => unit = "init"

@module("@datadog/browser-logs") @scope(("datadogLogs", "logger"))
external info: string => unit = "info"
@module("@datadog/browser-logs") @scope(("datadogLogs", "logger"))
external info2: (string, 'a) => unit = "info"
