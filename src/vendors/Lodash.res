@module("lodash/flatten")
external flatten: array<array<'a>> => array<'a> = "default"
@module("lodash/groupBy")
external groupBy: (array<'a>, 'a => 'b) => Js.Dict.t<array<'a>> = "default"
@module("lodash/sortBy")
external sortBy: (array<'a>, 'a => 'b) => array<'a> = "default"
@module("lodash/uniq")
external uniq: array<'a> => array<'a> = "default"
@module("lodash/uniqBy")
external uniqBy: (array<'a>, 'a => 'b) => array<'a> = "default"
@module("lodash/orderBy")
external orderBy: (array<'a>, 'a => 'b, ~orders: array<string>=?, unit) => array<'a> = "default"
@module("lodash/difference")
external difference: (array<'a>, array<'a>) => array<'a> = "default"
@module("lodash") external capitalize: string => string = "capitalize"
@module("lodash")
external cloneDeep: 'a => 'a = "cloneDeep"
@module("lodash")
external chunk: (array<'a>, int) => array<array<'a>> = "chunk"
@module("lodash")
external isEmpty: 'a => bool = "isEmpty"
@module("lodash/debounce")
external debounce: ('a => unit, int) => 'b = "default"
