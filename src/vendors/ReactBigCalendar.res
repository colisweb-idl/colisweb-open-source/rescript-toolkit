type event<'a> = {
  title: string,
  start: Js.Date.t,
  end: Js.Date.t,
  resource: 'a,
}

type view = [
  | #month
  | #week
  | #work_week
  | #day
  | #agenda
]

type eventProp<'a> = {
  resource: 'a,
  start: Js.Date.t,
  end: Js.Date.t,
  isSelected: bool,
  title: string,
}

type eventClassStyle

@obj
external makeEventClassStyle: (
  ~className: string=?,
  ~style: ReactDOM.Style.t=?,
  unit,
) => eventClassStyle = ""

type timeSlot = {
  start: Js.Date.t,
  end: Js.Date.t,
}

@module("react-big-calendar") @react.component
external make: (
  ~localizer: 'a,
  ~events: array<event<'b>>,
  ~defaultView: view=?,
  ~views: array<view>=?,
  ~culture: string=?,
  ~toolbar: bool=?,
  ~selectable: bool=?,
  ~timeslots: int=?,
  ~min: Js.Date.t=?,
  ~max: Js.Date.t=?,
  ~step: int=?,
  ~components: 'z=?,
  ~eventPropGetter: eventProp<'b> => eventClassStyle=?,
  ~messages: Js.t<'message>=?,
  ~formats: Js.t<'formats>=?,
  ~onSelectEvent: event<'b> => unit=?,
  ~onView: view => unit=?,
  ~onNavigate: Js.Date.t => unit=?,
  ~onSelectSlot: timeSlot => unit=?,
) => React.element = "Calendar"

type calendarLocalizer

@module("react-big-calendar")
external dateFnsLocalizer: 'a => calendarLocalizer = "dateFnsLocalizer"

module Constants = {
  @module("react-big-calendar/lib/utils/constants") @scope("navigate")
  external previous: string = "PREVIOUS"

  @module("react-big-calendar/lib/utils/constants") @scope("navigate")
  external next: string = "NEXT"
}
