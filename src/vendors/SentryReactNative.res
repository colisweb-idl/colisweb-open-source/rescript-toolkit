module Sentry = {
  module Scope = {
    type t

    @send external setExtra: (t, string, 'a) => unit = "setExtra"
  }

  @module("@sentry/react-native")
  external init: 'a => unit = "init"

  @module("@sentry/react-native")
  external setTag: (string, string) => unit = "setTag"

  @module("@sentry/react-native")
  external setUser: 'a => unit = "setUser"

  @module("@sentry/react-native")
  external captureException: Js.Promise.error => unit = "captureException"

  @module("@sentry/react-native")
  external captureMessage: string => unit = "captureMessage"

  @module("@sentry/react-native")
  external withScope: (Scope.t => unit) => unit = "withScope"
}
