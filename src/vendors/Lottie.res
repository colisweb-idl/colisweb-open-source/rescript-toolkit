@module("lottie-react") @react.component
external make: (~animationData: string, ~loop: bool=?) => React.element = "default"
