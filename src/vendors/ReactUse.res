@module("react-use")
external useDebounce: (unit => unit, int, array<'a>) => (Js.Nullable.t<bool>, unit => unit) =
  "useDebounce"

@module("react-use")
external useDebounce2: (unit => unit, int, ('a, 'b)) => (Js.Nullable.t<bool>, unit => unit) =
  "useDebounce"

@module("react-use")
external useDebounce3: (unit => unit, int, ('a, 'b, 'c)) => (Js.Nullable.t<bool>, unit => unit) =
  "useDebounce"

@module("react-use")
external useDebounce4: (
  unit => unit,
  int,
  ('a, 'b, 'c, 'd),
) => (Js.Nullable.t<bool>, unit => unit) = "useDebounce"

@module("react-use")
external useTimeoutFn: (unit => unit, int) => (Js.Nullable.t<bool>, unit => unit) = "useTimeoutFn"

@module("react-use") external usePrevious: 'a => option<'a> = "usePrevious"

@module("react-use")
external useFirstMountState: unit => bool = "useFirstMountState"

@module("react-use")
external useMountedState: (. unit) => (. unit) => bool = "useMountedState"

@module("react-use")
external useUpdateEffect: (unit => option<unit => unit>, array<'a>) => unit = "useUpdateEffect"

@module("react-use")
external useUpdateEffect2: (unit => option<unit => unit>, ('a, 'b)) => unit = "useUpdateEffect"

@module("react-use")
external useUpdateEffect3: (unit => option<unit => unit>, ('a, 'b, 'c)) => unit = "useUpdateEffect"

@module("react-use")
external usePreviousDistinct: (~compare: option<('a, 'b) => bool>=?, 'a, unit) => 'a =
  "usePreviousDistinct"

@module("react-use")
external useGetSet: (unit => 'a) => ('a, 'a => 'a) = "useGetSet"

@module("react-use")
external useHover: React.element => (React.element, bool) = "useHover"

type intersectionObserver = {intersectionRatio: float}

type intersectionOptions<'domElement> = {
  root: Js.Nullable.t<'domElement>,
  rootMargin: string,
  threshold: float,
}

@module("react-use")
external useIntersection: (
  React.ref<'a>,
  intersectionOptions<'domElement>,
) => Js.Nullable.t<intersectionObserver> = "useIntersection"

type scrollCoords = {
  x: int,
  y: int,
}

@module("react-use")
external useWindowScroll: unit => scrollCoords = "useWindowScroll"
