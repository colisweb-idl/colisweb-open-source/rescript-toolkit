type config

module Headers = {
  type t

  external fromObj: Js.t<{..}> => t = "%identity"
  external fromDict: Js.Dict.t<string> => t = "%identity"
}

module CancelToken = {
  type t

  @module("axios")
  external source: unit => t = "source"
}

type requestTransformer<'data, 'a, 'transformedData> = ('data, Js.t<'a>) => 'transformedData
type responseTransformer<'data, 'transformedData> = 'data => 'transformedData
type paramsSerializer<'params, 'serializedParams> = 'params => 'serializedParams
type auth = {
  username: string,
  password: string,
}
type proxy = {host: int, port: int, auth: auth}

type response<'data> = {
  data: 'data,
  status: int,
  statusText: string,
  headers: Js.Dict.t<string>,
  config: config,
}

type error<'responseData> = {
  request: option<Js.Json.t>,
  response: option<response<'responseData>>,
  message: string,
}

type adapter<'a, 'b, 'err> = config => Promise.Js.t<response<'a>, 'err>

@module("axios")
external isAxiosError: error<'a> => bool = "isAxiosError"

@obj
external makeConfig: (
  ~url: string=?,
  ~_method: string=?,
  ~baseURL: string=?,
  ~transformRequest: array<requestTransformer<'data, Js.t<'a>, 'tranformedData>>=?,
  ~transformResponse: array<responseTransformer<'data, 'tranformedData>>=?,
  ~headers: Headers.t=?,
  ~params: Js.t<'params>=?,
  ~paramsSerializer: paramsSerializer<'params, 'serializedParams>=?,
  ~data: Js.t<'data>=?,
  ~timeout: int=?,
  ~withCredentials: bool=?,
  ~adapter: adapter<'a, 'b, 'err>=?,
  ~auth: auth=?,
  ~responseType: string=?,
  ~xsrfCookieName: string=?,
  ~xsrfHeaderName: string=?,
  ~onUploadProgress: 'uploadProgress => unit=?,
  ~onDownloadProgress: 'downloadProgress => unit=?,
  ~maxContentLength: int=?,
  ~validateStatus: int => bool=?,
  ~maxRedirects: int=?,
  ~socketPath: string=?,
  ~proxy: proxy=?,
  ~cancelToken: CancelToken.t=?,
  unit,
) => config = ""

@module("axios")
external get: (string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> = "get"

@module("axios")
external post: (string, ~data: 'a, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> =
  "post"

@module("axios")
external put: (string, ~data: 'a, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> =
  "put"

@module("axios")
external patch: (string, ~data: 'a, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> =
  "patch"

@module("axios")
external delete: (string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> = "delete"

@module("axios")
external options: (string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> = "options"

module Instance = {
  type t

  @module("axios")
  external create: config => t = "create"

  @send
  external get: (t, string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> = "get"

  @send
  external post: (
    t,
    string,
    ~data: 'a,
    ~config: config,
    unit,
  ) => Promise.Js.t<response<'data>, 'err> = "post"

  @send
  external put: (
    t,
    string,
    ~data: 'a,
    ~config: config,
    unit,
  ) => Promise.Js.t<response<'data>, 'err> = "put"

  @send
  external patch: (
    t,
    string,
    ~data: 'a,
    ~config: config,
    unit,
  ) => Promise.Js.t<response<'data>, 'err> = "patch"

  @send
  external delete: (t, string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> =
    "delete"

  @send
  external options: (t, string, ~config: config, unit) => Promise.Js.t<response<'data>, 'err> =
    "options"

  module Interceptors = {
    type interceptor
    module Request = {
      @send @scope(("interceptors", "request"))
      external use: (t, 'config => Promise.Js.t<'updatedConfig, 'error>) => interceptor = "use"

      @send @scope(("interceptors", "request"))
      external eject: (t, interceptor) => unit = "eject"
    }

    module Response = {
      @send @scope(("interceptors", "response"))
      external use: (t, 'res => 'res, error<'z> => 'a) => unit = "use"

      @send @scope(("interceptors", "response"))
      external eject: (t, interceptor) => unit = "eject"
    }
  }
}

module Interceptors = {
  type t
  module Request = {
    @module("axios") @scope(("default", "interceptors", "request"))
    external use: ('config => Promise.Js.t<'updatedConfig, 'error>) => t = "use"

    @module("axios") @scope(("default", "interceptors", "request"))
    external eject: t => unit = "eject"
  }

  module Response = {
    @module("axios") @scope(("default", "interceptors", "response"))
    external use: ('res => 'res, error<'z> => 'a) => unit = "use"

    @module("axios") @scope(("default", "interceptors", "response"))
    external eject: t => unit = "eject"
  }
}

module WithResult = {
  type customError<'apiError, 'response> = [
    | #default(error<'response>)
    | #decodeError(config, Spice.decodeError)
    | #custom('apiError)
  ]

  type decodeData<'newData> = Js.Json.t => result<'newData, Spice.decodeError>

  type mapError<'a, 'response, 'headers, 'request> = error<'response> => customError<'a, 'response>

  let toResult = (promise, ~mapError, ~decodeData: decodeData<'a>) =>
    promise
    ->Promise.Js.toResult
    ->Promise.mapError(err => mapError->Option.mapWithDefault(#default(err), fn => fn(err)))
    ->Promise.flatMapOk(response => {
      Promise.resolved(
        switch decodeData(response.data) {
        | Ok(_) as ok => ok
        | Error(decodeError) => Error(#decodeError(response.config, decodeError))
        },
      )
    })

  let get = (
    ~instance=?,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => get(string, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.get(string, ~config, ())->toResult(~decodeData, ~mapError)
    }

  let post = (
    ~instance=?,
    ~data,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => post(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.post(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    }

  let put = (
    ~instance=?,
    ~data,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => put(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.put(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    }

  let patch = (
    ~instance=?,
    ~data,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => patch(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.patch(string, ~data, ~config, ())->toResult(~decodeData, ~mapError)
    }

  let delete = (
    ~instance=?,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => delete(string, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.delete(string, ~config, ())->toResult(~decodeData, ~mapError)
    }

  let options = (
    ~instance=?,
    ~config,
    ~decodeData: decodeData<'newData>,
    ~mapError: option<mapError<'a, 'response, 'headers, 'request>>=?,
    string,
  ) =>
    switch instance {
    | None => options(string, ~config, ())->toResult(~decodeData, ~mapError)
    | Some(instance) =>
      instance->Instance.options(string, ~config, ())->toResult(~decodeData, ~mapError)
    }
}
