type placeAutocompleteService

type prediction = {
  @as("place_id")
  placeId: string,
  description: string,
  types: array<string>,
}

@deriving(jsConverter)
type addressComponentType = [
  | #premises
  | @as("street_number") #streetNumber
  | @as("street_address") #streetAddress
  | #route
  | #locality
  | @as("postal_code") #postalCode
  | #country
]

@deriving(jsConverter)
type placeServiceStatus = [
  | @as("OK") #ok
  | @as("INVALID_REQUEST") #invalidRequest
  | @as("NOT_FOUND") #notFound
  | @as("OVER_QUERY_LIMIT") #overQueryLimit
  | @as("REQUEST_DENIED") #requestDenied
  | @as("UNKNOWN_ERROR") #unknownError
  | @as("ZERO_RESULTS") #zeroResults
]

@new
external createPlaceAutocompleteService: unit => placeAutocompleteService =
  "google.maps.places.AutocompleteService"

@send
external _getPlacePredictions: (
  placeAutocompleteService,
  'options,
  (Js.Nullable.t<array<prediction>>, string) => unit,
) => unit = "getPlacePredictions"

let getPlacePredictions = (
  ~location: option<Toolkit.Decoders.Coordinates.t>=?,
  ~sessionToken=?,
  ~country="fr",
  query,
  (),
): Promise.t<result<array<prediction>, string>> => {
  let placeAutocompleteService = createPlaceAutocompleteService()

  let _restrictedTypes = [#streetAddress, #premises]->Array.map(addressComponentTypeToJs)

  Promise.exec(resolve =>
    _getPlacePredictions(
      placeAutocompleteService,
      {
        "input": query,
        "types": ["address"],
        "sessionToken": sessionToken,
        "location": location->Option.map(
          ({latitude, longitude}) => {"lat": () => latitude, "lng": () => longitude},
        ),
        "radius": location->Option.map(_ => 20000),
        "bounds": Js.undefined,
        "componentRestrictions": {
          "country": country,
        },
      },
      (predictions, status) =>
        switch status->placeServiceStatusFromJs->Option.getExn {
        | #ok =>
          Js.Global.setTimeout(
            _ =>
              resolve(
                Ok(
                  predictions
                  ->Js.Nullable.toOption
                  ->Option.getWithDefault([]),
                  // ->Array.keep(prediction =>
                  //     restrictedTypes
                  //     ->Array.getBy(Js.Array.includes(_, prediction.types))
                  //     ->Option.isSome
                  //   ),
                ),
              ),
            1000,
          )->ignore

        | #zeroResults => resolve(Ok([]))

        | _ => resolve(Error(status))
        },
    )
  )
}
