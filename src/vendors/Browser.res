module LocalStorage = {
  @val
  external getItem: string => Js.Nullable.t<string> = "localStorage.getItem"
  @val
  external setItem: (string, Js.Nullable.t<string>) => unit = "localStorage.setItem"
  @val
  external removeItem: string => unit = "localStorage.removeItem"
}

module Location = {
  type t

  @val
  external location: t = "window.location"

  @get
  external host: t => string = "host"
  @get
  external pathname: t => string = "pathname"
  @get
  external hash: t => string = "hash"
  @get
  external href: t => string = "href"
  @get
  external search: t => string = "search"

  @send
  external replace: (t, string) => unit = "replace"
  @send
  external assign: (t, string) => unit = "assign"
}

module Document = {
  @val external addEventListener: (string, ReactEvent.Form.t => unit) => unit = "addEventListener"
  @val
  external removeEventListener: (string, ReactEvent.Form.t => unit) => unit = "removeEventListener"

  module Element = {
    type t

    @val
    external make: string => t = "document.createElement"
    @set
    external setSrc: (t, string) => unit = "src"
    @set
    external setHref: (t, string) => unit = "href"
    @set
    external setHref: (t, string) => unit = "href"
    @set
    external setRel: (t, string) => unit = "rel"
    @set
    external setAsync: (t, bool) => unit = "async"

    @set
    external onLoad: (t, unit => unit) => unit = "onload"
  }

  module Head = {
    @val
    external append: Element.t => unit = "document.head.append"
  }
}

module DomElement = {
  type rect = {
    x: float,
    y: float,
    width: float,
    height: float,
    left: float,
    right: float,
    top: float,
    bottom: float,
  }
  @send external getBoundingClientRect: Dom.element => rect = "getBoundingClientRect"

  @send external focus: Dom.element => unit = "focus"

  external toDomElement: 'a => Dom.element = "%identity"

  @send
  external addEventListener: (Dom.element, string, ReactEvent.Form.t => unit) => unit =
    "addEventListener"
  @send
  external removeEventListener: (Dom.element, string, ReactEvent.Form.t => unit) => unit =
    "removeEventListener"

  module ClassList = {
    @send @scope("classList")
    external contains: (Dom.element, string) => bool = "contains"
  }

  module Node = {
    @send
    external contains: (Dom.element, Dom.element) => bool = "contains"
  }
}

@val
external innerWidth: int = "window.innerWidth"

@val
external screenWidth: int = "window.screen.width"

@val
external screenHeight: int = "window.screen.height"

module Navigator = {
  type t

  @val
  external userLanguage: option<string> = "window.navigator.userLanguage"

  @val external language: option<string> = "window.navigator.language"

  let getBrowserLanguage = () =>
    switch (userLanguage, language) {
    | (Some(l), _) => l
    | (_, Some(l)) => l
    | (None, None) => "fr"
    }

  module Clipboard = {
    @val
    external writeText: string => unit = "navigator.clipboard.writeText"
  }
}

module URL = {
  type t

  @new
  external make: string => t = "URL"

  @new
  external makeFromLocation: Location.t => t = "URL"

  @send @scope("searchParams")
  external setSearchParams: (t, string, string) => unit = "set"
}

module URLSearchParams = {
  type t

  @new
  external make: string => t = "URLSearchParams"

  @send
  external set: (t, string, string) => unit = "set"
  @send
  external delete: (t, string) => unit = "delete"
  @send
  external toString: (t, unit) => string = "toString"
}

module History = {
  type t

  @val
  external history: t = "window.history"

  @send
  external back: (t, unit) => unit = "back"

  @send
  external pushState: (t, Js.t<'a>, string, string) => unit = "pushState"

  @send
  external pushStateWithURL: (t, Js.t<'a>, string, URL.t) => unit = "pushState"
}

module FormData = {
  type t

  module File = {
    type t
  }

  @new external create: unit => t = "FormData"

  @send external append: (t, string, 'a) => unit = "append"
  // append(formData, fieldName(string), file, fileName(string))
  @send external appendWithFileName: (t, string, 'a, string) => unit = "append"
  @send external get: (t, string) => Js.Nullable.t<'a> = "get"
  @send external getAll: (t, string) => Js.Nullable.t<array<'a>> = "getAll"
  @send external has: (t, string) => bool = "has"
}

module Window = {
  @val
  external addEventListener: (string, ReactEvent.Form.t => unit) => unit = "window.addEventListener"
  @val
  external removeEventListener: (string, ReactEvent.Form.t => unit) => unit =
    "window.removeEventListener"
}
