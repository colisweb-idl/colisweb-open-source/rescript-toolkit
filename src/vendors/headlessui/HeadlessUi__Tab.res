open HeadlessUi__Shared

type renderProps = {selected: bool}
@module("@headlessui/react") @react.component
external make: (
  ~children: React.element,
  ~\"as": as_<'a>=?,
  ~disabled: bool=?,
  ~className: string=?,
) => React.element = "Tab"

module RenderTab = {
  type renderProps = {selected: bool}
  @module("@headlessui/react") @react.component
  external make: (
    ~children: renderProps => React.element,
    ~\"as": as_<'a>=?,
    ~disabled: bool=?,
    ~className: string=?,
  ) => React.element = "Tab"
}

module Group = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: React.element,
    ~defaultIndex: int=?,
    ~selectedIndex: int=?,
    ~onChange: int => unit=?,
  ) => React.element = "TabGroup"
}

module GroupRender = {
  type renderProps = {selectedIndex: int}
  @module("@headlessui/react") @react.component
  external make: (
    ~children: renderProps => React.element,
    ~defaultIndex: int=?,
    ~selectedIndex: int=?,
    ~onChange: int => unit=?,
  ) => React.element = "TabGroup"
}

module List = {
  @module("@headlessui/react") @react.component
  external make: (~children: React.element, ~className: string=?) => React.element = "TabList"
}

module Panels = {
  @module("@headlessui/react") @react.component
  external make: (~children: React.element, ~className: string=?) => React.element = "TabPanels"
}
module Panel = {
  @module("@headlessui/react") @react.component
  external make: (~children: React.element, ~className: string=?) => React.element = "TabPanel"
}
