@module("react")
external fragment: React.element = "Fragment"

@unboxed
type rec as_<'a> = Node('a): as_<'a>
