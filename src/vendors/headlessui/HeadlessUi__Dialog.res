@module("@headlessui/react") @react.component
external make: (
  ~\"open": bool,
  ~onClose: unit => unit,
  ~children: React.element,
  ~className: string=?,
  ~transition: bool=?,
  ~style: ReactDOM.Style.t=?,
) => React.element = "Dialog"

module Panel = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~transition: bool=?,
    ~style: ReactDOM.Style.t=?,
  ) => React.element = "DialogPanel"
}
module Title = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~style: ReactDOM.Style.t=?,
  ) => React.element = "DialogTitle"
}
module Backdrop = {
  @module("@headlessui/react") @react.component
  external make: (
    ~className: string=?,
    ~transition: bool=?,
    ~style: ReactDOM.Style.t=?,
  ) => React.element = "DialogBackdrop"
}
