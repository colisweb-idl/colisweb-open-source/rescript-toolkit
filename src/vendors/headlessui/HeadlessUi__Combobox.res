open HeadlessUi__Shared

type rec suggestions<'value> = array<suggestion<'value>>
and suggestion<'value> = {
  label: string,
  value: 'value,
}
@module("@headlessui/react") @react.component
external make: (
  ~value: Js.Nullable.t<suggestion<'value>>=?,
  ~defaultValue: suggestion<'value>=?,
  ~nullable: bool=?,
  ~children: React.element,
  ~onChange: Js.Nullable.t<suggestion<'value>> => unit,
) => React.element = "Combobox"

module Input = {
  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (
    ~onChange: ReactEvent.Form.t => unit,
    ~className: string=?,
    ~placeholder: string=?,
    ~autocomplete: string=?,
    ~id: string=?,
    ~autoFocus: bool=?,
    ~displayValue: Js.Nullable.t<suggestion<'value>> => string=?,
    ~by: 'by=?,
  ) => React.element = "Input"
}

module Label = {
  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (~children: React.element, ~className: string=?) => React.element = "Label"
}

module Options = {
  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (
    ~children: React.element,
    ~transition: bool=?,
    ~className: string=?,
  ) => React.element = "Options"
}

module Option = {
  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (
    ~children: React.element,
    ~value: 'value,
    ~className: string=?,
    ~disabled: bool=?,
  ) => React.element = "Option"
}

module Button = {
  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~disabled: bool=?,
  ) => React.element = "Button"
}

module OptionRender = {
  type render = {
    active: bool,
    selected: bool,
  }

  @module("@headlessui/react") @scope("Combobox") @react.component
  external make: (
    ~children: render => React.element,
    ~value: 'value,
    ~className: string=?,
    ~disabled: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "Option"
}
