open HeadlessUi__Shared

@module("@headlessui/react") @react.component
external make: (
  ~children: React.element,
  ~className: string=?,
  ~defaultOpen: bool=?,
  ~\"as": as_<'a>=?,
) => React.element = "Disclosure"

module Render = {
  type render = {\"open": bool, close: unit => unit}

  @module("@headlessui/react") @react.component
  external make: (
    ~children: render => React.element,
    ~className: string=?,
    ~defaultOpen: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "Disclosure"
}

module Button = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "DisclosureButton"
}

module Panel = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~static: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "DisclosurePanel"
}

module PanelRender = {
  type render = {\"open": bool, close: unit => unit}

  @module("@headlessui/react") @react.component
  external make: (
    ~children: render => React.element,
    ~className: string=?,
    ~static: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "DisclosurePanel"
}
