open HeadlessUi__Shared

@module("@headlessui/react") @react.component
external make: (
  ~show: bool=?,
  ~children: React.element,
  ~enter: string=?,
  ~enterFrom: string=?,
  ~enterTo: string=?,
  ~leave: string=?,
  ~leaveFrom: string=?,
  ~leaveTo: string=?,
  ~className: string=?,
  ~\"as": as_<'a>=?,
) => React.element = "Transition"

module Child = {
  @module("@headlessui/react") @scope("Transition") @react.component
  external make: (
    ~children: React.element,
    ~enter: string=?,
    ~enterFrom: string=?,
    ~enterTo: string=?,
    ~leave: string=?,
    ~leaveFrom: string=?,
    ~leaveTo: string=?,
    ~className: string=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "Child"
}
