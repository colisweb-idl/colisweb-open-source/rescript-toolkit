open HeadlessUi__Shared

type renderProps = {selected: bool}

@module("@headlessui/react") @react.component
external make: (~children: React.element, ~\"as": as_<'a>=?, ~disabled: bool=?) => React.element =
  "Menu"

module Render = {
  @module("@headlessui/react") @react.component
  external make: (
    ~children: renderProps => React.element,
    ~\"as": as_<'a>=?,
    ~disabled: bool=?,
  ) => React.element = "Menu"
}

module Button = {
  @module("@headlessui/react") @scope("Menu") @react.component
  external make: (~children: React.element, ~className: string=?) => React.element = "Button"
}

module Items = {
  @module("@headlessui/react") @scope("Menu") @react.component
  external make: (
    ~children: React.element,
    ~static: bool=?,
    ~className: string=?,
  ) => React.element = "Items"
}

module ItemsRender = {
  type render = {\"open": bool}
  @module("@headlessui/react") @scope("Menu") @react.component
  external make: (
    ~children: render => React.element,
    ~static: bool=?,
    ~className: string=?,
  ) => React.element = "Items"
}

module Item = {
  @module("@headlessui/react") @scope("Menu") @react.component
  external make: (
    ~children: React.element,
    ~className: string=?,
    ~disabled: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "Item"
}

module ItemRender = {
  type render = {active: bool}
  @module("@headlessui/react") @scope("Menu") @react.component
  external make: (
    ~children: render => React.element,
    ~className: string=?,
    ~disabled: bool=?,
    ~\"as": as_<'a>=?,
  ) => React.element = "Item"
}
