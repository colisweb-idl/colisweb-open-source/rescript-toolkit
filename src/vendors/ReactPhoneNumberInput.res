@module("react-phone-number-input") @react.component
external make: (
  ~placeholder: string=?,
  ~value: string=?,
  ~countryCallingCodeEditable: bool=?,
  ~international: bool=?,
  ~defaultCountry: string=?,
  ~onChange: string => unit=?,
  ~onBlur: 'a => unit=?,
  ~className: string=?,
  ~id: string=?,
) => React.element = "default"

@module("react-phone-number-input")
external isPossiblePhoneNumber: string => bool = "isPossiblePhoneNumber"

type phoneNumber = {
  country: string,
  countryCallingCode: string,
  number: string,
  nationalNumber: string,
}
@module("react-phone-number-input")
external parsePhoneNumber: string => option<phoneNumber> = "parsePhoneNumber"
