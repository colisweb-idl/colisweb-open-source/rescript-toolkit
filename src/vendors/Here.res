type coordinates = {
  lat: float,
  lng: float,
}

module Geo = {
  module Point = {
    type t

    @new
    external make: (float, float) => t = "H.geo.Point"
  }
  module Rect = {
    type t
  }
  module LineString = {
    type t
    @val
    external fromFlexiblePolyline: 'polyline => t = "H.geo.LineString.fromFlexiblePolyline"

    @send
    external getLatLngAltArray: (t, unit) => array<float> = "getLatLngAltArray"
  }
}

module Event = {
  type t<'data>

  module Target = {
    type t<'data>

    type data = {maxZoom: int}

    @send
    external getData: (t<'data>, unit) => 'data = "getData"
    @send
    external getObjectData: (t<'data>, unit) => data = "getData"
    @send
    external getGeometry: (t<'data>, unit) => Geo.Point.t = "getGeometry"
    @send
    external getBoundingBox: (t<'data>, unit) => Geo.Rect.t = "getBoundingBox"
  }

  @get
  external target: t<'data> => Target.t<'data> = "target"
}

module Layers = {
  type map

  type rec t = {vector: vector}
  and vector = {normal: normalVector}
  and normalVector = {map: map}
}

module Url = {
  module MultiValueQueryParameter = {
    type t

    @new
    external make: array<string> => t = "H.service.Url.MultiValueQueryParameter"
  }
}

module Platform = {
  type t
  type options = {apikey: string}

  @new
  external make: options => t = "H.service.Platform"

  type layersOptions = {pois: bool}

  module Router = {
    type t

    type routeRequestParams = {
      routingMode: string,
      transportMode: string,
      origin: string,
      destination: string,
      return: string,
      via?: Url.MultiValueQueryParameter.t,
    }

    type rec result = {routes: array<route>}
    and route = {sections: array<section>}
    and section = {
      polyline: string,
      id: string,
      arrival: location,
      departure: location,
      transport: transport,
    }
    and transport = {mode: string}
    and location = {
      place: place,
      time: string,
    }
    and place = {
      location: coordinates,
      originalLocation: coordinates,
      @as("type") type_: string,
      waypoint?: int,
    }

    @send
    external calculateRoute: (t, routeRequestParams, result => unit, 'error => unit) => unit =
      "calculateRoute"
  }

  @send
  external createDefaultLayers: (t, layersOptions) => Layers.t = "createDefaultLayers"
  @send
  external getRoutingService: (t, 'a, int) => Router.t = "getRoutingService"
}

module Map = {
  type instance
  type object
  type objectLayer
  type padding = {top?: int, left?: int, bottom?: int, right?: int}

  type options = {
    zoom: int,
    center: coordinates,
    padding?: padding,
  }

  @new
  external make: (Dom.element, Layers.map, options) => instance = "H.Map"

  module Icon = {
    type t

    type rec options = {
      size?: size,
      anchor?: anchor,
    }
    and size = {
      w: int,
      h: int,
    }
    and anchor = {
      x: float,
      y: float,
    }

    @new
    external make: (string, options) => t = "H.map.Icon"
  }
  module DomIcon = {
    type t
    @new
    external make: string => t = "H.map.DomIcon"
  }

  module Marker = {
    type t

    type options = {icon?: Icon.t, min?: int, max?: int}
    @new
    external makeWithCoordinates: (coordinates, options) => t = "H.map.Marker"
    @new
    external makeWithGeoPoint: (Geo.Point.t, options) => t = "H.map.Marker"

    @send
    external setData: (t, string) => unit = "setData"
    @send
    external addEventListener: (t, string, Event.t<'a> => unit) => unit = "addEventListener"

    external toObject: t => object = "%identity"
  }
  module DomMarker = {
    type t

    type options = {icon?: DomIcon.t, min?: int, max?: int}

    @new
    external makeWithCoordinates: (coordinates, options) => t = "H.map.DomMarker"
    @new
    external makeWithGeoPoint: (Geo.Point.t, options) => t = "H.map.DomMarker"

    @send
    external setData: (t, string) => unit = "setData"
    @send
    external addEventListener: (t, string, Event.t<'a> => unit) => unit = "addEventListener"

    external toObject: t => object = "%identity"
  }

  module Viewport = {
    type t

    @send
    external resize: (t, unit) => unit = "resize"
    @send
    external setPadding: (t, int, int, int, int) => unit = "setPadding"
  }
  module ViewModel = {
    type t

    type options = {
      zoom?: int,
      bounds: Geo.Rect.t,
    }

    @send
    external setLookAtData: (t, options) => unit = "setLookAtData"

    @unboxed
    type animationOption =
      | Boolean(bool)
      | Number(float)
    @send
    external setLookAtDataWithAnimateOption: (t, options, animationOption) => unit = "setLookAtData"
  }

  module Group = {
    type t
    @new
    external make: unit => t = "H.map.Group"

    type makeOptions = {objects: array<object>}
    @new
    external makeFromObjects: makeOptions => t = "H.map.Group"

    @send
    external addEventListener: (t, string, Event.t<'data> => unit) => unit = "addEventListener"

    @send
    external addMarker: (t, Marker.t) => unit = "addObject"
    @send
    external addDomMarker: (t, DomMarker.t) => unit = "addObject"
    @send
    external getBoundingBox: (t, unit) => Geo.Rect.t = "getBoundingBox"
    @send
    external removeAll: (t, unit) => unit = "removeAll"
    @send
    external removeObject: (t, object) => unit = "removeObject"
    @send
    external removeObjects: (t, array<object>) => unit = "removeObjects"

    external toObject: t => object = "%identity"
  }
  module Polyline = {
    type t

    type style = {
      ...JsxDOMStyle.t,
      lineWidth: int,
      strokeColor: string,
    }
    type options = {style?: style}

    @new
    external make: (Geo.LineString.t, options) => t = "H.map.Polyline"

    @send
    external getBoundingBox: (t, unit) => Geo.Rect.t = "getBoundingBox"
  }
  module Circle = {
    type t

    type rec options = {style?: style}
    and style = {fillColor?: string}
    @new
    external make: (Geo.Point.t, int, options) => t = "H.map.Circle"
    external toObject: t => object = "%identity"

    type dataOptions = {maxZoom?: int}

    @send
    external setData: (t, dataOptions) => unit = "setData"
  }

  @send
  external addMarker: (instance, Marker.t) => unit = "addObject"
  @send
  external addGroup: (instance, Group.t) => unit = "addObject"
  @send
  external addObject: (instance, object) => unit = "addObject"
  @send
  external addPolyline: (instance, Polyline.t) => unit = "addObject"

  @send
  external addLayer: (instance, objectLayer) => unit = "addLayer"
  @send
  external setCenter: (instance, coordinates) => unit = "setCenter"
  @send
  external setCenterWithAnimation: (instance, Geo.Point.t, bool) => unit = "setCenter"
  @send
  external setZoom: (instance, int) => unit = "setZoom"

  @send
  external getObjects: (instance, unit) => array<object> = "getObjects"
  @send
  external getViewPort: (instance, unit) => Viewport.t = "getViewPort"
  @send
  external getViewModel: (instance, unit) => ViewModel.t = "getViewModel"
}
module Clustering = {
  module NoisePoint = {
    type t

    @send
    external getData: (t, unit) => 'data = "getData"
    @send
    external setData: (t, 'data) => unit = "setData"
    @send
    external getPosition: (t, unit) => Geo.Point.t = "getPosition"
    @send
    external getMinZoom: (t, unit) => int = "getMinZoom"
  }

  module Cluster = {
    type t

    @send
    external forEachDataPoint: (t, NoisePoint.t => unit) => unit = "forEachDataPoint"
    @send
    external getBoundingBox: (t, unit) => Geo.Rect.t = "getBoundingBox"
    @send
    external getPosition: (t, unit) => Geo.Point.t = "getPosition"
    @send
    external getMinZoom: (t, unit) => int = "getMinZoom"
    @send
    external getMaxZoom: (t, unit) => int = "getMaxZoom"
  }

  type theme = {
    getClusterPresentation: Cluster.t => Map.Marker.t,
    getNoisePresentation: NoisePoint.t => Map.Marker.t,
  }

  module DataPoint = {
    type t<'a>
    @new
    external make: (float, float, Js.Nullable.t<float>, 'data) => t<'data> =
      "H.clustering.DataPoint"
  }
  module Provider = {
    type t<'a>

    type rec options = {clusteringOptions: clusteringOptions, theme?: theme}
    and clusteringOptions = {
      eps: int,
      // minimum weight of points required to form a cluster
      minWeight: int,
    }
    @new
    external make: (array<DataPoint.t<'a>>, options) => t<'a> = "H.clustering.Provider"

    @send
    external addEventListener: (t<'a>, string, Event.t<'a> => unit) => unit = "addEventListener"
  }
}

module MapLayer = {
  module ObjectLayer = {
    @new
    external make: Clustering.Provider.t<'data> => Map.objectLayer = "H.map.layer.ObjectLayer"
  }
}

module MapEvents = {
  type t

  @new
  external make: Map.instance => t = "H.mapevents.MapEvents"
}
module Behavior = {
  type t

  @new
  external make: MapEvents.t => t = "H.mapevents.Behavior"
}
module Ui = {
  type t

  @new
  external createDefault: (Map.instance, Layers.t) => t = "H.ui.UI.createDefault"

  module InfoBubble = {
    type t
    type state =
      | @as("open") Open
      | @as("closed") Closed

    type options = {
      content: string,
      onStateChange?: unknown => unit,
    }
    @new
    external make: (Geo.Point.t, options) => t = "H.ui.InfoBubble"

    @send
    external setPosition: (t, Geo.Point.t) => unit = "setPosition"
    @send
    external setContent: (t, string) => unit = "setContent"
    @send
    external setState: (t, state) => unit = "setState"
    @send
    external open_: (t, unit) => unit = "open"
  }

  @send
  external addBubble: (t, InfoBubble.t) => unit = "addBubble"
}
