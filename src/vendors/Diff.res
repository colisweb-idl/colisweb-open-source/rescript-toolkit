@module("diff")
external _diffJson: ('a, 'b) => 'c = "diffJson"

let diffJson = (value1, value2) => {
  let changes = _diffJson(value1, value2)

  switch changes {
  | [val] if val["added"]->Option.isNone && val["removed"]->Option.isNone => false
  | _ => true
  }
}
