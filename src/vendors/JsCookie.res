module JsCookie = {
  type options = {expires: int}

  @module("js-cookie")
  external set: (string, string, options) => unit = "set"

  @module("js-cookie") external get: string => option<string> = "get"
}
