# Fetcher

## Usage

```rescript
module AccessRestrictedByStoreId =
  Fetcher.Make({
    module Request = UnleashApi.AccessRestrictedByStoreId.Request;
    let key = storeId => [|
      "accessRestrictedByStoreId",
      storeId->StoreId.toString,
    |];
  });

[@react.component]
let make = (~storeId) => {
  let (data, _) = AccessRestrictedByStoreId.use(Some(storeId));
}
```

### Conditional fetching

```rescript
module AccessRestrictedByStoreId =
  Fetcher.Make({
    module Request = UnleashApi.AccessRestrictedByStoreId.Request;
    let key = storeId => [|
      "accessRestrictedByStoreId",
      storeId->StoreId.toString,
    |];
  });

[@react.component]
let make = (~storeId) => {
  let (shouldFetch, setShouldFetch) = React.useState(() => false);

  let (data, _) = AccessRestrictedByStoreId.use(shouldFetch ? Some(storeId) : None);
}
```
