type error<'apiError> =
  | DecodeError(Spice.decodeError)
  | Unknown(Ky.error)
  | Custom('apiError)

type requestConfig<'apiError, 'response> = {
  kyInstance: Ky.Instance.t,
  path: string,
  requestOptions: Ky.requestOptions,
  key?: array<string>,
  mapCustomErrors?: Ky.error => Promise.t<error<'apiError>>,
  mapRawResponse?: result<Js.Json.t, Ky.error> => Promise.t<result<'response, error<'apiError>>>,
}

%%private(
  let fetch = (
    ~instance,
    ~path,
    ~requestOptions,
    ~mapRawResponse=?,
    ~mapCustomErrors=?,
    ~response_decode,
  ) => {
    // TODO :
    // - abort controller signal

    (instance->Ky.Instance.asCallable)(path, ~options=requestOptions)
    ->Ky.Response.jsonFromPromise()
    ->Promise.Js.fromBsPromise
    ->Promise.Js.toResult
    ->Promise.flatMap(result => {
      let result: result<Js.Json.t, Ky.error> = result->Obj.magic

      let mapRawResponseOrDecode = switch mapRawResponse {
      | Some(fn) => fn(result)
      | None =>
        Promise.resolved(
          switch result {
          | Error(err) => Error(Unknown(err))
          | Ok(response) =>
            switch response->response_decode {
            | Ok(response) => Ok(response)
            | Error(decodeError) => Error(DecodeError(decodeError))
            }
          },
        )
      }

      let mappedCustomErrors = mapRawResponseOrDecode->Promise.flatMapError(error => {
        switch error {
        | Unknown(err) =>
          mapCustomErrors->Option.mapWithDefault(
            Promise.resolved(Error(Unknown(err))),
            fn => fn(err)->Promise.map(v => {Error(v)}),
          )
        | err => Promise.resolved(Error(err))
        }
      })

      mappedCustomErrors
    })
  }
)

module type Config = {
  type argument
  type response
  type error
  let response_decode: Js.Json.t => result<response, Spice.decodeError>
  let config: argument => requestConfig<error, response>
}

let fetchAPI = (
  type argument response err,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = err
  ),
  argument: argument,
): Promise.t<result<response, error<err>>> => {
  let module(C) = config
  let requestConfig = C.config(argument)

  fetch(
    ~instance=requestConfig.kyInstance,
    ~path=requestConfig.path,
    ~response_decode=C.response_decode,
    ~requestOptions=requestConfig.requestOptions,
    ~mapRawResponse=?requestConfig.mapRawResponse,
    ~mapCustomErrors=?requestConfig.mapCustomErrors,
  )
}
let useFetcher = (
  type argument response error,
  ~options: option<Swr.fetcherOptions>=?,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = error
  ),
  argument: option<argument>,
): Toolkit__Hooks.fetcher<response> => {
  let module(C) = config

  Toolkit__Hooks.useFetcher(
    ~options?,
    argument->Option.flatMap(argument => {
      let requestConfig = C.config(argument)

      switch requestConfig.key->Obj.magic {
      | None =>
        Js.Exn.raiseError(
          `You are using a config without a key for this path ${requestConfig.path}`,
        )
      | Some(key) => key
      }
    }),
    () => {
      fetchAPI(config, argument->Option.getExn)->Promise.Js.fromResult
    },
  )
}

let useOptionalFetcher = (
  type argument response error,
  ~options: option<Swr.fetcherOptions>=?,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = error
  ),
  argument: option<argument>,
): Toolkit__Hooks.fetcher<option<response>> => {
  let module(C) = config

  Toolkit__Hooks.useOptionalFetcher(
    ~options?,
    argument->Option.flatMap(argument => {
      let requestConfig = C.config(argument)

      switch requestConfig.key->Obj.magic {
      | None =>
        Js.Exn.raiseError(
          `You are using a config without a key for this path ${requestConfig.path}`,
        )
      | Some(key) => key
      }
    }),
    () => {
      fetchAPI(config, argument->Option.getExn)->Promise.Js.fromResult
    },
  )
}

type state<'data, 'error> =
  | NotAsked
  | Loading
  | Done(result<'data, 'error>)

%%private(
  let minInt = -999999999
  let maxInt = 1000000000

  let increment = (num: int): int => num !== maxInt ? num + 1 : minInt
)

// TODO: do real cancelation
let useCancelableRequest = (
  type argument response error,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = error
  ),
) => {
  let module(Config) = config

  let lastCallId = React.useRef(0)
  let canceled = React.useRef(false)
  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState(.)

  let trigger = argument => {
    lastCallId.current = lastCallId.current->increment
    let callId = lastCallId.current

    set(_ => Loading)

    canceled.current = false

    fetchAPI(module(Config), argument)->Promise.map(result => {
      let isCanceled = callId !== lastCallId.current || canceled.current

      if isMounted() && !isCanceled {
        set(_ => Done(result))
      }

      (result, isCanceled)
    })
  }

  let cancel = React.useCallback(() => canceled.current = true, [])

  (state, trigger, cancel)
}

let useManualRequest = (
  type argument response error,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = error
  ),
) => {
  let module(Config) = config

  let (state, set) = React.useState(() => NotAsked)
  let isMounted = ReactUse.useMountedState(.)

  let trigger = argument => {
    set(_ => Loading)

    fetchAPI(module(Config), argument)->Promise.tap(res => {
      if isMounted() {
        set(_ => Done(res))
      }
    })
  }

  (state, trigger)
}

external exnToError: Js.Exn.t => error<'a> = "%identity"

let decodeResponseError = (responseError, decoder) => {
  responseError
  ->Ky.Response.json()
  ->Promise.Js.fromBsPromise
  ->Promise.Js.toResult
  ->Promise.mapError(Obj.magic)
  ->Promise.flatMapOk(json => json->decoder->Promise.resolved)
}

let decodeRawResponse = (result: result<Js.Json.t, Ky.error>, decoder): result<
  'a,
  error<'apiError>,
> => {
  result
  ->Result.mapError(err => Unknown(err))
  ->Result.flatMap(json => json->decoder->Result.mapError(err => DecodeError(err)))
}

let mutate = (
  type argument response err,
  swrConfig: Swr.SwrConfig.t,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = err
  ),
  argument: argument,
) => {
  let module(Config) = config
  swrConfig->Swr.SwrConfig.mutateByKey(Array(Config.config(argument).key->Option.getUnsafe))
}

let mutateWithParams = (
  type argument response err,
  swrConfig: Swr.SwrConfig.t,
  config: module(Config with
    type argument = argument
    and type response = response
    and type error = err
  ),
  argument: argument,
  params,
  revalidate: bool,
) => {
  let module(Config) = config
  swrConfig->Swr.SwrConfig.mutateByKeyWithParams(
    Array(Config.config(argument).key->Option.getUnsafe),
    params,
    revalidate,
  )
}

module FetchAndRender = {
  @react.component
  let make = (
    type argument response err,
    ~config: module(Config with
      type argument = argument
      and type response = response
      and type error = err
    ),
    ~argument,
    ~options=?,
    ~children,
  ) => {
    let (data, _, _) = useFetcher(~options?, config, Some(argument))

    children(data)
  }
}

module FetchAndRenderWithSuspense = {
  @react.component
  let make = (
    type argument response err,
    ~config: module(Config with
      type argument = argument
      and type response = response
      and type error = err
    ),
    ~argument,
    ~children,
    ~fallback=?,
    ~options=?,
    ~fallbackRender,
  ) => {
    <Toolkit.Ui.ErrorBoundary
      fallbackRender={({error}) => {
        let module(C) = config
        let apiError: error<C.error> = error->Obj.magic
        fallbackRender(apiError)
      }}>
      <React.Suspense fallback={fallback->Option.getWithDefault(<Toolkit.Ui.SpinnerFullScreen />)}>
        <FetchAndRender ?options config argument> {data => children(data)} </FetchAndRender>
      </React.Suspense>
    </Toolkit.Ui.ErrorBoundary>
  }
}
