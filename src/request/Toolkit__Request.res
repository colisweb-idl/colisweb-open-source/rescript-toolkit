module type Config = {
  type argument
  type response
  type error
  let exec: argument => Promise.promise<
    result<response, Axios.WithResult.customError<error, Js.Json.t>>,
  >
}

module Make = (Config: Config) => {
  include Config
}
