# Request

## Usage

```rescript
module API = {
  let kyInstance = Ky.Instance.create({
    prefixUrl: `${ColiswebApi__Env.v6ApiUrl}`,
    timeout: ColiswebApi__Env.isNodeDevelopment ? 5000 : 20000,
  })

  module FetchUsers = {
    type argument = ()

    @spice
    type user = {
      id: string,
      name: string
    }

    @spice
    type response = {
      users: array<user>
    }

    type error = unit

    let config = () => {
      kyInstance,
      requestOptions: {
        method: Ky.HttpMethod.GET,
      },
      key: [
        "FetchUser"
      ],
      path: `/users`,
    }
  };
};

let (data, isLoading, _) = Request.useFetcher(module(API.FetchUsers), Some())

Request.fetchAPI(module(API.FetchUsers), Some())
->Promise.tapOk(({users}) => Js.log(users))
```

### Handle custom error

```rescript
module API = {
  let kyInstance = Ky.Instance.create({
    prefixUrl: `${ColiswebApi__Env.v6ApiUrl}`,
    timeout: ColiswebApi__Env.isNodeDevelopment ? 5000 : 20000,
  })

  module FetchUsers = {
    type argument = ()

    @spice
    type user = {
      id: string,
      name: string
    }

    @spice
    type response = {
      users: array<user>
    }

    type error = Anything

    let config = () => {
      kyInstance,
      requestOptions: {
        method: Ky.HttpMethod.GET,
      },
      key: [
        "FetchUser"
      ],
      path: `/users`,
      mapCustomErrors: error =>
        switch error.response {
        | Some({status: 409}) => Custom(Anything)
        | _ => Unknown(error)
        }->Promise.resolved,
    }
  };
};


Request.fetchAPI(module(API.FetchUsers), Some())
->Promise.tapOk(({users}) => Js.log(users))
->Promise.tapError(error => {
  switch error {
    | Custom(Anything) => ...
    | DecodeError(spiceError) => ...
    | Unknown(kyError) => ...
  }
})
```
